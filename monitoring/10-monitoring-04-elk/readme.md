# Домашнее задание к занятию "10.04. ELK"

## Дополнительные ссылки

При выполнении задания пользуйтесь вспомогательными ресурсами:

- [поднимаем elk в докер](https://www.elastic.co/guide/en/elastic-stack-get-started/current/get-started-docker.html)
- [поднимаем elk в докер с filebeat и докер логами](https://www.sarulabs.com/post/5/2019-08-12/sending-docker-logs-to-elasticsearch-and-kibana-with-filebeat.html)
- [конфигурируем logstash](https://www.elastic.co/guide/en/logstash/current/configuration.html)
- [плагины filter для logstash](https://www.elastic.co/guide/en/logstash/current/filter-plugins.html)
- [конфигурируем filebeat](https://www.elastic.co/guide/en/beats/libbeat/5.3/config-file-format.html)
- [привязываем индексы из elastic в kibana](https://www.elastic.co/guide/en/kibana/current/index-patterns.html)
- [как просматривать логи в kibana](https://www.elastic.co/guide/en/kibana/current/discover.html)
- [решение ошибки increase vm.max_map_count elasticsearch](https://stackoverflow.com/questions/42889241/how-to-increase-vm-max-map-count)

В процессе выполнения задания могут возникнуть также не указанные тут проблемы в зависимости от системы.

Используйте output stdout filebeat/kibana и api elasticsearch для изучения корня проблемы и ее устранения.

## Задание повышенной сложности

Не используйте директорию [help](./help) при выполнении домашнего задания.

## Задание 1

Вам необходимо поднять в докере:
- elasticsearch(hot и warm ноды)
- logstash
- kibana
- filebeat

и связать их между собой.

Logstash следует сконфигурировать для приёма по tcp json сообщений.

Filebeat следует сконфигурировать для отправки логов docker вашей системы в logstash.

В директории [help](./help) находится манифест docker-compose и конфигурации filebeat/logstash для быстрого 
выполнения данного задания.

Результатом выполнения данного задания должны быть:
- скриншот `docker ps` через 5 минут после старта всех контейнеров (их должно быть 5)
- скриншот интерфейса kibana
- docker-compose манифест (если вы не использовали директорию help)
- ваши yml конфигурации для стека (если вы не использовали директорию help)

## Ответ
```
As is вложенный проект не взлетел, даже после долгих изменений конфигураций.
То elastic в кластере не видели друг друга, то какие-то 
лицензии дополнительные просила, то пермишены на файловую структуру
 В общем на просторах инет нашел другой похожий проект, модернизировал его
```
[Ссылка на альтернативный](https://habr.com/ru/post/671344/)

```
Рабочий/модернизированный конфиг, работает в однонодовой конфигурации
Также в проекте все файлы конфигурации.
Kibana: http://localhost:5601, логин/пароль: elastic/MyPw123
на директорию ./docker_volumes выдать 777
```
```
lex@lexhost /var/lib $ docker container ps
WARNING: Error loading config file:/home/lex/.docker/config.json - stat /home/lex/.docker/config.json: permission denied
CONTAINER ID        IMAGE                     COMMAND                  CREATED             STATUS              PORTS                                                                    NAMES
0bfe107d0698        elastic/filebeat:7.16.2   "filebeat -e -stri..."   11 minutes ago      Up 11 minutes                                                                                beats
a5ce571b0c58        python:3.9-alpine         "python3 /opt/run.py"    19 minutes ago      Up 11 minutes                                                                                host_metrics_app
f01b2f1cd586        kibana:7.16.1             "/bin/tini -- /usr..."   6 hours ago         Up 11 minutes       0.0.0.0:5601->5601/tcp                                                   kibana
f88731adfed4        logstash:7.16.2           "/usr/local/bin/do..."   6 hours ago         Up 11 minutes       0.0.0.0:5044->5044/tcp, 0.0.0.0:9600->9600/tcp, 0.0.0.0:5001->5000/tcp   logstash
175b87c2ed83        elasticsearch:7.16.1      "/bin/tini -- /usr..."   6 hours ago         Up 11 minutes       0.0.0.0:9200->9200/tcp, 0.0.0.0:9300->9300/tcp                           elasticsearch
bcfd8d6f0219        postgres                  "docker-entrypoint..."   4 days ago          Up 5 hours          0.0.0.0:5432->5432/tcp                                                   postgres
lex@lexhost /var/lib $ 
```
![](pics/kibana.png)
![](pics/logstash.png)

## Задание 2

Перейдите в меню [создания index-patterns  в kibana](http://localhost:5601/app/management/kibana/indexPatterns/create)
и создайте несколько index-patterns из имеющихся.

Перейдите в меню просмотра логов в kibana (Discover) и самостоятельно изучите как отображаются логи и как производить 
поиск по логам.

В манифесте директории help также приведенно dummy приложение, которое генерирует рандомные события в stdout контейнера.
Данные логи должны порождать индекс logstash-* в elasticsearch. Если данного индекса нет - воспользуйтесь советами 
и источниками из раздела "Дополнительные ссылки" данного ДЗ.
 

## Ответ
Так изменил исходный проект, в моем проекте генерятся логи в расшаренную папку ./docker_volumes/host_metrics_app
под ключом host_metrics_app в клнфигурации FileBeat
---
![](pics/kibana.png)


### Как оформить ДЗ?

Выполненное домашнее задание пришлите ссылкой на .md-файл в вашем репозитории.

---

 
