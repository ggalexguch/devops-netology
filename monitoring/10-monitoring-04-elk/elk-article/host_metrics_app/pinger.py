#!/usr/bin/env python3

import datetime
import json
import random
import logging

import os
from time import sleep

while True:
    log = {
        'load_avg': {
            'load1': random.randrange(0, 100),
            'load5': random.randrange(0, 100),
            'load15': random.randrange(0, 100),
        },
        'ram_usage_percent': random.randrange(0, 100),
        'asctime': datetime.datetime.utcnow().isoformat(timespec='milliseconds', sep=' ')
    }

    with open(os.path.join('/host_metrics_app/host_metrics_app.log'), 'a') as f:
        f.write(json.dumps(log) + '\n')

    sleep(7)