# Домашнее задание к занятию "14.Средство визуализации Grafana"

## Задание повышенной сложности

**В части задания 1** не используйте директорию [help](./help) для сборки проекта, самостоятельно разверните grafana, где в 
роли источника данных будет выступать prometheus, а сборщиком данных node-exporter:
- grafana
- prometheus-server
- prometheus node-exporter

За дополнительными материалами, вы можете обратиться в официальную документацию grafana и prometheus.

В решении к домашнему заданию приведите также все конфигурации/скрипты/манифесты, которые вы 
использовали в процессе решения задания.

**В части задания 3** вы должны самостоятельно завести удобный для вас канал нотификации, например Telegram или Email
и отправить туда тестовые события.

В решении приведите скриншоты тестовых событий из каналов нотификаций.

## Обязательные задания

### Задание 1
Используя директорию [help](./help) внутри данного домашнего задания - запустите связку prometheus-grafana.

Зайдите в веб-интерфейс графана, используя авторизационные данные, указанные в манифесте docker-compose.

Подключите поднятый вами prometheus как источник данных.

Решение домашнего задания - скриншот веб-интерфейса grafana со списком подключенных Datasource.

## Ответ

Немного доработал yml файл, порты не видны были снаружи, добавил секцию ports для каждой сущности
Т.к. Grafana работает в docker, то доступ к прометеусу нужно указать по имени, заданной в yml файле

```
Starting nodeexporter ... done
Starting prometheus   ... done
Starting grafana      ... done
Attaching to nodeexporter, prometheus, grafana
prometheus      | level=info ts=2023-01-13T02:12:57.776Z caller=main.go:364 msg="Starting Prometheus" version="(version=2.24.1, branch=HEAD, revision=e4487274853c587717006eeda8804e597d120340)"
prometheus      | level=info ts=2023-01-13T02:12:57.776Z caller=main.go:369 build_context="(go=go1.15.6, user=root@0b5231a0de0f, date=20210120-00:09:36)"
nodeexporter    | level=info ts=2023-01-13T02:12:57.475Z caller=node_exporter.go:177 msg="Starting node_exporter" version="(version=1.0.1, branch=HEAD, revision=3715be6ae899f2a9b9dbfd9c39f3e09a7bd4559f)"
nodeexporter    | level=info ts=2023-01-13T02:12:57.475Z caller=node_exporter.go:178 msg="Build context" build_context="(go=go1.14.4, user=root@1f76dbbcfa55, date=20200616-12:44:12)"
prometheus      | level=info ts=2023-01-13T02:12:57.776Z caller=main.go:370 host_details="(Linux 4.4.0-148-generic #174~14.04.1-Ubuntu SMP Thu May 9 08:17:37 UTC 2019 x86_64 0c95e5f9f00e (none))"
prometheus      | level=info ts=2023-01-13T02:12:57.776Z caller=main.go:371 fd_limits="(soft=524288, hard=1048576)"
prometheus      | level=info ts=2023-01-13T02:12:57.776Z caller=main.go:372 vm_limits="(soft=unlimited, hard=unlimited)"
prometheus      | level=info ts=2023-01-13T02:12:57.778Z caller=web.go:530 component=web msg="Start listening for connections" address=0.0.0.0:9090
nodeexporter    | level=info ts=2023-01-13T02:12:57.476Z caller=node_exporter.go:105 msg="Enabled collectors"
prometheus      | level=info ts=2023-01-13T02:12:57.778Z caller=main.go:738 msg="Starting TSDB ..."
nodeexporter    | level=info ts=2023-01-13T02:12:57.476Z caller=node_exporter.go:112 collector=arp
nodeexporter    | level=info ts=2023-01-13T02:12:57.476Z caller=node_exporter.go:112 collector=bcache
nodeexporter    | level=info ts=2023-01-13T02:12:57.476Z caller=node_exporter.go:112 collector=bonding
nodeexporter    | level=info ts=2023-01-13T02:12:57.476Z caller=node_exporter.go:112 collector=btrfs
nodeexporter    | level=info ts=2023-01-13T02:12:57.476Z caller=node_exporter.go:112 collector=conntrack
nodeexporter    | level=info ts=2023-01-13T02:12:57.476Z caller=node_exporter.go:112 collector=cpu
nodeexporter    | level=info ts=2023-01-13T02:12:57.476Z caller=node_exporter.go:112 collector=cpufreq
nodeexporter    | level=info ts=2023-01-13T02:12:57.476Z caller=node_exporter.go:112 collector=diskstats
nodeexporter    | level=info ts=2023-01-13T02:12:57.476Z caller=node_exporter.go:112 collector=edac
nodeexporter    | level=info ts=2023-01-13T02:12:57.476Z caller=node_exporter.go:112 collector=entropy
nodeexporter    | level=info ts=2023-01-13T02:12:57.476Z caller=node_exporter.go:112 collector=filefd
nodeexporter    | level=info ts=2023-01-13T02:12:57.476Z caller=node_exporter.go:112 collector=filesystem
nodeexporter    | level=info ts=2023-01-13T02:12:57.476Z caller=node_exporter.go:112 collector=hwmon
nodeexporter    | level=info ts=2023-01-13T02:12:57.476Z caller=node_exporter.go:112 collector=infiniband
nodeexporter    | level=info ts=2023-01-13T02:12:57.476Z caller=node_exporter.go:112 collector=ipvs
nodeexporter    | level=info ts=2023-01-13T02:12:57.476Z caller=node_exporter.go:112 collector=loadavg
nodeexporter    | level=info ts=2023-01-13T02:12:57.476Z caller=node_exporter.go:112 collector=mdadm
nodeexporter    | level=info ts=2023-01-13T02:12:57.476Z caller=node_exporter.go:112 collector=meminfo
nodeexporter    | level=info ts=2023-01-13T02:12:57.476Z caller=node_exporter.go:112 collector=netclass
nodeexporter    | level=info ts=2023-01-13T02:12:57.476Z caller=node_exporter.go:112 collector=netdev
nodeexporter    | level=info ts=2023-01-13T02:12:57.476Z caller=node_exporter.go:112 collector=netstat
nodeexporter    | level=info ts=2023-01-13T02:12:57.476Z caller=node_exporter.go:112 collector=nfs
nodeexporter    | level=info ts=2023-01-13T02:12:57.476Z caller=node_exporter.go:112 collector=nfsd
nodeexporter    | level=info ts=2023-01-13T02:12:57.476Z caller=node_exporter.go:112 collector=powersupplyclass
nodeexporter    | level=info ts=2023-01-13T02:12:57.476Z caller=node_exporter.go:112 collector=pressure
nodeexporter    | level=info ts=2023-01-13T02:12:57.476Z caller=node_exporter.go:112 collector=rapl
nodeexporter    | level=info ts=2023-01-13T02:12:57.476Z caller=node_exporter.go:112 collector=schedstat
nodeexporter    | level=info ts=2023-01-13T02:12:57.476Z caller=node_exporter.go:112 collector=sockstat
nodeexporter    | level=info ts=2023-01-13T02:12:57.476Z caller=node_exporter.go:112 collector=softnet
nodeexporter    | level=info ts=2023-01-13T02:12:57.476Z caller=node_exporter.go:112 collector=stat
nodeexporter    | level=info ts=2023-01-13T02:12:57.476Z caller=node_exporter.go:112 collector=textfile
nodeexporter    | level=info ts=2023-01-13T02:12:57.476Z caller=node_exporter.go:112 collector=thermal_zone
nodeexporter    | level=info ts=2023-01-13T02:12:57.476Z caller=node_exporter.go:112 collector=time
nodeexporter    | level=info ts=2023-01-13T02:12:57.476Z caller=node_exporter.go:112 collector=timex
nodeexporter    | level=info ts=2023-01-13T02:12:57.476Z caller=node_exporter.go:112 collector=udp_queues
nodeexporter    | level=info ts=2023-01-13T02:12:57.476Z caller=node_exporter.go:112 collector=uname
nodeexporter    | level=info ts=2023-01-13T02:12:57.476Z caller=node_exporter.go:112 collector=vmstat
nodeexporter    | level=info ts=2023-01-13T02:12:57.476Z caller=node_exporter.go:112 collector=xfs
nodeexporter    | level=info ts=2023-01-13T02:12:57.476Z caller=node_exporter.go:112 collector=zfs
prometheus      | level=info ts=2023-01-13T02:12:57.778Z caller=repair.go:57 component=tsdb msg="Found healthy block" mint=1673551552634 maxt=1673553600000 ulid=01GPMDVBK6X1MYTKS20W6ZXP3M
prometheus      | level=info ts=2023-01-13T02:12:57.779Z caller=repair.go:57 component=tsdb msg="Found healthy block" mint=1673553602057 maxt=1673560800000 ulid=01GPMDVBR031285FKMG0XSKGM7
nodeexporter    | level=info ts=2023-01-13T02:12:57.476Z caller=node_exporter.go:191 msg="Listening on" address=:9100
prometheus      | level=info ts=2023-01-13T02:12:57.779Z caller=tls_config.go:191 component=web msg="TLS is disabled." http2=false
nodeexporter    | level=info ts=2023-01-13T02:12:57.476Z caller=tls_config.go:170 msg="TLS is disabled and it cannot be enabled on the fly." http2=false
prometheus      | level=info ts=2023-01-13T02:12:57.782Z caller=head.go:645 component=tsdb msg="Replaying on-disk memory mappable chunks if any"
prometheus      | level=info ts=2023-01-13T02:12:57.782Z caller=head.go:659 component=tsdb msg="On-disk memory mappable chunks replay completed" duration=3.616µs
prometheus      | level=info ts=2023-01-13T02:12:57.782Z caller=head.go:665 component=tsdb msg="Replaying WAL, this may take a while"
prometheus      | level=info ts=2023-01-13T02:12:57.791Z caller=head.go:717 component=tsdb msg="WAL segment loaded" segment=0 maxSegment=4
prometheus      | level=info ts=2023-01-13T02:12:57.793Z caller=head.go:717 component=tsdb msg="WAL segment loaded" segment=1 maxSegment=4
prometheus      | level=info ts=2023-01-13T02:12:57.832Z caller=head.go:717 component=tsdb msg="WAL segment loaded" segment=2 maxSegment=4
prometheus      | level=info ts=2023-01-13T02:12:57.832Z caller=head.go:717 component=tsdb msg="WAL segment loaded" segment=3 maxSegment=4
prometheus      | level=info ts=2023-01-13T02:12:57.832Z caller=head.go:717 component=tsdb msg="WAL segment loaded" segment=4 maxSegment=4
prometheus      | level=info ts=2023-01-13T02:12:57.832Z caller=head.go:722 component=tsdb msg="WAL replay completed" checkpoint_replay_duration=35.652µs wal_replay_duration=49.897437ms total_replay_duration=49.957675ms
prometheus      | level=info ts=2023-01-13T02:12:57.834Z caller=main.go:758 fs_type=EXT4_SUPER_MAGIC
prometheus      | level=info ts=2023-01-13T02:12:57.834Z caller=main.go:761 msg="TSDB started"
prometheus      | level=info ts=2023-01-13T02:12:57.834Z caller=main.go:887 msg="Loading configuration file" filename=/etc/prometheus/prometheus.yml
prometheus      | level=info ts=2023-01-13T02:12:57.835Z caller=main.go:918 msg="Completed loading of configuration file" filename=/etc/prometheus/prometheus.yml totalDuration=431.653µs remote_storage=1.322µs web_handler=321ns query_engine=928ns scrape=192.73µs scrape_sd=26.845µs notify=630ns notify_sd=1.993µs rules=1.002µs
prometheus      | level=info ts=2023-01-13T02:12:57.835Z caller=main.go:710 msg="Server is ready to receive web requests."
grafana         | t=2023-01-13T02:12:58+0000 lvl=info msg="Starting Grafana" logger=server version=7.4.0 commit=c2203b9859 branch=HEAD compiled=2021-02-04T11:11:00+0000
grafana         | t=2023-01-13T02:12:58+0000 lvl=info msg="Config loaded from" logger=settings file=/usr/share/grafana/conf/defaults.ini
grafana         | t=2023-01-13T02:12:58+0000 lvl=info msg="Config loaded from" logger=settings file=/etc/grafana/grafana.ini
grafana         | t=2023-01-13T02:12:58+0000 lvl=info msg="Config overridden from command line" logger=settings arg="default.paths.data=/var/lib/grafana"
grafana         | t=2023-01-13T02:12:58+0000 lvl=info msg="Config overridden from command line" logger=settings arg="default.paths.logs=/var/log/grafana"
grafana         | t=2023-01-13T02:12:58+0000 lvl=info msg="Config overridden from command line" logger=settings arg="default.paths.plugins=/var/lib/grafana/plugins"
grafana         | t=2023-01-13T02:12:58+0000 lvl=info msg="Config overridden from command line" logger=settings arg="default.paths.provisioning=/etc/grafana/provisioning"
grafana         | t=2023-01-13T02:12:58+0000 lvl=info msg="Config overridden from command line" logger=settings arg="default.log.mode=console"
grafana         | t=2023-01-13T02:12:58+0000 lvl=info msg="Config overridden from Environment variable" logger=settings var="GF_PATHS_DATA=/var/lib/grafana"
grafana         | t=2023-01-13T02:12:58+0000 lvl=info msg="Config overridden from Environment variable" logger=settings var="GF_PATHS_LOGS=/var/log/grafana"
grafana         | t=2023-01-13T02:12:58+0000 lvl=info msg="Config overridden from Environment variable" logger=settings var="GF_PATHS_PLUGINS=/var/lib/grafana/plugins"
grafana         | t=2023-01-13T02:12:58+0000 lvl=info msg="Config overridden from Environment variable" logger=settings var="GF_PATHS_PROVISIONING=/etc/grafana/provisioning"
grafana         | t=2023-01-13T02:12:58+0000 lvl=info msg="Config overridden from Environment variable" logger=settings var="GF_SECURITY_ADMIN_USER=admin"
grafana         | t=2023-01-13T02:12:58+0000 lvl=info msg="Config overridden from Environment variable" logger=settings var="GF_SECURITY_ADMIN_PASSWORD=*********"
grafana         | t=2023-01-13T02:12:58+0000 lvl=info msg="Config overridden from Environment variable" logger=settings var="GF_USERS_ALLOW_SIGN_UP=false"
grafana         | t=2023-01-13T02:12:58+0000 lvl=info msg="Path Home" logger=settings path=/usr/share/grafana
grafana         | t=2023-01-13T02:12:58+0000 lvl=info msg="Path Data" logger=settings path=/var/lib/grafana
grafana         | t=2023-01-13T02:12:58+0000 lvl=info msg="Path Logs" logger=settings path=/var/log/grafana
grafana         | t=2023-01-13T02:12:58+0000 lvl=info msg="Path Plugins" logger=settings path=/var/lib/grafana/plugins
grafana         | t=2023-01-13T02:12:58+0000 lvl=info msg="Path Provisioning" logger=settings path=/etc/grafana/provisioning
grafana         | t=2023-01-13T02:12:58+0000 lvl=info msg="App mode production" logger=settings
grafana         | t=2023-01-13T02:12:58+0000 lvl=info msg="Connecting to DB" logger=sqlstore dbtype=sqlite3
grafana         | t=2023-01-13T02:12:58+0000 lvl=info msg="Starting DB migrations" logger=migrator
grafana         | t=2023-01-13T02:12:58+0000 lvl=info msg="Starting plugin search" logger=plugins
grafana         | t=2023-01-13T02:12:58+0000 lvl=info msg="Registering plugin" logger=plugins id=input
grafana         | t=2023-01-13T02:12:58+0000 lvl=info msg="HTTP Server Listen" logger=http.server address=[::]:3000 protocol=http subUrl= socket=
prometheus      | level=info ts=2023-01-13T02:13:07.114Z caller=compact.go:498 component=tsdb msg="write block resulted in empty block" mint=1673560800000 maxt=1673568000000 duration=24.15208ms
prometheus      | level=info ts=2023-01-13T02:13:07.115Z caller=head.go:824 component=tsdb msg="Head GC completed" duration=1.180851ms
prometheus      | level=info ts=2023-01-13T02:13:07.115Z caller=checkpoint.go:95 component=tsdb msg="Creating checkpoint" from_segment=0 to_segment=2 mint=1673568000000
prometheus      | level=info ts=2023-01-13T02:13:07.168Z caller=head.go:921 component=tsdb msg="WAL checkpoint complete" first=0 last=2 duration=52.50148ms
grafana         | t=2023-01-13T02:30:29+0000 lvl=eror msg="Data proxy error" logger=data-proxy-log userId=1 orgId=1 uname=admin path=/api/datasources/proxy/1/api/v1/query_range remote_addr=172.18.0.1 referer="http://localhost:3000/explore?left=%7B%22range%22:%7B%22from%22:%221673576182650%22,%22to%22:%221673576487234%22%7D,%22datasource%22:%22Prometheus%22,%22context%22:%22explore%22,%22queries%22:%5B%7B%22expr%22:%22avg(node_filesystem_avail_bytes)%2F1000000000%22,%22interval%22:%22%22,%22refId%22:%22A%22,%22intervalFactor%22:1,%22datasource%22:%22Prometheus%22%7D%5D,%22originPanelId%22:10%7D" error="http: proxy error: context canceled"
grafana         | t=2023-01-13T02:30:29+0000 lvl=eror msg="Request Completed" logger=context userId=1 orgId=1 uname=admin method=GET path=/api/datasources/proxy/1/api/v1/query_range status=502 remote_addr=172.18.0.1 time_ms=0 size=0 referer="http://localhost:3000/explore?left=%7B%22range%22:%7B%22from%22:%221673576182650%22,%22to%22:%221673576487234%22%7D,%22datasource%22:%22Prometheus%22,%22context%22:%22explore%22,%22queries%22:%5B%7B%22expr%22:%22avg(node_filesystem_avail_bytes)%2F1000000000%22,%22interval%22:%22%22,%22refId%22:%22A%22,%22intervalFactor%22:1,%22datasource%22:%22Prometheus%22%7D%5D,%22originPanelId%22:10%7D"
grafana         | t=2023-01-13T02:30:29+0000 lvl=eror msg="Data proxy error" logger=data-proxy-log userId=1 orgId=1 uname=admin path=/api/datasources/proxy/1/api/v1/query_range remote_addr=172.18.0.1 referer="http://localhost:3000/explore?left=%7B%22range%22:%7B%22from%22:%221673576182650%22,%22to%22:%221673576487234%22%7D,%22datasource%22:%22Prometheus%22,%22context%22:%22explore%22,%22queries%22:%5B%7B%22expr%22:%22avg(node_filesystem_avail_bytes)%2F1000000000%22,%22interval%22:%22%22,%22refId%22:%22A%22,%22intervalFactor%22:1,%22datasource%22:%22Prometheus%22%7D%5D,%22originPanelId%22:10%7D" error="http: proxy error: context canceled"
grafana         | t=2023-01-13T02:30:29+0000 lvl=eror msg="Request Completed" logger=context userId=1 orgId=1 uname=admin method=GET path=/api/datasources/proxy/1/api/v1/query_range status=502 remote_addr=172.18.0.1 time_ms=0 size=0 referer="http://localhost:3000/explore?left=%7B%22range%22:%7B%22from%22:%221673576182650%22,%22to%22:%221673576487234%22%7D,%22datasource%22:%22Prometheus%22,%22context%22:%22explore%22,%22queries%22:%5B%7B%22expr%22:%22avg(node_filesystem_avail_bytes)%2F1000000000%22,%22interval%22:%22%22,%22refId%22:%22A%22,%22intervalFactor%22:1,%22datasource%22:%22Prometheus%22%7D%5D,%22originPanelId%22:10%7D"

```
![](pics/grafana.png)
![](pics/grafana_ds.png)


## Задание 2
Изучите самостоятельно ресурсы:
- [promql-for-humans](https://timber.io/blog/promql-for-humans/#cpu-usage-by-instance)
- [understanding prometheus cpu metrics](https://www.robustperception.io/understanding-machine-cpu-usage)

Создайте Dashboard и в ней создайте следующие Panels:
- Утилизация CPU для nodeexporter (в процентах, 100-idle)
- CPULA 1/5/15
- Количество свободной оперативной памяти
- Количество места на файловой системе

Для решения данного ДЗ приведите promql запросы для выдачи этих метрик, а также скриншот получившейся Dashboard.

## Ответ

![](pics/grafana_dashboard.png)

```
HDD Free Space 
    avg(node_filesystem_avail_bytes)/1000000000
Memory Usage
    Memory 5Min =>  100 * (1 - ((avg_over_time(node_memory_MemFree_bytes[5m]) + avg_over_time(node_memory_Cached_bytes[5m]) + avg_over_time(node_memory_Buffers_bytes[5m])) / avg_over_time(node_memory_MemTotal_bytes[5m])))
    Memory 15Min =>  100 * (1 - ((avg_over_time(node_memory_MemFree_bytes[5m]) + avg_over_time(node_memory_Cached_bytes[15m]) + avg_over_time(node_memory_Buffers_bytes[15m])) / avg_over_time(node_memory_MemTotal_bytes[15m])))
CPU Usage
    CPU Total AVG (1m) => 100 - avg(((irate(node_cpu_seconds_total{job="nodeexporter",mode="idle"}[1m])) * 100))
    CPU Total AVG (5m) => 100 - avg(((irate(node_cpu_seconds_total{job="nodeexporter",mode="idle"}[5m])) * 100))
    CPU Total AVG (15m) => 100 - avg(((irate(node_cpu_seconds_total{job="nodeexporter",mode="idle"}[15m])) * 100))

```

## Задание 3
Создайте для каждой Dashboard подходящее правило alert (можно обратиться к первой лекции в блоке "Мониторинг").

Для решения ДЗ - приведите скриншот вашей итоговой Dashboard.

## Ответ

![](pics/totalAlerts.png)
![](pics/AlertingPage.png)

## Задание 4
Сохраните ваш Dashboard.

Для этого перейдите в настройки Dashboard, выберите в боковом меню "JSON MODEL".

Далее скопируйте отображаемое json-содержимое в отдельный файл и сохраните его.

В решении задания - приведите листинг этого файла.

[Файл DashboardModel](GrafanaDashboardModel.json)

---

### Как оформить ДЗ?

Выполненное домашнее задание пришлите ссылкой на .md-файл в вашем репозитории.

---
