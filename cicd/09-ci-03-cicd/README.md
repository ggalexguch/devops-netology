# Домашнее задание к занятию "09.03 CI\CD"

## Подготовка к выполнению

1. Создаём 2 VM в yandex cloud со следующими параметрами: 2CPU 4RAM Centos7(остальное по минимальным требованиям)
2. Прописываем в [inventory](./infrastructure/inventory/cicd/hosts.yml) [playbook'a](./infrastructure/site.yml) созданные хосты
3. Добавляем в [files](./infrastructure/files/) файл со своим публичным ключом (id_rsa.pub). Если ключ называется иначе - найдите таску в плейбуке, которая использует id_rsa.pub имя и исправьте на своё
4. Запускаем playbook, ожидаем успешного завершения
5. Проверяем готовность Sonarqube через [браузер](http://localhost:9000)
6. Заходим под admin\admin, меняем пароль на свой
7.  Проверяем готовность Nexus через [бразуер](http://localhost:8081)
8. Подключаемся под admin\admin123, меняем пароль, сохраняем анонимный доступ

## Ответ 

![](pics/sonarInitial.png)
![](pics/nexusInitial.png)

## Знакомоство с SonarQube

### Основная часть

1. Создаём новый проект, название произвольное
2. Скачиваем пакет sonar-scanner, который нам предлагает скачать сам sonarqube
3. Делаем так, чтобы binary был доступен через вызов в shell (или меняем переменную PATH или любой другой удобный вам способ)
4. Проверяем `sonar-scanner --version`
5. Запускаем анализатор против кода из директории [example](./example) с дополнительным ключом `-Dsonar.coverage.exclusions=fail.py`
6. Смотрим результат в интерфейсе
7. Исправляем ошибки, которые он выявил(включая warnings)
8. Запускаем анализатор повторно - проверяем, что QG пройдены успешно
9. Делаем скриншот успешного прохождения анализа, прикладываем к решению ДЗ

## Ответ
   
      lex@lexhost ~/PycharmProjects/netologyProjects/devops-netology/cicd/09-ci-03-cicd/example $ sonar-scanner   -Dsonar.projectKey=netology   -Dsonar.sources=.   -Dsonar.host.url=http://158.160.50.23:9000   -Dsonar.login=88d6b8721ea4b4186707a56f7404eff07d3b4b9d
      INFO: Scanner configuration file: /home/lex/netology/cicd/sonar-scanner-4.7.0.2747-linux/conf/sonar-scanner.properties
      INFO: Project root configuration file: NONE
      INFO: SonarScanner 4.7.0.2747
      INFO: Java 11.0.14.1 Eclipse Adoptium (64-bit)
      INFO: Linux 4.4.0-148-generic amd64
      ...
      INFO: Project root configuration file: NONE
      INFO: Analyzing on SonarQube server 9.1.0
      INFO: Default locale: "ru_RU", source code encoding: "UTF-8" (analysis is platform dependent)
      INFO: Load global settings
      INFO: Load global settings (done) | time=1213ms
      INFO: Server id: 9CFC3560-AYV9xzX13nYREPCaxBZA
      INFO: User cache: /home/lex/.sonar/cache
      INFO: Load/download plugins
      INFO: Load plugins index
      INFO: Load plugins index (done) | time=116ms
      INFO: Load/download plugins (done) | time=179ms
      INFO: Process project properties
      INFO: Process project properties (done) | time=9ms
      INFO: Execute project builders
      INFO: Execute project builders (done) | time=1ms
      INFO: Project key: netology
      INFO: Base dir: /home/lex/PycharmProjects/devops-netology/cicd/09-ci-03-cicd/example
      INFO: Working dir: /home/lex/PycharmProjects/devops-netology/cicd/09-ci-03-cicd/example/.scannerwork
      INFO: Load project settings for component key: 'netology'
      INFO: Load project settings for component key: 'netology' (done) | time=274ms
      INFO: Load quality profiles
      INFO: Load quality profiles (done) | time=114ms
      INFO: Load active rules
      INFO: Load active rules (done) | time=3751ms
      INFO: Indexing files...
      INFO: Project configuration:
      INFO: 1 file indexed
      INFO: 0 files ignored because of scm ignore settings
      INFO: Quality profile for py: Sonar way
      INFO: ------------- Run sensors on module netology
      INFO: Load metrics repository
      INFO: Load metrics repository (done) | time=98ms
      INFO: Sensor Python Sensor [python]
      WARN: Your code is analyzed as compatible with python 2 and 3 by default. This will prevent the detection of issues specific to python 2 or python 3. You can get a more precise analysis by setting a python version in your configuration via the parameter "sonar.python.version"
      INFO: Starting global symbols computation
      INFO: 1 source file to be analyzed
      INFO: Load project repositories
      INFO: Load project repositories (done) | time=79ms
      INFO: 1/1 source file has been analyzed
      INFO: Starting rules execution
      INFO: 1 source file to be analyzed
      INFO: 1/1 source file has been analyzed
      INFO: Sensor Python Sensor [python] (done) | time=713ms
      INFO: Sensor Cobertura Sensor for Python coverage [python]
      INFO: Sensor Cobertura Sensor for Python coverage [python] (done) | time=43ms
      INFO: Sensor PythonXUnitSensor [python]
      INFO: Sensor PythonXUnitSensor [python] (done) | time=2ms
      INFO: Sensor CSS Rules [cssfamily]
      INFO: No CSS, PHP, HTML or VueJS files are found in the project. CSS analysis is skipped.
      INFO: Sensor CSS Rules [cssfamily] (done) | time=2ms
      INFO: Sensor JaCoCo XML Report Importer [jacoco]
      INFO: 'sonar.coverage.jacoco.xmlReportPaths' is not defined. Using default locations: target/site/jacoco/jacoco.xml,target/site/jacoco-it/jacoco.xml,build/reports/jacoco/test/jacocoTestReport.xml
      INFO: No report imported, no coverage information will be imported by JaCoCo XML Report Importer
      INFO: Sensor JaCoCo XML Report Importer [jacoco] (done) | time=13ms
      INFO: Sensor C# Project Type Information [csharp]
      INFO: Sensor C# Project Type Information [csharp] (done) | time=4ms
      INFO: Sensor C# Analysis Log [csharp]
      INFO: Sensor C# Analysis Log [csharp] (done) | time=66ms
      INFO: Sensor C# Properties [csharp]
      INFO: Sensor C# Properties [csharp] (done) | time=0ms
      INFO: Sensor JavaXmlSensor [java]
      INFO: Sensor JavaXmlSensor [java] (done) | time=3ms
      INFO: Sensor HTML [web]
      INFO: Sensor HTML [web] (done) | time=12ms
      INFO: Sensor VB.NET Project Type Information [vbnet]
      INFO: Sensor VB.NET Project Type Information [vbnet] (done) | time=3ms
      INFO: Sensor VB.NET Analysis Log [vbnet]
      INFO: Sensor VB.NET Analysis Log [vbnet] (done) | time=61ms
      INFO: Sensor VB.NET Properties [vbnet]
      INFO: Sensor VB.NET Properties [vbnet] (done) | time=1ms
      INFO: ------------- Run sensors on project
      INFO: Sensor Zero Coverage Sensor
      INFO: Sensor Zero Coverage Sensor (done) | time=25ms
      INFO: SCM Publisher SCM provider for this project is: git
      INFO: SCM Publisher 1 source file to be analyzed
      INFO: SCM Publisher 0/1 source files have been analyzed (done) | time=381ms
      WARN: Missing blame information for the following files:
      WARN:   * fail.py
      WARN: This may lead to missing/broken features in SonarQube
      INFO: CPD Executor Calculating CPD for 1 file
      INFO: CPD Executor CPD calculation finished (done) | time=12ms
      INFO: Analysis report generated in 103ms, dir size=103,1 kB
      INFO: Analysis report compressed in 11ms, zip size=14,1 kB
      INFO: Analysis report uploaded in 222ms
      INFO: ANALYSIS SUCCESSFUL, you can browse http://158.160.50.23:9000/dashboard?id=netology
      INFO: Note that you will be able to access the updated dashboard once the server has processed the submitted analysis report
      INFO: More about the report processing at http://158.160.50.23:9000/api/ce/task?id=AYV95FeW3nYREPCaxGeI
      INFO: Analysis total time: 7.701 s
      INFO: ------------------------------------------------------------------------
      INFO: EXECUTION SUCCESS
      INFO: ------------------------------------------------------------------------
      INFO: Total time: 9.983s
      INFO: Final Memory: 8M/40M
      INFO: ------------------------------------------------------------------------
      lex@lexhost ~/PycharmProjects/devops-netology/cicd/09-ci-03-cicd/example $ 
      
![](pics/passedErrors.png)
![](pics/passedE1.png)
![](pics/passedE2.png)
![](pics/exampleFile.png)

После изменений
![](pics/ok.png)

## Знакомство с Nexus

### Основная часть

1. В репозиторий `maven-releases` загружаем артефакт с GAV параметрами:
   1. groupId: netology
   2. artifactId: java
   3. version: 8_282
   4. classifier: distrib
   5. type: tar.gz
2. В него же загружаем такой же артефакт, но с version: 8_102
3. Проверяем, что все файлы загрузились успешно
4. В ответе присылаем файл `maven-metadata.xml` для этого артефекта

## Ответ
![](pics/nexusRepo.png)
![](pics/metadata.png)

[maven-metadata](pics/maven-metadata.xml)



### Знакомство с Maven

### Подготовка к выполнению

1. Скачиваем дистрибутив с [maven](https://maven.apache.org/download.cgi)
2. Разархивируем, делаем так, чтобы binary был доступен через вызов в shell (или меняем переменную PATH или любой другой удобный вам способ)
3. Удаляем из `apache-maven-<version>/conf/settings.xml` упоминание о правиле, отвергающем http соединение( раздел mirrors->id: my-repository-http-blocker)
4. Проверяем `mvn --version`
5. Забираем директорию [mvn](./mvn) с pom

## Ответ

      lex@lexhost ~/PycharmProjects/netologyProjects/ $ mvn --version
      Apache Maven 3.3.9 (bb52d8502b132ec0a5a3f4c09453c07478323dc5; 2015-11-10T22:41:47+06:00)
      Maven home: /usr/share/maven3
      Java version: 1.8.0_201, vendor: Oracle Corporation
      Java home: /usr/lib/jvm/java-8-oracle/jre
      Default locale: ru_RU, platform encoding: UTF-8
      OS name: "linux", version: "4.4.0-148-generic", arch: "amd64", family: "unix"
      lex@lexhost ~/PycharmProjects/netologyProjects/ $ 

### Основная часть

1. Меняем в `pom.xml` блок с зависимостями под наш артефакт из первого пункта задания для Nexus (java с версией 8_282)
2. Запускаем команду `mvn package` в директории с `pom.xml`, ожидаем успешного окончания
3. Проверяем директорию `~/.m2/repository/`, находим наш артефакт
4. В ответе присылаем исправленный файл `pom.xml`

## Ответ

      lex@lexhost ~/PycharmProjects/netologyProjects/devops-netology/cicd/09-ci-03-cicd/mvn $ mvn package
      [INFO] Scanning for projects...
      [INFO]                                                                         
      [INFO] ------------------------------------------------------------------------
      [INFO] Building simple-app 1.0-SNAPSHOT
      [INFO] ------------------------------------------------------------------------
      Downloading: http://158.160.50.235:8081/repository/maven-releases/netology/java/8_282/java-8_282.pom
      Downloading: https://repo.maven.apache.org/maven2/netology/java/8_282/java-8_282.pom
      [WARNING] The POM for netology:java:tar.gz:distrib:8_282 is missing, no dependency information available
      Downloading: http://158.160.50.235:8081/repository/maven-releases/netology/java/8_282/java-8_282-distrib.tar.gz
      Downloaded: http://158.160.50.235:8081/repository/maven-releases/netology/java/8_282/java-8_282-distrib.tar.gz (246 KB at 238.0 KB/sec)
      [INFO] 
      [INFO] --- maven-resources-plugin:2.6:resources (default-resources) @ simple-app ---
      [WARNING] Using platform encoding (UTF-8 actually) to copy filtered resources, i.e. build is platform dependent!
      [INFO] skip non existing resourceDirectory /home/lex/PycharmProjects/netologyProjects/2.1. Системы контроля версий/devops-netology/cicd/09-ci-03-cicd/mvn/src/main/resources
      [INFO] 
      [INFO] --- maven-compiler-plugin:3.1:compile (default-compile) @ simple-app ---
      [INFO] No sources to compile
      [INFO] 
      [INFO] --- maven-resources-plugin:2.6:testResources (default-testResources) @ simple-app ---
      [WARNING] Using platform encoding (UTF-8 actually) to copy filtered resources, i.e. build is platform dependent!
      [INFO] skip non existing resourceDirectory /home/lex/PycharmProjects/netologyProjects/2.1. Системы контроля версий/devops-netology/cicd/09-ci-03-cicd/mvn/src/test/resources
      [INFO] 
      [INFO] --- maven-compiler-plugin:3.1:testCompile (default-testCompile) @ simple-app ---
      [INFO] No sources to compile
      [INFO] 
      [INFO] --- maven-surefire-plugin:2.12.4:test (default-test) @ simple-app ---
      [INFO] No tests to run.
      [INFO] 
      [INFO] --- maven-jar-plugin:2.4:jar (default-jar) @ simple-app ---
      [WARNING] JAR will be empty - no content was marked for inclusion!
      [INFO] Building jar: /home/lex/PycharmProjects/netologyProjects/devops-netology/cicd/09-ci-03-cicd/mvn/target/simple-app-1.0-SNAPSHOT.jar
      [INFO] ------------------------------------------------------------------------
      [INFO] BUILD SUCCESS
      [INFO] ------------------------------------------------------------------------
      [INFO] Total time: 3.434 s
      [INFO] Finished at: 2023-01-05T00:25:41+06:00
      [INFO] Final Memory: 12M/210M
      [INFO] ------------------------------------------------------------------------
      lex@lexhost ~/PycharmProjects/netologyProjects/devops-netology/cicd/09-ci-03-cicd/mvn $ ls
      pom.xml  target
      lex@lexhost ~/PycharmProjects/netologyProjects/devops-netology/cicd/09-ci-03-cicd/mvn $ cd target/
      lex@lexhost ~/PycharmProjects/netologyProjects/devops-netology/cicd/09-ci-03-cicd/mvn/target $ ll
      итого 16
      drwxr-xr-x 3 lex lex 4096 янв.   5 00:25 ./
      drwxr-xr-x 3 lex lex 4096 янв.   5 00:25 ../
      drwxr-xr-x 2 lex lex 4096 янв.   5 00:25 maven-archiver/
      -rw-r--r-- 1 lex lex 1599 янв.   5 00:25 simple-app-1.0-SNAPSHOT.jar
      lex@lexhost ~/PycharmProjects/netologyProjects/devops-netology/cicd/09-ci-03-cicd/mvn/target $ 

      lex@lexhost ~/.m2/repository/netology/java/8_282 $ ll
      итого 268
      drwxr-xr-x 2 lex lex   4096 янв.   5 00:25 ./
      drwxr-xr-x 3 lex lex   4096 янв.   5 00:25 ../
      -rw-r--r-- 1 lex lex 251224 янв.   5 00:25 java-8_282-distrib.tar.gz
      -rw-r--r-- 1 lex lex     40 янв.   5 00:25 java-8_282-distrib.tar.gz.sha1
      -rw-r--r-- 1 lex lex    390 янв.   5 00:25 java-8_282.pom.lastUpdated
      -rw-r--r-- 1 lex lex    175 янв.   5 00:25 _remote.repositories
      lex@lexhost ~/.m2/repository/netology/java/8_282 $ 

[pom.xml](mvn/pom.xml)

---

### Как оформить ДЗ?

Выполненное домашнее задание пришлите ссылкой на .md-файл в вашем репозитории.

---
