### Задача 1.

Давайте потренируемся читать исходный код AWS провайдера, который можно склонировать от сюда: https://github.com/hashicorp/terraform-provider-aws.git. Просто найдите нужные ресурсы в исходном коде и ответы на вопросы станут понятны.

1. Найдите, где перечислены все доступные resource и data_source, приложите ссылку на эти строки в коде на гитхабе.
2. Для создания очереди сообщений SQS используется ресурс aws_sqs_queue у которого есть параметр name.
* С каким другим параметром конфликтует name? Приложите строчку кода, в которой это указано. 
* Какая максимальная длина имени? 
* Какому регулярному выражению должно подчиняться имя?

**Ответ:**

####Вопрос 1
1. resource -->   https://github.com/hashicorp/terraform-provider-aws/blob/8e4d8a3f3f781b83f96217c2275f541c893fec5a/aws/provider.go#L411
2. datasource --> https://github.com/hashicorp/terraform-provider-aws/blob/8e4d8a3f3f781b83f96217c2275f541c893fec5a/aws/provider.go#L169

####Вопрос 2
name конфликтует с name_prefix
Выдержка
    
    Schema: map[string]*schema.Schema{
                "name": {
                    Type:          schema.TypeString,
                    Optional:      true,
                    ForceNew:      true,
                    Computed:      true,
                    ConflictsWith: []string{"name_prefix"},
                    ValidateFunc:  validateSQSQueueName,
                },
                "name_prefix": {
                    Type:          schema.TypeString,
                    Optional:      true,
                    ForceNew:      true,
                    ConflictsWith: []string{"name"},
                },
    .....
Ссылка: https://github.com/hashicorp/terraform-provider-aws/blob/8e4d8a3f3f781b83f96217c2275f541c893fec5a/aws/resource_aws_sqs_queue.go#L56

Максимальная длина имени проверяется в функциях: 
* validateSQSQueueName https://github.com/hashicorp/terraform-provider-aws/blob/8e4d8a3f3f781b83f96217c2275f541c893fec5a/aws/validators.go#L1037
* validateSQSNonFifoQueueName https://github.com/hashicorp/terraform-provider-aws/blob/8e4d8a3f3f781b83f96217c2275f541c893fec5a/aws/validators.go#L1050
* validateSQSFifoQueueName https://github.com/hashicorp/terraform-provider-aws/blob/8e4d8a3f3f781b83f96217c2275f541c893fec5a/aws/validators.go#L1064
<br> и равна не более 80 символам

Имя должно подчиняться регуляркам:
<br>NonFifo ``^[0-9A-Za-z-_]+$`` https://github.com/hashicorp/terraform-provider-aws/blob/8e4d8a3f3f781b83f96217c2275f541c893fec5a/aws/validators.go#L1054
<br> Fifo 
* ``^[0-9A-Za-z-_.]+$`` https://github.com/hashicorp/terraform-provider-aws/blob/8e4d8a3f3f781b83f96217c2275f541c893fec5a/aws/validators.go#L1068
* ``^[^a-zA-Z0-9-_]`` https://github.com/hashicorp/terraform-provider-aws/blob/8e4d8a3f3f781b83f96217c2275f541c893fec5a/aws/validators.go#L1072
* ``\.fifo$`` https://github.com/hashicorp/terraform-provider-aws/blob/8e4d8a3f3f781b83f96217c2275f541c893fec5a/aws/validators.go#L1076





