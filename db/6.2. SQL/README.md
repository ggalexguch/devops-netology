### Задача 1.
Используя docker поднимите инстанс PostgreSQL (версию 12) c 2 volume, в который будут складываться данные БД и бэкапы.

Приведите получившуюся команду или docker-compose манифест.

**Ответ:**

    lex@lex ~ $ sudo docker pull postgres:12
    lex@lex ~ $ sudo docker volume create vol1
    vol1
    lex@lex ~ $ sudo docker volume create vol2
    vol2
    lex@lex ~ $ sudo docker volume ls
    DRIVER              VOLUME NAME
    local               vol1
    local               vol2
    lex@lex  ~ $ sudo docker volume inspect vol1
    [
        {
            "Driver": "local",
            "Labels": {},
            "Mountpoint": "/var/lib/docker/volumes/vol1/_data",
            "Name": "vol1",
            "Options": {},
            "Scope": "local"
        }
    ]
    lex@lex  ~ $ sudo docker volume inspect vol2
    [
        {
            "Driver": "local",
            "Labels": {},
            "Mountpoint": "/var/lib/docker/volumes/vol2/_data",
            "Name": "vol2",
            "Options": {},
            "Scope": "local"
        }
    ]
    lex@lex  ~ $ 
    lex@lex  ~ $ sudo docker run --name pg-docker -e POSTGRES_PASSWORD=postgres -ti  -d  -v vol1:/var/lib/postgresql/data -v vol2:/var/lib/postgresql postgres:12
    0867df9ded834762c198269f1bf06b5e329ac1b00dbd4533fd921c0c1d822220
    lex@lex  ~ $ 
    lex@lex  ~ $ 
    lex@lex  ~ $ sudo docker exec -it pg-docker bash
    root@0867df9ded83:/# su - postgres
    postgres@0867df9ded83:~$ psql
    psql (12.12 (Debian 12.12-1.pgdg110+1))
    Type "help" for help.
    
    postgres=# exit
    postgres@0867df9ded83:~$ exit
    logout
    root@0867df9ded83:/# exit
    exit
    lex@lex  ~ $ 

### Задача 2.
Используя docker поднимите инстанс PostgreSQL (версию 12) c 2 volume, в который будут складываться данные БД и бэкапы.

Приведите получившуюся команду или docker-compose манифест.

**Ответ:**

    lex@lex  ~ $ sudo docker exec -it pg-docker bash
    root@0867df9ded83:/# su - postgres
    postgres@0867df9ded83:~$ psql
    psql (12.12 (Debian 12.12-1.pgdg110+1))
    Type "help" for help.
    
    postgres=# \l                    
                                     List of databases
       Name    |  Owner   | Encoding |  Collate   |   Ctype    |   Access privileges   
    -----------+----------+----------+------------+------------+-----------------------
     postgres  | postgres | UTF8     | en_US.utf8 | en_US.utf8 | 
     template0 | postgres | UTF8     | en_US.utf8 | en_US.utf8 | =c/postgres          +
               |          |          |            |            | postgres=CTc/postgres
     template1 | postgres | UTF8     | en_US.utf8 | en_US.utf8 | =c/postgres          +
               |          |          |            |            | postgres=CTc/postgres
    (3 rows)
    
    postgres=# create database test_db;
    CREATE DATABASE
    postgres=# CREATE ROLE "test-admin-user" SUPERUSER NOCREATEDB NOCREATEROLE NOINHERIT LOGIN;
    CREATE ROLE
    postgres=# \l
                                 List of databases
       Name    |  Owner   | Encoding |  Collate   |   Ctype    |   Access privileges   
    -----------+----------+----------+------------+------------+-----------------------
     postgres  | postgres | UTF8     | en_US.utf8 | en_US.utf8 | 
     template0 | postgres | UTF8     | en_US.utf8 | en_US.utf8 | =c/postgres          +
               |          |          |            |            | postgres=CTc/postgres
     template1 | postgres | UTF8     | en_US.utf8 | en_US.utf8 | =c/postgres          +
               |          |          |            |            | postgres=CTc/postgres
     test_db   | postgres | UTF8     | en_US.utf8 | en_US.utf8 | 
    (4 rows)
    
    postgres=#
    postgres=# exit   
    postgres@0867df9ded83:~$ psql test_db;
    psql (12.12 (Debian 12.12-1.pgdg110+1))
    Type "help" for help.
    
    test_db=# CREATE TABLE orders                                                          (  
    id serial, 
    name varchar(255), 
    price integer, 
    PRIMARY KEY (id) 
    );
    CREATE TABLE
    test_db=# CREATE TABLE clients 
    (
            id serial PRIMARY KEY,
            lastname varchar(255),
            country varchar(255),
            order_id integer,
            FOREIGN KEY (order_id) REFERENCES orders (Id)
    );
    CREATE TABLE
    test_db=# 
    test_db=# \dt
              List of relations
     Schema |  Name   | Type  |  Owner   
    --------+---------+-------+----------
     public | clients | table | postgres
     public | orders  | table | postgres
    (2 rows)
    
    test_db=# CREATE ROLE "test-simple-user" NOSUPERUSER NOCREATEDB NOCREATEROLE NOINHERIT LOGIN;
    CREATE ROLE
    test_db=# GRANT SELECT ON TABLE public.clients TO "test-simple-user";
    GRANT INSERT ON TABLE public.clients TO "test-simple-user";
    GRANT UPDATE ON TABLE public.clients TO "test-simple-user";
    GRANT DELETE ON TABLE public.clients TO "test-simple-user";
    GRANT SELECT ON TABLE public.orders TO "test-simple-user";
    GRANT INSERT ON TABLE public.orders TO "test-simple-user";
    GRANT UPDATE ON TABLE public.orders TO "test-simple-user";
    GRANT DELETE ON TABLE public.orders TO "test-simple-user";
    GRANT
    GRANT
    GRANT
    GRANT
    GRANT
    GRANT
    GRANT
    GRANT
    test_db=# 
    test_db=# \du
                                       List of roles
    Role name     |                         Attributes                         | Member of 
    ------------------+------------------------------------------------------------+-----------
     postgres         | Superuser, Create role, Create DB, Replication, Bypass RLS | {}
     test-admin-user  | Superuser, No inheritance                                  | {}
     test-simple-user | No inheritance                                             | {}
    
    test_db=# 
    test_db=# 
    test_db=# select * from information_schema.table_privileges where grantee in ('test-admin-user','test-simple-user');
     grantor  |     grantee      | table_catalog | table_schema | table_name | privilege_type | is_grantable | with_hierarchy 
    ----------+------------------+---------------+--------------+------------+----------------+--------------+----------------
     postgres | test-simple-user | test_db       | public       | clients    | INSERT         | NO           | NO
     postgres | test-simple-user | test_db       | public       | clients    | SELECT         | NO           | YES
     postgres | test-simple-user | test_db       | public       | clients    | UPDATE         | NO           | NO
     postgres | test-simple-user | test_db       | public       | clients    | DELETE         | NO           | NO
     postgres | test-simple-user | test_db       | public       | orders     | INSERT         | NO           | NO
     postgres | test-simple-user | test_db       | public       | orders     | SELECT         | NO           | YES
     postgres | test-simple-user | test_db       | public       | orders     | UPDATE         | NO           | NO
     postgres | test-simple-user | test_db       | public       | orders     | DELETE         | NO           | NO
    (8 rows)
    
    test_db=# 
    test_db=# 
    test_db=# \d+
                                List of relations
     Schema |      Name      |   Type   |  Owner   |    Size    | Description 
    --------+----------------+----------+----------+------------+-------------
     public | clients        | table    | postgres | 8192 bytes | 
     public | clients_id_seq | sequence | postgres | 8192 bytes | 
     public | orders         | table    | postgres | 0 bytes    | 
     public | orders_id_seq  | sequence | postgres | 8192 bytes | 
    (4 rows)
    
    test_db=# \d+ clients
                                                             Table "public.clients"
      Column  |          Type          | Collation | Nullable |               Default               | Storage  | Stats target | Description 
    ----------+------------------------+-----------+----------+-------------------------------------+----------+--------------+-------------
     id       | integer                |           | not null | nextval('clients_id_seq'::regclass) | plain    |              | 
     lastname | character varying(255) |           |          |                                     | extended |              | 
     country  | character varying(255) |           |          |                                     | extended |              | 
     order_id | integer                |           |          |                                     | plain    |              | 
    Indexes:
        "clients_pkey" PRIMARY KEY, btree (id)
    Foreign-key constraints:
        "clients_order_id_fkey" FOREIGN KEY (order_id) REFERENCES orders(id)
    Access method: heap
    
    test_db=# \d+ orders 
                                                            Table "public.orders"
     Column |          Type          | Collation | Nullable |              Default               | Storage  | Stats target | Description 
    --------+------------------------+-----------+----------+------------------------------------+----------+--------------+-------------
     id     | integer                |           | not null | nextval('orders_id_seq'::regclass) | plain    |              | 
     name   | character varying(255) |           |          |                                    | extended |              | 
     price  | integer                |           |          |                                    | plain    |              | 
    Indexes:
        "orders_pkey" PRIMARY KEY, btree (id)
    Referenced by:
        TABLE "clients" CONSTRAINT "clients_order_id_fkey" FOREIGN KEY (order_id) REFERENCES orders(id)
    Access method: heap
    
    test_db=# 

### Задача 3.
Используя SQL синтаксис - наполните таблицы следующими тестовыми данными:

**Ответ:**

    test_db=# 
    test_db=# insert into orders VALUES (1, 'Шоколад', 10), \
                                        (2, 'Принтер', 3000), \
                                        (3, 'Книга', 500), \ 
                                        (4, 'Монитор', 7000), \
                                        (5, 'Гитара', 4000);
    INSERT 0 5
    test_db=# 
    test_db=# 
    test_db=# select * from orders;
     id |      name      | price 
    ----+----------------+-------
      1 | Шоколад |    10
      2 | Принтер |  3000
      3 | Книга   |   500
      4 | Монитор |  7000
      5 | Гитара  |  4000
    (5 rows)
    
    test_db=# 
    test_db=# insert into clients VALUES (1, 'Иванов Иван Иванович', 'USA'),\
                                         (2, 'Петров Петр Петрович', 'Canada'), \
                                         (3, 'Иоганн Себастьян Бах', 'Japan'), \
                                         (4, 'Ронни Джеймс Дио', 'Russia'), \
                                         (5, 'Ritchie Blackmore', 'Russia');
    INSERT 0 5
    test_db=# 
    test_db=# select * from clients;
     id |                lastname                | country | order_id 
    ----+----------------------------------------+---------+----------
      1 | Иванов Иван Иванович | USA     |         
      2 | Петров Петр Петрович | Canada  |         
      3 | Иоганн Себастьян Бах | Japan   |         
      4 | Ронни Джеймс Дио     | Russia  |         
      5 | Ritchie Blackmore    | Russia  |         
    (5 rows)
    
    test_db=# 
    test_db=# 
    test_db=# select count(*) from clients;
     count 
    -------
         5
    (1 row)
    
    test_db=# 
    test_db=# select count(*) from orders;
     count 
    -------
         5
    (1 row)
    
    test_db=# 

### Задача 4.
Часть пользователей из таблицы clients решили оформить заказы из таблицы orders.

Используя foreign keys свяжите записи из таблиц, согласно таблице:

    ФИО 	Заказ
    Иванов Иван Иванович 	Книга
    Петров Петр Петрович 	Монитор
    Иоганн Себастьян Бах 	Гитара

**Ответ:**

    test_db=# select * from clients;
     id |                lastname                | country | order_id 
    ----+----------------------------------------+---------+----------
      1 | Иванов Иван Иванович                   | USA     |         
      2 | Петров Петр Петрович                   | Canada  |         
      3 | Иоганн Себастьян Бах                   | Japan   |         
      4 | Ронни Джеймс Дио                       | Russia  |         
      5 | Ritchie Blackmore                      | Russia  |         
    (5 rows)
    
    test_db=# update clients set order_id=3 where id=1;
    UPDATE 1
    test_db=# update clients set order_id=4 where id=2;
    UPDATE 1
    test_db=# update clients set order_id=5 where id=3;
    UPDATE 1
    test_db=# 
    test_db=# select * from clients order by id;
     id |                lastname                | country | order_id 
    ----+----------------------------------------+---------+----------
      1 | Иванов Иван Иванович                   | USA     |        3
      2 | Петров Петр Петрович                   | Canada  |        4
      3 | Иоганн Себастьян Бах                   | Japan   |        5
      4 | Ронни Джеймс Дио                       | Russia  |         
      5 | Ritchie Blackmore                      | Russia  |         
    (5 rows)
    
    test_db=# select c.lastname,c.country,o.name as "order_name" from clients c, orders o where c.order_id=o.id;
                    lastname                | country |      order_name      
    ----------------------------------------+---------+----------------
     Иванов Иван Иванович                   | USA     | Книга
     Петров Петр Петрович                   | Canada  | Монитор
     Иоганн Себастьян Бах                   | Japan   | Гитара
    (3 rows)
    
    test_db=# 

### Задача 5.
Получите полную информацию по выполнению запроса выдачи всех пользователей из задачи 4 (используя директиву EXPLAIN).

Приведите получившийся результат и объясните что значат полученные значения.

**Ответ:**

    test_db=# 
    test_db=# explain select c.lastname,c.country,o.name as  "order_name" from clients c, orders o where c.order_id=o.id;
                                    QUERY PLAN                                
    --------------------------------------------------------------------------
     Hash Join  (cost=11.57..24.20 rows=70 width=1548)
       Hash Cond: (o.id = c.order_id)
       ->  Seq Scan on orders o  (cost=0.00..11.40 rows=140 width=520)
       ->  Hash  (cost=10.70..10.70 rows=70 width=1036)
             ->  Seq Scan on clients c  (cost=0.00..10.70 rows=70 width=1036)
    (5 rows)
    
    test_db=# 
    test_db=# 
    test_db=# explain select * from clients c where order_id is not null;
                              QUERY PLAN                          
    --------------------------------------------------------------
     Seq Scan on clients c  (cost=0.00..10.70 rows=70 width=1040)
       Filter: (order_id IS NOT NULL)
    (2 rows)
    
    test_db=# 
    test_db=# 
    test_db=# create index orders_indx on orders(id);
    CREATE INDEX
    test_db=# 
    test_db=# 
    test_db=# explain select c.lastname,c.country,o.name as  "order_name" from clients c, orders o where c.order_id=o.id;
                                  QUERY PLAN                              
    ----------------------------------------------------------------------
     Hash Join  (cost=1.11..12.01 rows=70 width=1548)
       Hash Cond: (c.order_id = o.id)
       ->  Seq Scan on clients c  (cost=0.00..10.70 rows=70 width=1036)
       ->  Hash  (cost=1.05..1.05 rows=5 width=520)
             ->  Seq Scan on orders o  (cost=0.00..1.05 rows=5 width=520)
    (5 rows)
    
    test_db=# 
    test_db=# create index clients_indx on clients(order_id);
    CREATE INDEX
    test_db=# 
    test_db=# 
    test_db=# explain select c.lastname,c.country,o.name as  "order_name" from clients c, orders o where c.order_id=o.id;
                                  QUERY PLAN                              
    ----------------------------------------------------------------------
     Hash Join  (cost=1.11..2.19 rows=5 width=1548)
       Hash Cond: (c.order_id = o.id)
       ->  Seq Scan on clients c  (cost=0.00..1.05 rows=5 width=1036)
       ->  Hash  (cost=1.05..1.05 rows=5 width=520)
             ->  Seq Scan on orders o  (cost=0.00..1.05 rows=5 width=520)
    (5 rows)
    
    test_db=# 
    test_db=# 
    test_db=# explain select * from clients c where order_id is not null;
                             QUERY PLAN                         
    ------------------------------------------------------------
     Seq Scan on clients c  (cost=0.00..1.05 rows=5 width=1040)
       Filter: (order_id IS NOT NULL)
    (2 rows)
    
    test_db=# 

Когда вначале запустили выборку на поиск всех пользователей с заказами со связкой с заказами и нет, 
то кол-во записей с выборками происходил полным сканированием таблиц, что отражено в атрибуте rows

       ->  Seq Scan on orders o  (cost=0.00..11.40 rows=140 width=520)
       ->  Hash  (cost=10.70..10.70 rows=70 width=1036)
             ->  Seq Scan on clients c  (cost=0.00..10.70 rows=70 width=1036)

после создания индекса по полю clients(order_id) и orders(id) результат
    
    ->  Seq Scan on clients c  (cost=0.00..1.05 rows=5 width=1036)
           ->  Hash  (cost=1.05..1.05 rows=5 width=520)
                 ->  Seq Scan on orders o  (cost=0.00..1.05 rows=5 width=520)

Что говорит о том, что теперь идет попадание в индекс и атрибут rows заметно упал, и благодаря индексу cost запроса упал

Также и с выборкой clients по полю order_id. Rows и Cost заметно упали, даже несмотря на малое количество записей

до:    
    
     Seq Scan on clients c  (cost=0.00..10.70 rows=70 width=1040)
       Filter: (order_id IS NOT NULL)

после:

     Seq Scan on clients c  (cost=0.00..1.05 rows=5 width=1040)
       Filter: (order_id IS NOT NULL)


### Задача 5.
Создайте бэкап БД test_db и поместите его в volume, предназначенный для бэкапов (см. Задачу 1).

Остановите контейнер с PostgreSQL (но не удаляйте volumes).

Поднимите новый пустой контейнер с PostgreSQL.

Восстановите БД test_db в новом контейнере.

Приведите список операций, который вы применяли для бэкапа данных и восстановления.

**Ответ:**

    lex@lex sudo docker run --rm --name pg-docker -e POSTGRES_PASSWORD=postgres  -d  -v vol1:/var/lib/postgresql/data -v vol2:/var/lib/postgresql postgres:12
    f29c0ac55c3dd7155c302f0e6c67a9572b0ab298fc5ade3491c5bcaf2b93b102
    lex@lex sudo docker exec -t pg-docker pg_dump -U postgres -C test_db -f /var/lib/postgresql/dump_test.sql
    lex@lex sudo docker stop f29c0ac55c3dd7155c302f0e6c67a9572b0ab298fc5ade3491c5bcaf2b93b102
    f29c0ac55c3dd7155c302f0e6c67a9572b0ab298fc5ade3491c5bcaf2b93b102
    lex@lex 

Запускаем вторую ВМ только с vol2    

    lex@lex sudo docker run --rm --name pg-docker2 -e POSTGRES_PASSWORD=postgres  -d  -v vol2:/var/lib/postgresql postgres:12
    735dd35d3937c0076fde931de9a3e17f7d66b2f77f3b8b76bacf262b61fcb833
    lex@lex  ~ $ sudo docker exec -it pg-docker2 bash
    root@735dd35d3937:/# 
    root@735dd35d3937:/# 
    root@735dd35d3937:/# su - postgres
    postgres@735dd35d3937:~$ \l
    -bash: l: command not found
    postgres@735dd35d3937:~$ psql
    psql (12.12 (Debian 12.12-1.pgdg110+1))
    Type "help" for help.
    
    postgres=# \l
                                     List of databases
       Name    |  Owner   | Encoding |  Collate   |   Ctype    |   Access privileges   
    -----------+----------+----------+------------+------------+-----------------------
     postgres  | postgres | UTF8     | en_US.utf8 | en_US.utf8 | 
     template0 | postgres | UTF8     | en_US.utf8 | en_US.utf8 | =c/postgres          +
               |          |          |            |            | postgres=CTc/postgres
     template1 | postgres | UTF8     | en_US.utf8 | en_US.utf8 | =c/postgres          +
               |          |          |            |            | postgres=CTc/postgres
    (3 rows)
    
    postgres=# 
    postgres=# exit
    postgres@735dd35d3937:~$ exit
    logout
    root@735dd35d3937:/# exit
    exit
Тут убедились, что базы test_db нет еще
Теперь, выполняем восстановление

    lex@lex  ~ $ sudo docker exec -i pg-docker2 psql -U postgres  -f /var/lib/postgresql/dump_test.sql
    SET
    SET
    SET
    SET
    SET
     set_config 
    ------------
     
    (1 row)
    
    SET
    SET
    SET
    SET
    CREATE DATABASE
    ALTER DATABASE
    You are now connected to database "test_db" as user "postgres".
    SET
    SET
    SET
    SET
    SET
     set_config 
    ------------
     
    (1 row)
    
    SET
    SET
    SET
    SET
    SET
    SET
    CREATE TABLE
    ALTER TABLE
    CREATE SEQUENCE
    ALTER TABLE
    ALTER SEQUENCE
    CREATE TABLE
    ALTER TABLE
    CREATE SEQUENCE
    ALTER TABLE
    ALTER SEQUENCE
    ALTER TABLE
    ALTER TABLE
    COPY 5
    COPY 5
     setval 
    --------
          1
    (1 row)
    
     setval 
    --------
          1
    (1 row)
    
    ALTER TABLE
    ALTER TABLE
    CREATE INDEX
    CREATE INDEX
    ALTER TABLE
    lex@lex  ~ $

Теперь войдем в ВМ и убедимся, что test_db имеется

    lex@lex  ~ $ sudo docker exec -it pg-docker2 bash
    root@735dd35d3937:/# 
    root@735dd35d3937:/# 
    root@735dd35d3937:/# su - postgres
    postgres@735dd35d3937:~$ psql
    psql (12.12 (Debian 12.12-1.pgdg110+1))
    Type "help" for help.
    
    postgres=# 
    postgres=# 
    postgres=# \l
                                     List of databases
       Name    |  Owner   | Encoding |  Collate   |   Ctype    |   Access privileges   
    -----------+----------+----------+------------+------------+-----------------------
     postgres  | postgres | UTF8     | en_US.utf8 | en_US.utf8 | 
     template0 | postgres | UTF8     | en_US.utf8 | en_US.utf8 | =c/postgres          +
               |          |          |            |            | postgres=CTc/postgres
     template1 | postgres | UTF8     | en_US.utf8 | en_US.utf8 | =c/postgres          +
               |          |          |            |            | postgres=CTc/postgres
     test_db   | postgres | UTF8     | en_US.utf8 | en_US.utf8 | 
    (4 rows)
    
    postgres=# 
    postgres=# exit;
    postgres@735dd35d3937:~$ exit
    logout
    root@735dd35d3937:/# exit
    exit
    lex@lex  ~ $ 
   