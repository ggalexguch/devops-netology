### Задача 1.
Используя docker поднимите инстанс MySQL (версию 8). Данные БД сохраните в volume.
Изучите бэкап БД и восстановитесь из него.
Перейдите в управляющую консоль mysql внутри контейнера.
Используя команду \h получите список управляющих команд.
Найдите команду для выдачи статуса БД и приведите в ответе из ее вывода версию сервера БД.
Подключитесь к восстановленной БД и получите список таблиц из этой БД.
Приведите в ответе количество записей с price > 300.

В следующих заданиях мы будем продолжать работу с данным контейнером.

**Ответ:**

    lex@lex  ~ $ sudo docker pull mysql:8.0
    8.0: Pulling from library/mysql
    Digest: sha256:ce2ae3bd3e9f001435c4671cf073d1d5ae55d138b16927268474fc54ba09ed79
    Status: Image is up to date for mysql:8.0
    lex@lex  ~ $  docker volume create volume_mysql
    volume_mysql
    lex@lex  ~ $ sudo docker run --rm --name mysql-docker -e MYSQL_ROOT_PASSWORD=root -ti -p 3306:3306 -d -v volume_mysql:/etc/mysql/ mysql:8.0
    19c6d2588775
    
Поместил скачанный бэкап в volume_mysql
Входим в контейнер и создаем БД test_db;
Восстанавливаем из бэкапа
    
    bash-4.4# mysql -u root -p
    Enter password: 
    Welcome to the MySQL monitor.  Commands end with ; or \g.
    Your MySQL connection id is 14
    Server version: 8.0.30 MySQL Community Server - GPL
    
    Copyright (c) 2000, 2022, Oracle and/or its affiliates.
    
    Oracle is a registered trademark of Oracle Corporation and/or its
    affiliates. Other names may be trademarks of their respective
    owners.
    
    Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.
    
    mysql> 
    mysql> create database test_db;
    Query OK, 1 row affected (0.01 sec)
    
    mysql> show databases;
    +--------------------+
    | Database           |
    +--------------------+
    | information_schema |
    | mysql              |
    | performance_schema |
    | sys                |
    +--------------------+
    4 rows in set (0.00 sec)
    
    mysql> quit
    Bye
    bash-4.4# mysql -u root -p < /etc/mysql/test_dump.sql test_db
    Enter password: 
    bash-4.4# mysql -u root -p
    Enter password: 
    Welcome to the MySQL monitor.  Commands end with ; or \g.
    Your MySQL connection id is 14
    Server version: 8.0.30 MySQL Community Server - GPL
    
    Copyright (c) 2000, 2022, Oracle and/or its affiliates.
    
    Oracle is a registered trademark of Oracle Corporation and/or its
    affiliates. Other names may be trademarks of their respective
    owners.
    
    Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.
    
    mysql> show databases;
    +--------------------+
    | Database           |
    +--------------------+
    | information_schema |
    | mysql              |
    | performance_schema |
    | sys                |
    | test_db            |
    +--------------------+
    5 rows in set (0.01 sec)
    
    mysql> show tables;
    +-------------------+
    | Tables_in_test_db |
    +-------------------+
    | orders            |
    +-------------------+
    1 row in set (0.00 sec)
    
    mysql> select * from orders;
    +----+-----------------------+-------+
    | id | title                 | price |
    +----+-----------------------+-------+
    |  1 | War and Peace         |   100 |
    |  2 | My little pony        |   500 |
    |  3 | Adventure mysql times |   300 |
    |  4 | Server gravity falls  |   300 |
    |  5 | Log gossips           |   123 |
    +----+-----------------------+-------+
    5 rows in set (0.00 sec)
    
    mysql> 
    mysql> 
    mysql> select * from orders where price>300;
    +----+----------------+-------+
    | id | title          | price |
    +----+----------------+-------+
    |  2 | My little pony |   500 |
    +----+----------------+-------+
    1 row in set (0.00 sec)
    
    mysql> 

### Задача 2.
Создайте пользователя test в БД c паролем test-pass, используя:

* плагин авторизации mysql_native_password
* срок истечения пароля - 180 дней
* количество попыток авторизации - 3
* максимальное количество запросов в час - 100
* аттрибуты пользователя:
Фамилия "Pretty"
Имя "James"

Предоставьте привелегии пользователю test на операции SELECT базы test_db.

Используя таблицу INFORMATION_SCHEMA.USER_ATTRIBUTES получите данные по пользователю test и приведите в ответе к задаче.

**Ответ:**

    mysql> CREATE USER test@localhost IDENTIFIED BY 'test-pass';
    Query OK, 0 rows affected (0.02 sec)
    
    mysql> 
    mysql> ALTER USER 'test'@'localhost' ATTRIBUTE '{"fname":"James", "lname":"Pretty"}';
    Query OK, 0 rows affected (0.01 sec)
    
    mysql> 
    mysql> 
    mysql> ALTER USER 'test'@'localhost' IDENTIFIED BY 'test-pass' WITH MAX_QUERIES_PER_HOUR 100 PASSWORD EXPIRE INTERVAL 180 DAY FAILED_LOGIN_ATTEMPTS 3 PASSWORD_LOCK_TIME 2;
    Query OK, 0 rows affected (0.02 sec)
    
    mysql> 
    mysql> 
    mysql> GRANT Select ON test_db.orders TO 'test'@'localhost';
    Query OK, 0 rows affected, 1 warning (0.00 sec)
    
    mysql> 
    mysql> 
    mysql> 
    mysql> SELECT * FROM INFORMATION_SCHEMA.USER_ATTRIBUTES WHERE USER='test';
    +------+-----------+---------------------------------------+
    | USER | HOST      | ATTRIBUTE                             |
    +------+-----------+---------------------------------------+
    | test | localhost | {"fname": "James", "lname": "Pretty"} |
    +------+-----------+---------------------------------------+
    1 row in set (0.00 sec)
    
    mysql> 

### Задача 3.
Установите профилирование SET profiling = 1. Изучите вывод профилирования команд SHOW PROFILES;.

Исследуйте, какой engine используется в таблице БД test_db и приведите в ответе.

Измените engine и приведите время выполнения и запрос на изменения из профайлера в ответе:

    на MyISAM
    на InnoDB

**Ответ:**

    mysql> set profiling=1;
    Query OK, 0 rows affected, 1 warning (0.00 sec)
    
    mysql> 
    mysql> SELECT TABLE_NAME,ENGINE,ROW_FORMAT,TABLE_ROWS FROM information_schema.TABLES WHERE table_name = 'orders' and  TABLE_SCHEMA = 'test_db' ORDER BY ENGINE asc;
    +------------+--------+------------+------------+
    | TABLE_NAME | ENGINE | ROW_FORMAT | TABLE_ROWS |
    +------------+--------+------------+------------+
    | orders     | InnoDB | Dynamic    |          5 |
    +------------+--------+------------+------------+
    1 row in set (0.00 sec)
    
    mysql> 
    mysql> ALTER TABLE orders ENGINE = MyISAM;
    Query OK, 5 rows affected (0.08 sec)
    Records: 5  Duplicates: 0  Warnings: 0
    
    mysql> 
    mysql> 
    mysql> ALTER TABLE orders ENGINE = InnoDB;
    Query OK, 5 rows affected (0.09 sec)
    Records: 5  Duplicates: 0  Warnings: 0
    
    mysql> mysql> show profiles;
    +----------+------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------+
    | Query_ID | Duration   | Query                                                                                                                                                       |
    +----------+------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------+
    |        1 | 0.06965500 | ALTER TABLE orders ENGINE = MyISAM                                                                                                                          |
    |        2 | 0.08631700 | ALTER TABLE orders ENGINE = InnoDB                                                                                                                          |
    +----------+------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------+
    2 rows in set, 1 warning (0.00 sec)
    
Переключение на MyISAM: 0.06965500 сек
Переключение на InnoDB: 0.08631700 сек

### Задача 4.
Изучите файл my.cnf в директории /etc/mysql.

Измените его согласно ТЗ (движок InnoDB):

    Скорость IO важнее сохранности данных
    Нужна компрессия таблиц для экономии места на диске
    Размер буффера с незакомиченными транзакциями 1 Мб
    Буффер кеширования 30% от ОЗУ
    Размер файла логов операций 100 Мб

Приведите в ответе измененный файл my.cnf.

**Ответ:**

Исходный файл

    bash-4.4# cat /etc/my.cnf
    # For advice on how to change settings please see
    # http://dev.mysql.com/doc/refman/8.0/en/server-configuration-defaults.html
    
    [mysqld]
    #
    # Remove leading # and set to the amount of RAM for the most important data
    # cache in MySQL. Start at 70% of total RAM for dedicated server, else 10%.
    # innodb_buffer_pool_size = 128M
    #
    # Remove leading # to turn on a very important data integrity option: logging
    # changes to the binary log between backups.
    # log_bin
    #
    # Remove leading # to set options mainly useful for reporting servers.
    # The server defaults are faster for transactions and fast SELECTs.
    # Adjust sizes as needed, experiment to find the optimal values.
    # join_buffer_size = 128M
    # sort_buffer_size = 2M
    # read_rnd_buffer_size = 2M
    
    # Remove leading # to revert to previous value for default_authentication_plugin,
    # this will increase compatibility with older clients. For background, see:
    # https://dev.mysql.com/doc/refman/8.0/en/server-system-variables.html#sysvar_default_authentication_plugin
    # default-authentication-plugin=mysql_native_password
    skip-host-cache
    skip-name-resolve
    datadir=/var/lib/mysql
    socket=/var/run/mysqld/mysqld.sock
    secure-file-priv=/var/lib/mysql-files
    user=mysql
    
    pid-file=/var/run/mysqld/mysqld.pid
    [client]
    socket=/var/run/mysqld/mysqld.sock
    
    !includedir /etc/mysql/conf.d/
    bash-4.4#  

После изменения

    bash-4.4# cat /etc/my.cnf
    # For advice on how to change settings please see
    # http://dev.mysql.com/doc/refman/8.0/en/server-configuration-defaults.html
    
    [mysqld]
    #
    # Remove leading # and set to the amount of RAM for the most important data
    # cache in MySQL. Start at 70% of total RAM for dedicated server, else 10%.
    # innodb_buffer_pool_size = 128M
    #
    # Remove leading # to turn on a very important data integrity option: logging
    # changes to the binary log between backups.
    # log_bin
    #
    # Remove leading # to set options mainly useful for reporting servers.
    # The server defaults are faster for transactions and fast SELECTs.
    # Adjust sizes as needed, experiment to find the optimal values.
    # join_buffer_size = 128M
    # sort_buffer_size = 2M
    # read_rnd_buffer_size = 2M
    
    # Remove leading # to revert to previous value for default_authentication_plugin,
    # this will increase compatibility with older clients. For background, see:
    # https://dev.mysql.com/doc/refman/8.0/en/server-system-variables.html#sysvar_default_authentication_plugin
    # default-authentication-plugin=mysql_native_password
    skip-host-cache
    skip-name-resolve
    datadir=/var/lib/mysql
    socket=/var/run/mysqld/mysqld.sock
    secure-file-priv=/var/lib/mysql-files
    user=mysql
    
    pid-file=/var/run/mysqld/mysqld.pid
    
    # Changes by Alex
    innodb_flush_log_at_trx_commit = 0 
    innodb_file_format=Barracuda
    innodb_log_buffer_size	= 1M
    key_buffer_size = 640М
    max_binlog_size	= 100M
    # Changes by Alex    

    [client]
    socket=/var/run/mysqld/mysqld.sock
    
    !includedir /etc/mysql/conf.d/
    
    bash-4.4#  
