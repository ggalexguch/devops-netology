### Задача 1.
Используя docker поднимите инстанс PostgreSQL (версию 13). Данные БД сохраните в volume.

Подключитесь к БД PostgreSQL используя psql.

Воспользуйтесь командой \? для вывода подсказки по имеющимся в psql управляющим командам.

Найдите и приведите управляющие команды для:

    вывода списка БД
    подключения к БД
    вывода списка таблиц
    вывода описания содержимого таблиц
    выхода из psql

**Ответ:**

    lex@lex  ~ $ 
    lex@lex  ~ $ sudo docker pull postgres:13.0
    13.0: Pulling from library/postgres
    bb79b6b2107f: Pull complete 
    e3dc51fa2b56: Pull complete 
    f213b6f96d81: Pull complete 
    2780ac832fde: Pull complete 
    ae5cee1a3f12: Pull complete 
    95db3c06319e: Pull complete 
    475ca72764d5: Pull complete 
    8d602872ecae: Pull complete 
    c4fca31f2e3d: Pull complete 
    a00c442835e0: Pull complete 
    2e2305af3390: Pull complete 
    6cff852bb872: Pull complete 
    25bb0be11543: Pull complete 
    4738c099c4ad: Pull complete 
    Digest: sha256:8f7c3c9b61d82a4a021da5d9618faf056633e089302a726d619fa467c73609e4
    Status: Downloaded newer image for postgres:13.0
    lex@lex  ~ $ 
    lex@lex  ~ $ 
    lex@lex  ~ $ sudo docker volume create volpg13
    volpg13
    lex@lex  ~ $ 
    lex@lex  ~ $ sudo docker run --rm --name pg13-docker -e POSTGRES_PASSWORD=postgres -d -v volpg13:/var/lib/postgresql/data postgres:13 
    336e89bea4436c2afcbb9a267d6f0605e08cd5b875a5a03fc236f398dc70d266
    lex@lex  ~ $ 
    lex@lex  ~ $ sudo docker exec -it pg13-docker bash
    root@336e89bea443:/# 
    root@336e89bea443:/# su - postgres
    postgres@336e89bea443:~$ 
    postgres@336e89bea443:~$ psql
    psql (13.8 (Debian 13.8-1.pgdg110+1))
    Type "help" for help.
    
    postgres=# 
    postgres=# \l
                                     List of databases
       Name    |  Owner   | Encoding |  Collate   |   Ctype    |   Access privileges   
    -----------+----------+----------+------------+------------+-----------------------
     postgres  | postgres | UTF8     | en_US.utf8 | en_US.utf8 | 
     template0 | postgres | UTF8     | en_US.utf8 | en_US.utf8 | =c/postgres          +
               |          |          |            |            | postgres=CTc/postgres
     template1 | postgres | UTF8     | en_US.utf8 | en_US.utf8 | =c/postgres          +
               |          |          |            |            | postgres=CTc/postgres
    (3 rows)
    
    postgres=# 
    postgres=# 
    postgres=# \c postgres
    You are now connected to database "postgres" as user "postgres".
    postgres=# 
    postgres=# \conninfo
    You are connected to database "postgres" as user "postgres" via socket in "/var/run/postgresql" at port "5432".
    postgres=# 
    postgres=# \dt
    Did not find any relations.
    postgres=# \dtS 
                        List of relations
       Schema   |          Name           | Type  |  Owner   
    ------------+-------------------------+-------+----------
     pg_catalog | pg_aggregate            | table | postgres
     pg_catalog | pg_am                   | table | postgres
     pg_catalog | pg_amop                 | table | postgres
     pg_catalog | pg_amproc               | table | postgres
     pg_catalog | pg_attrdef              | table | postgres
     pg_catalog | pg_attribute            | table | postgres
     pg_catalog | pg_auth_members         | table | postgres
     ...  
    postgres=# 
    postgres=# 
    postgres=# \dtS pg_database;
                  List of relations
       Schema   |    Name     | Type  |  Owner   
    ------------+-------------+-------+----------
     pg_catalog | pg_database | table | postgres
    (1 row)
    
    postgres=# 
    postgres-# \q
    postgres@336e89bea443:~$ exit
    logout
    root@336e89bea443:/# exit
    exit
    lex@lex  ~ $ 

### Задача 2.
Используя psql создайте БД test_database.

* Изучите бэкап БД.
* Восстановите бэкап БД в test_database.
* Перейдите в управляющую консоль psql внутри контейнера.
* Подключитесь к восстановленной БД и проведите операцию ANALYZE для сбора статистики по таблице.
* Используя таблицу pg_stats, найдите столбец таблицы orders с наибольшим средним значением размера элементов в байтах.
* Приведите в ответе команду, которую вы использовали для вычисления и полученный результат.

**Ответ:**

Скачал бэкап и поместил в volume
Восстановил из бэкапа, предварительно создав БД

    postgres=# create database test_database;
    CREATE DATABASE
    postgres=# exit
    root@336e89bea443:/# psql -U postgres -f ./test_dump.sql test_database
    SET
    SET
    SET
    SET
    SET
     set_config 
    ------------
     
    (1 row)
    
    SET
    SET
    SET
    SET
    SET
    SET
    CREATE TABLE
    ALTER TABLE
    CREATE SEQUENCE
    ALTER TABLE
    ALTER SEQUENCE
    ALTER TABLE
    COPY 8
     setval 
    --------
          8
    (1 row)
    
    ALTER TABLE
    root@336e89bea443:/# su - postgres
    postgres@336e89bea443:~$ psql test_database;
    psql (13.8 (Debian 13.8-1.pgdg110+1))
    Type "help" for help.
    
    test_database=# 
    test_database=# \dt
             List of relations
     Schema |  Name  | Type  |  Owner   
    --------+--------+-------+----------
     public | orders | table | postgres
    (1 row)
    
    test_database=# 
    test_database=# vacuum (VERBOSE,ANALYZE) orders;
    INFO:  vacuuming "public.orders"
    INFO:  "orders": found 0 removable, 8 nonremovable row versions in 1 out of 1 pages
    DETAIL:  0 dead row versions cannot be removed yet, oldest xmin: 496
    There were 0 unused item identifiers.
    Skipped 0 pages due to buffer pins, 0 frozen pages.
    0 pages are entirely empty.
    CPU: user: 0.00 s, system: 0.00 s, elapsed: 0.00 s.
    INFO:  analyzing "public.orders"
    INFO:  "orders": scanned 1 of 1 pages, containing 8 live rows and 0 dead rows; 8 rows in sample, 8 estimated total rows
    VACUUM
    test_database=# 
    test_database=# select attname,avg_width from pg_stats where tablename='orders';
     attname | avg_width 
    ---------+-----------
     id      |         4
     title   |        16
     price   |         4
    (3 rows)
    
    test_database=# select attname,avg_width from pg_stats s where tablename='orders' \ 
                            and avg_width=(select max(avg_width) from pg_stats s2 \ 
                                            where s2.tablename=s.tablename); 
    attname | avg_width 
    ---------+-----------
     title   |        16
    (1 row)
    
    test_database=# 

### Задача 3.
Архитектор и администратор БД выяснили, что ваша таблица orders разрослась до невиданных размеров и поиск по ней занимает долгое время. Вам, как успешному выпускнику курсов DevOps в нетологии предложили провести разбиение таблицы на 2 (шардировать на orders_1 - price>499 и orders_2 - price<=499).

Предложите SQL-транзакцию для проведения данной операции.

Можно ли было изначально исключить "ручное" разбиение при проектировании таблицы orders?

**Ответ:**

    test_database=# 
    test_database=# alter table orders rename to renamed_orders;
    ALTER TABLE
    test_database=# 
    test_database=# 
    test_database=# create table orders (id integer not null, title varchar(80), price integer default 0) partition by range(price);
    CREATE TABLE
    test_database=# 
    test_database=# 
    test_database=# create table orders_2 partition of orders for values from (0) to (499);
    CREATE TABLE
    test_database=# 
    test_database=# 
    test_database=# create table orders_1 partition of orders for values from (499) to (9999999);
    CREATE TABLE
    test_database=# 
    test_database=# 
    test_database=# insert into orders (id, title, price) select * from renamed_orders;
    INSERT 0 8
    test_database=# 
    test_database=# 

Проверим, что данные поместились в нужные партиции
   
     test_database=# select * from orders_1;
     id |       title        | price 
    ----+--------------------+-------
      2 | My little database |   500
      6 | WAL never lies     |   900
      7 | Me and my bash-pet |   499
      8 | Dbiezdmin          |   501
    (4 rows)
    
    test_database=# select * from orders_2;
     id |        title         | price 
    ----+----------------------+-------
      1 | War and peace        |   100
      3 | Adventure psql time  |   300
      4 | Server gravity falls |   300
      5 | Log gossips          |   123
    (4 rows)
    
Если выбрать из orders без условия, сканируются все партиции

    test_database=# explain select * from orders;            
                                     QUERY PLAN                                 
    ----------------------------------------------------------------------------
     Append  (cost=0.00..31.40 rows=760 width=186)
       ->  Seq Scan on orders_2 orders_1  (cost=0.00..13.80 rows=380 width=186)
       ->  Seq Scan on orders_1 orders_2  (cost=0.00..13.80 rows=380 width=186)
    (3 rows)

Если укажем конкретное значение, то обращение будет в рамках нужной партиции

    test_database=# explain select * from orders where price=300;
                                QUERY PLAN                            
    ------------------------------------------------------------------
     Seq Scan on orders_2 orders  (cost=0.00..14.75 rows=2 width=186)
       Filter: (price = 300)
    (2 rows)
    
    test_database=# explain select * from orders where price=500;
                                QUERY PLAN                            
    ------------------------------------------------------------------
     Seq Scan on orders_1 orders  (cost=0.00..14.75 rows=2 width=186)
       Filter: (price = 500)
    (2 rows)
    
    test_database=# 
    test_database=# 

Думаю если с самого начала таблица хотябы была секционирована, то добавление новых партиций
можно производить "на лету", а так я думаю реализовать можно только вышеуказанным способом.

### Задача 4.
Используя утилиту pg_dump создайте бекап БД test_database.

Как бы вы доработали бэкап-файл, чтобы добавить уникальность значения столбца title для таблиц test_database?

**Ответ:**

    Вопрос немного не понял по поводу доработки бэкапа для добавления уникальности значения столбца title.
    Ключей у утилиты pg_dump для добавления уникальности значений таблицы в выгружаемый файл я не нашел.
    Возможно имелось ввиду добавить ограничение уникальности на таблицу title в самой БД, а потом сформировать бэкап.
    Так и представлю решение:

    test_database=# 
    test_database=# 
    test_database=# create unique index orders_title_ux on orders(lower(title));
    ERROR:  unique constraint on partitioned table must include all partitioning columns
    DETAIL:  UNIQUE constraint on table "orders" lacks column "price" which is part of the partition key.
    test_database=# 

Нельзя сделать уникальность по полю title на основную таблицу, поэтому нужно создать уникальность, для каждой партиции

    test_database=# create unique index orders_title_ux on orders_1(lower(title));
    CREATE INDEX
    test_database=# 
    test_database=# create unique index orders2_title_ux on orders_2(lower(title));
    CREATE INDEX
    test_database=# 
    test_database=# drop table renamed_orders;
    DROP TABLE
    test_database=# 
    test_database=# exit;
    postgres@336e89bea443:~$ exit
    root@336e89bea443:/var/lib/postgresql/data# pg_dump -U postgres -d test_database >test_database_dump.sql
    root@336e89bea443:/var/lib/postgresql/data#

    Вариант2:
    Создать ограничение уровня таблицы
    Согласно раздела 5.11.2.3. Limitations документации DDL-PARTITIONING-DECLARATIVE
    * Unique constraints (and hence primary keys) on partitioned tables must include all the partition key columns. This limitation exists because the individual indexes making up the constraint can only directly enforce uniqueness within their own partitions; therefore, the partition structure itself must guarantee that there are not duplicates in different partitions.
    т.е уникальный индекс нужно создать совместно с колонками партиционирования, в нашем случае (title,price) 

    test_database=# ALTER TABLE orders ADD CONSTRAINT uk_orders UNIQUE (title,price);
    ALTER TABLE
    test_database=# 

    Но данный метод ограничивает только в рамках партиции, но не всей таблицы.
    т.е я спокойно вставил запись с таким же title но в другую партицию
    test_database=# select * from orders;
     id |        title         | price 
    ----+----------------------+-------
      1 | War and peace        |   100
      3 | Adventure psql time  |   300
      4 | Server gravity falls |   300
      5 | Log gossips          |   123
      2 | My little database   |   500
      6 | WAL never lies       |   900
      7 | Me and my bash-pet   |   499
      8 | Dbiezdmin            |   501
    (8 rows)

    insert into orders values(9,'Dbiezdmin',400);

    test_database=# select * from orders order by id;
     id |        title         | price 
    ----+----------------------+-------
      1 | War and peace        |   100
      2 | My little database   |   500
      3 | Adventure psql time  |   300
      4 | Server gravity falls |   300
      5 | Log gossips          |   123
      6 | WAL never lies       |   900
      7 | Me and my bash-pet   |   499
      8 | Dbiezdmin            |   501
      9 | Dbiezdmin            |   400
    (9 rows)
    Конечно так себе решение.

    Вариант 3.
    Это создать триггер для таблиы orders, и там добавить проверку на уникальность. Но это еще хуже решение, так как триггеры 
    замедляют бизнес-логику.



