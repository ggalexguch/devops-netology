### Задача 1.
В этом задании вы потренируетесь в:

    установке elasticsearch
    первоначальном конфигурировании elastcisearch
    запуске elasticsearch в docker

Используя докер образ centos:7 как базовый и документацию по установке и запуску Elastcisearch:

    составьте Dockerfile-манифест для elasticsearch
    соберите docker-образ и сделайте push в ваш docker.io репозиторий
    запустите контейнер из получившегося образа и выполните запрос пути / c хост-машины

Требования к elasticsearch.yml:

    данные path должны сохраняться в /var/lib
    имя ноды должно быть netology_test

В ответе приведите:

    текст Dockerfile манифеста
    ссылку на образ в репозитории dockerhub
    ответ elasticsearch на запрос пути / в json виде

Подсказки:

    возможно вам понадобится установка пакета perl-Digest-SHA для корректной работы пакета shasum
    при сетевых проблемах внимательно изучите кластерные и сетевые настройки в elasticsearch.yml
    при некоторых проблемах вам поможет docker директива ulimit
    elasticsearch в логах обычно описывает проблему и пути ее решения

Далее мы будем работать с данным экземпляром elasticsearch.

**Ответ:**

    По инструкции для 8 версии elasticsearch, у меня скачанный образ ломался на запуске, выдавая ошибку доступа к папке 
    где находится встроенный jdk. Долго разбираться времени не было, выполнил на 7 версии, конечно если не критично

****
    Dockerfile:


    FROM centos:7
    
    ENV PATH=/usr/lib:$PATH
    
    RUN rpm --import https://artifacts.elastic.co/GPG-KEY-elasticsearch
    
    RUN echo "[elasticsearch]" >>/etc/yum.repos.d/elasticsearch.repo &&\
        echo "name=Elasticsearch repository for 7.x packages" >>/etc/yum.repos.d/elasticsearch.repo &&\
        echo "baseurl=https://artifacts.elastic.co/packages/7.x/yum">>/etc/yum.repos.d/elasticsearch.repo &&\
        echo "gpgcheck=1">>/etc/yum.repos.d/elasticsearch.repo &&\
        echo "gpgkey=https://artifacts.elastic.co/GPG-KEY-elasticsearch">>/etc/yum.repos.d/elasticsearch.repo &&\
        echo "enabled=0">>/etc/yum.repos.d/elasticsearch.repo &&\
        echo "autorefresh=1">>/etc/yum.repos.d/elasticsearch.repo &&\
        echo "type=rpm-md">>/etc/yum.repos.d/elasticsearch.repo 
    
    RUN yum install -y --enablerepo=elasticsearch elasticsearch 
    ADD elasticsearch.yml /etc/elasticsearch/
    RUN mkdir /usr/share/elasticsearch/snapshots &&\
        chown elasticsearch:elasticsearch /usr/share/elasticsearch/snapshots
    RUN mkdir /var/lib/logs \
        && chown elasticsearch:elasticsearch /var/lib/logs \
        && mkdir /var/lib/data \
        && chown elasticsearch:elasticsearch /var/lib/data
    
    USER elasticsearch
    CMD ["/usr/sbin/init"]
    CMD ["/usr/share/elasticsearch/bin/elasticsearch"]

****

    elasticsearch.yml   

    cluster.name: netology_test
    discovery.type: single-node
    path.data: /var/lib/data
    path.logs: /var/lib/logs
    path.repo: /usr/share/elasticsearch/snapshots
    network.host: 0.0.0.0
    discovery.seed_hosts: ["127.0.0.1", "[::1]"]

****

    lex@lex ~ $ curl localhost:9200
    {
      "name" : "2bb5e372dc95",
      "cluster_name" : "netology_test",
      "cluster_uuid" : "lVp38WSASyud_VnQxrGN7w",
      "version" : {
        "number" : "7.17.5",
        "build_flavor" : "default",
        "build_type" : "rpm",
        "build_hash" : "8d61b4f7ddf931f219e3745f295ed2bbc50c8e84",
        "build_date" : "2022-06-23T21:57:28.736740635Z",
        "build_snapshot" : false,
        "lucene_version" : "8.11.1",
        "minimum_wire_compatibility_version" : "6.8.0",
        "minimum_index_compatibility_version" : "6.0.0-beta1"
      },
      "tagline" : "You Know, for Search"
    }
    lex@lex ~ $ 

Ссылка на образ: https://hub.docker.com/repository/docker/dckralx/devops-virt15-elastic


### Задача 2.


**Ответ:**

    lex@lex ~ $ curl -X GET 'http://localhost:9200/_cat/indices?v'
    health status index            uuid                   pri rep docs.count docs.deleted store.size pri.store.size
    green  open   .geoip_databases MecKaRMcQiindKXaWApguA   1   0         41            0     38.8mb         38.8mb
    lex@lex ~ $ 
    lex@lex ~ $ curl -X PUT localhost:9200/ind-1 -H 'Content-Type: application/json' -d'{ "settings": { "number_of_shards": 1,  "number_of_replicas": 0 }}'
    {"acknowledged":true,"shards_acknowledged":true,"index":"ind-1"}
    lex@lex ~ $ 
    lex@lex ~ $ curl -X PUT localhost:9200/ind-2 -H 'Content-Type: application/json' -d'{ "settings": { "number_of_shards": 2,  "number_of_replicas": 1 }}'
    {"acknowledged":true,"shards_acknowledged":true,"index":"ind-2"}
    lex@lex ~ $ 
    lex@lex ~ $ curl -X PUT localhost:9200/ind-3 -H 'Content-Type: application/json' -d'{ "settings": { "number_of_shards": 4,  "number_of_replicas": 2 }}' 
    {"acknowledged":true,"shards_acknowledged":true,"index":"ind-3"}
    lex@lex ~ $ 
    lex@lex ~ $ curl -X GET 'http://localhost:9200/_cat/indices?v'
    health status index            uuid                   pri rep docs.count docs.deleted store.size pri.store.size
    green  open   .geoip_databases MecKaRMcQiindKXaWApguA   1   0         41            0     38.8mb         38.8mb
    green  open   ind-1            swHWJhHKRvyzqXc77-0MWw   1   0          0            0       226b           226b
    yellow open   ind-3            Y6w8QSImRT-HJFFqdfcyhQ   4   2          0            0       904b           904b
    yellow open   ind-2            UrZpcOOxR2ixYKdc1nq_DQ   2   1          0            0       452b           452b
    lex@lex ~ $ 
    
В статусе yellow, потому, что  при указанных репликах отсутствуют сервера, куда реплицировать 

    lex@lex ~ $ 
    lex@lex ~ $ curl -X DELETE 'http://localhost:9200/ind-1'
    {"acknowledged":true}
    lex@lex ~ $ 
    lex@lex ~ $ curl -X DELETE 'http://localhost:9200/ind-2'
    {"acknowledged":true}
    lex@lex ~ $ 
    lex@lex ~ $ curl -X DELETE 'http://localhost:9200/ind-3'
    {"acknowledged":true}
    lex@lex ~ $ 
    lex@lex ~ $ curl -X GET 'http://localhost:9200/_cat/indices?v'
    health status index            uuid                   pri rep docs.count docs.deleted store.size pri.store.size
    green  open   .geoip_databases MecKaRMcQiindKXaWApguA   1   0         41            0     38.8mb         38.8mb
    lex@lex ~ $ 
    lex@lex ~ $ 


### Задача 3.

В данном задании вы научитесь:

    создавать бэкапы данных
    восстанавливать индексы из бэкапов

Создайте директорию {путь до корневой директории с elasticsearch в образе}/snapshots.

Используя API зарегистрируйте данную директорию как snapshot repository c именем netology_backup.

Приведите в ответе запрос API и результат вызова API для создания репозитория.

Создайте индекс test с 0 реплик и 1 шардом и приведите в ответе список индексов.

Создайте snapshot состояния кластера elasticsearch.

Приведите в ответе список файлов в директории со snapshotами.

Удалите индекс test и создайте индекс test-2. Приведите в ответе список индексов.

Восстановите состояние кластера elasticsearch из snapshot, созданного ранее.

Приведите в ответе запрос к API восстановления и итоговый список индексов.

Подсказки:

    возможно вам понадобится доработать elasticsearch.yml в части директивы path.repo и перезапустить elasticsearch


**Ответ:**
    
    lex@lex ~ $ curl -X POST localhost:9200/_snapshot/netology_backup?pretty -H 'Content-Type: application/json' -d'{"type": "fs", "settings": { "location":"/usr/share/elasticsearch/snapshots" }}'
    {
      "acknowledged" : true
    }
    lex@lex ~ $ curl -X GET localhost:9200/_snapshot/netology_backup?pretty
    {
      "netology_backup" : {
        "type" : "fs",
        "uuid" : "px5Qc5EcTsy376_DKiZsxQ",
        "settings" : {
          "location" : "/usr/share/elasticsearch/snapshots"
        }
      }
    }
    lex@lex ~ $ curl -X GET http://localhost:9200/_cat/indices?v
    health status index            uuid                   pri rep docs.count docs.deleted store.size pri.store.size
    green  open   .geoip_databases MecKaRMcQiindKXaWApguA   1   0         41            0     38.8mb         38.8mb
    lex@lex ~ $ 
    lex@lex ~ $ curl -X PUT localhost:9200/test?pretty -H 'Content-Type: application/json' -d'{ "settings": { "number_of_shards": 1,  "number_of_replicas": 0 }}'
    {
      "acknowledged" : true,
      "shards_acknowledged" : true,
      "index" : "test"
    }
    lex@lex ~ $ 
    lex@lex ~ $ curl -X GET http://localhost:9200/_cat/indices?v
    health status index            uuid                   pri rep docs.count docs.deleted store.size pri.store.size
    green  open   .geoip_databases IpI7hncdQ8iBTlleu9VDKg   1   0         41            0     38.8mb         38.8mb
    green  open   test             ysAuU46CQv6ZecZ8tRc3JQ   1   0          0            0       226b           226b
    lex@lex ~ $ 
    lex@lex ~ $ curl -X PUT localhost:9200/_snapshot/netology_backup/snapshot?wait_for_completion=true
    {"snapshot":{"snapshot":"snapshot","uuid":"qQw0b9r1TjCk8-Y4I0-2vg","repository":"netology_backup","version_id":7170599,"version":"7.17.5","indices":[".ds-.logs-deprecation.elasticsearch-default-2022.08.16-000001",".geoip_databases","test",".ds-ilm-history-5-2022.08.16-000001"],"data_streams":["ilm-history-5",".logs-deprecation.elasticsearch-default"],"include_global_state":true,"state":"SUCCESS","start_time":"2022-08-16T05:03:22.964Z","start_time_in_millis":1660626202964,"end_time":"2022-08-16T05:03:24.164Z","end_time_in_millis":1660626204164,"duration_in_millis":1200,"failures":[],"shards":{"total":4,"failed":0,"successful":4},"feature_states":[{"feature_name":"geoip","indices":[".geoip_databases"]}]}}lex@lex ~ $ 
    lex@lex ~ $ 
    
Список файлов в папке снапшотов.(Подключился командой: sudo docker exec -it elastic bash )

    bash-4.2$ cd /usr/share/elasticsearch/snapshots/
    bash-4.2$ ls -la
    bash-4.2$ ls -la
    total 56
    drwxr-xr-x 3 elasticsearch elasticsearch  4096 Aug 16 05:03 .
    drwxr-xr-x 9 root          root           4096 Aug 16 04:04 ..
    -rw-r--r-- 1 elasticsearch elasticsearch  1420 Aug 16 05:03 index-10
    -rw-r--r-- 1 elasticsearch elasticsearch     8 Aug 16 05:03 index.latest
    drwxr-xr-x 6 elasticsearch elasticsearch  4096 Aug 16 05:03 indices
    -rw-r--r-- 1 elasticsearch elasticsearch 29297 Aug 16 05:03 meta-qQw0b9r1TjCk8-Y4I0-2vg.dat
    -rw-r--r-- 1 elasticsearch elasticsearch   707 Aug 16 05:03 snap-qQw0b9r1TjCk8-Y4I0-2vg.dat
    bash-4.2$ 

Удаляем индекс test, создаем test-2

    lex@lex ~ $ 
    lex@lex ~ $ curl -X DELETE 'http://localhost:9200/test?pretty'
    {
      "acknowledged" : true
    }
    lex@lex ~ $ 
    lex@lex ~ $ 
    lex@lex ~ $ 
    lex@lex ~ $ curl -X PUT localhost:9200/test-2?pretty -H 'Content-Type: application/json' -d'{ "settings": { "number_of_shards": 1,  "number_of_replicas": 0 }}'
    {
      "acknowledged" : true,
      "shards_acknowledged" : true,
      "index" : "test-2"
    }
    lex@lex ~ $ 

Список индексов

    lex@lex ~ $ curl -X GET http://localhost:9200/_cat/indices?v
    health status index            uuid                   pri rep docs.count docs.deleted store.size pri.store.size
    green  open   .geoip_databases IpI7hncdQ8iBTlleu9VDKg   1   0         41            0     38.8mb         38.8mb
    green  open   test-2           U8P5O7GaS5K5oZN-JqARtw   1   0          0            0       226b           226b
    lex@lex ~ $ 
    lex@lex ~ $ 

Восстанавливаем snapshot

    lex@lex ~ $ curl -X POST localhost:9200/_snapshot/netology_backup/snapshot/_restore?pretty -H 'Content-Type: application/json' -d'{"include_global_state":true,"indices": "test"}'
    {
      "accepted" : true
    }
    lex@lex ~ $ 
    lex@lex ~ $ 

Результат

    lex@lex ~ $ curl -X GET http://localhost:9200/_cat/indices?v
    health status index            uuid                   pri rep docs.count docs.deleted store.size pri.store.size
    green  open   test-2           U8P5O7GaS5K5oZN-JqARtw   1   0          0            0       226b           226b
    green  open   .geoip_databases IpI7hncdQ8iBTlleu9VDKg   1   0         41            0     38.8mb         38.8mb
    green  open   test             ysAuU46CQv6ZecZ8tRc3JQ   1   0          0            0       226b           226b
    lex@lex ~ $ 
    lex@lex ~ $ 
    lex@lex ~ $ 

Итоговые elasticsearch.yml, Dockerfile приведены в ответе к заданию 1.
