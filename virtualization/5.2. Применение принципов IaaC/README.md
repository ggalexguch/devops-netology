### Задача 1.
Опишите своими словами основные преимущества применения на практике IaaC паттернов.  
Какой из принципов IaaC является основополагающим?

**Ответ:**

    Принцип IaaC - инфраструктура как код. Согласно данной практике процесс создания инфраструктуры подобен разработке ПО.
    С помощью данного подхода ускоряется вывод продукта на рынок, быстрая подготовка разных сред (прод, тест, и проч), 
    стабильность конфигураций, легкость настройки CI/CD
    
    Главный постулат IaaC - Идемпотентность - это получение идентичного результата при повторном выполнении кода развертывания.

### Задача 2.
Чем Ansible выгодно отличается от других систем управление конфигурациями? 
Какой, на ваш взгляд, метод работы систем конфигурации более надёжный push или pull?

**Ответ:**

    Наиболее выгодное отличие от существующих систем управления конфигурациями, это то, что
    Ansible использует существующую инфраструктуру SSH, а также наиболее простой как в освоении, 
    так и в работе.

    На мой взглад более управляемый метод - это push, когда конфигурация отправляется с иницирующего сервера.
    Отстутствуют точки отказа в виде неработающих агентов для схемы pull

### Задача 3.
Установить на личный компьютер:

 VirtualBox
 Vagrant
 Ansible

**Ответ:**

    lex@lex ~ $ vagrant --version&&ansible --version&&vboxmanage --version
    Vagrant 2.2.9
    ansible 2.9.27
    5.2.32r132073
    lex@lex ~ $ 


### Задача 4.
Воспроизвести практическую часть лекции самостоятельно.

**Ответ:**
    
    lex@lex /home/lex/projects/devops/netology/vagrantansible/vagrant $ vagrant up
    Bringing machine 'server1.netology' up with 'virtualbox' provider...
    ==> server1.netology: Checking if box 'bento/ubuntu-20.04' version '202112.19.0' is up to date...
    ==> server1.netology: A newer version of the box 'bento/ubuntu-20.04' for provider 'virtualbox' is
    ==> server1.netology: available! You currently have version '202112.19.0'. The latest is version
    ==> server1.netology: '202206.03.0'. Run `vagrant box update` to update.
    ==> server1.netology: Clearing any previously set forwarded ports...
    ==> server1.netology: Clearing any previously set network interfaces...
    ==> server1.netology: Preparing network interfaces based on configuration...
        server1.netology: Adapter 1: nat
        server1.netology: Adapter 2: hostonly
    ==> server1.netology: Forwarding ports...
        server1.netology: 22 (guest) => 20011 (host) (adapter 1)
        server1.netology: 22 (guest) => 2222 (host) (adapter 1)
    ==> server1.netology: Running 'pre-boot' VM customizations...
    ==> server1.netology: Booting VM...
    ==> server1.netology: Waiting for machine to boot. This may take a few minutes...
        server1.netology: SSH address: 127.0.0.1:2222
        server1.netology: SSH username: vagrant
        server1.netology: SSH auth method: private key
    ==> server1.netology: Machine booted and ready!
    ==> server1.netology: Checking for guest additions in VM...
        server1.netology: The guest additions on this VM do not match the installed version of
        server1.netology: VirtualBox! In most cases this is fine, but in rare cases it can
        server1.netology: prevent things such as shared folders from working properly. If you see
        server1.netology: shared folder errors, please make sure the guest additions within the
        server1.netology: virtual machine match the version of VirtualBox you have installed on
        server1.netology: your host and reload your VM.
        server1.netology: 
        server1.netology: Guest Additions Version: 6.1.30
        server1.netology: VirtualBox Version: 5.2
    ==> server1.netology: Setting hostname...
    ==> server1.netology: Configuring and enabling network interfaces...
    ==> server1.netology: Mounting shared folders...
        server1.netology: /vagrant => /home/lex/projects/devops/netology/vagrantansible/vagrant
    ==> server1.netology: Machine already provisioned. Run `vagrant provision` or use the `--provision`
    ==> server1.netology: flag to force provisioning. Provisioners marked to run always will still run.
    lex@lex /home/lex/projects/devops/netology/vagrantansible/vagrant $ vagrant ssh
    Welcome to Ubuntu 20.04.3 LTS (GNU/Linux 5.4.0-91-generic x86_64)
    
    * Documentation:  https://help.ubuntu.com
    * Management:     https://landscape.canonical.com
    * Support:        https://ubuntu.com/advantage

    System information disabled due to load higher than 1.0


    This system is built by the Bento project by Chef Software
    More information can be found at https://github.com/chef/bento
    Last login: Thu Aug 11 18:37:37 2022
    vagrant@server1:~$ 
    vagrant@server1:~$ 
    vagrant@server1:~$ ps -ef| grep docker
    vagrant     1335    1323  0 19:01 pts/0    00:00:00 grep --color=auto docker
    vagrant@server1:~$ docker ps
    -bash: docker: command not found
    vagrant@server1:~$ exit
    logout
    Connection to 127.0.0.1 closed.
    lex@lex /home/lex/projects/devops/netology/vagrantansible/vagrant $ 
    lex@lex /home/lex/projects/devops/netology/vagrantansible/vagrant $ 
    lex@lex /home/lex/projects/devops/netology/vagrantansible/vagrant $ vagrant provision
    ==> server1.netology: Running provisioner: ansible...
        server1.netology: Running ansible-playbook...
     ______________
    < PLAY [nodes] >
     --------------
            \   ^__^
             \  (oo)\_______
                (__)\       )\/\
                    ||----w |
                    ||     ||
    
     ________________________
    < TASK [Gathering Facts] >
     ------------------------
            \   ^__^
             \  (oo)\_______
                (__)\       )\/\
                    ||----w |
                    ||     ||
    
    ok: [server1.netology]
     ______________________________________
    < TASK [Create directory for ssh-keys] >
     --------------------------------------
            \   ^__^
             \  (oo)\_______
                (__)\       )\/\
                    ||----w |
                    ||     ||
    
    ok: [server1.netology]
     _____________________________________________________
    < TASK [Adding rsa-key in /root/.ssh/authorized_keys] >
     -----------------------------------------------------
            \   ^__^
             \  (oo)\_______
                (__)\       )\/\
                    ||----w |
                    ||     ||
    
    changed: [server1.netology]
     _____________________
    < TASK [Checking DNS] >
     ---------------------
            \   ^__^
             \  (oo)\_______
                (__)\       )\/\
                    ||----w |
                    ||     ||
    
    changed: [server1.netology]
     _________________________
    < TASK [Installing tools] >
     -------------------------
            \   ^__^
             \  (oo)\_______
                (__)\       )\/\
                    ||----w |
                    ||     ||
    
    ok: [server1.netology] => (item=[u'git', u'curl'])
     __________________________
    < TASK [Installing docker] >
     --------------------------
            \   ^__^
             \  (oo)\_______
                (__)\       )\/\
                    ||----w |
                    ||     ||
    
    changed: [server1.netology]
     _____________________________________________
    < TASK [Add the current user to docker group] >
     ---------------------------------------------
            \   ^__^
             \  (oo)\_______
                (__)\       )\/\
                    ||----w |
                    ||     ||
    
    changed: [server1.netology]
     ____________
    < PLAY RECAP >
     ------------
            \   ^__^
             \  (oo)\_______
                (__)\       )\/\
                    ||----w |
                    ||     ||
    
    server1.netology           : ok=7    changed=4    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
    
    lex@lex /home/lex/projects/devops/netology/vagrantansible/vagrant $ vagrant ssh
    Welcome to Ubuntu 20.04.3 LTS (GNU/Linux 5.4.0-91-generic x86_64)

    * Documentation:  https://help.ubuntu.com
    * Management:     https://landscape.canonical.com
    * Support:        https://ubuntu.com/advantage

    System information as of Thu 11 Aug 2022 07:04:52 PM UTC

      System load:  0.37               Users logged in:          0
      Usage of /:   13.7% of 30.88GB   IPv4 address for docker0: 172.17.0.1
      Memory usage: 25%                IPv4 address for eth0:    10.0.2.15
      Swap usage:   0%                 IPv4 address for eth1:    192.168.56.11
      Processes:    119
    
    
    This system is built by the Bento project by Chef Software
    More information can be found at https://github.com/chef/bento
    Last login: Thu Aug 11 19:04:20 2022 from 10.0.2.2
    vagrant@server1:~$ docker ps
    CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES
    vagrant@server1:~$ 
