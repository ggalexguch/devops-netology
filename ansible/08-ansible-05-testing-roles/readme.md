# Домашнее задание к занятию "08.05 Тестирование Roles"

## Подготовка к выполнению
1. Установите molecule: `pip3 install "molecule==3.5.2"`
2. Выполните `docker pull aragast/netology:latest` -  это образ с podman, tox и несколькими пайтонами (3.7 и 3.9) внутри

## Основная часть

Наша основная цель - настроить тестирование наших ролей. Задача: сделать сценарии тестирования для vector. Ожидаемый результат: все сценарии успешно проходят тестирование ролей.

### Molecule

1. Запустите  `molecule test -s centos8` внутри корневой директории clickhouse-role, посмотрите на вывод команды.
2. Перейдите в каталог с ролью vector-role и создайте сценарий тестирования по умолчанию при помощи `molecule init scenario --driver-name docker`.
3. Добавьте несколько разных дистрибутивов (centos:8, ubuntu:latest) для инстансов и протестируйте роль, исправьте найденные ошибки, если они есть.
4. Добавьте несколько assert'ов в verify.yml файл для  проверки работоспособности vector-role (проверка, что конфиг валидный, проверка успешности запуска, etc). Запустите тестирование роли повторно и проверьте, что оно прошло успешно.
5. Добавьте новый тег на коммит с рабочим сценарием в соответствии с семантическим версионированием.

## Ответ

1. 
```
lex@lexhost ~/PycharmProjects/netologyProjects/2.1. Системы контроля версий/devops-netology/ansible/08-ansible-05-testing/playbook/ansible-clickhouse $ molecule test -s centos_7 >> log.txt
INFO     centos_7 scenario test matrix: dependency, lint, cleanup, destroy, syntax, create, prepare, converge, idempotence, side_effect, verify, cleanup, destroy
INFO     Performing prerun...
INFO     Set ANSIBLE_LIBRARY=/home/lex/.cache/ansible-compat/a5b59e/modules:/home/lex/.ansible/plugins/modules:/usr/share/ansible/plugins/modules
INFO     Set ANSIBLE_COLLECTIONS_PATH=/home/lex/.cache/ansible-compat/a5b59e/collections:/home/lex/.ansible/collections:/usr/share/ansible/collections
INFO     Set ANSIBLE_ROLES_PATH=/home/lex/.cache/ansible-compat/a5b59e/roles:/home/lex/.ansible/roles:/usr/share/ansible/roles:/etc/ansible/roles
INFO     Inventory /home/lex/PycharmProjects/netologyProjects/devops-netology/ansible/08-ansible-05-testing/playbook/ansible-clickhouse/molecule/centos_7/../resources/inventory/hosts.yml linked to /home/lex/.cache/molecule/ansible-clickhouse/centos_7/inventory/hosts
INFO     Inventory /home/lex/PycharmProjects/netologyProjects/devops-netology/ansible/08-ansible-05-testing/playbook/ansible-clickhouse/molecule/centos_7/../resources/inventory/group_vars/ linked to /home/lex/.cache/molecule/ansible-clickhouse/centos_7/inventory/group_vars
INFO     Inventory /home/lex/PycharmProjects/netologyProjects/devops-netology/ansible/08-ansible-05-testing/playbook/ansible-clickhouse/molecule/centos_7/../resources/inventory/host_vars/ linked to /home/lex/.cache/molecule/ansible-clickhouse/centos_7/inventory/host_vars
INFO     Running centos_7 > dependency
WARNING  Skipping, missing the requirements file.
WARNING  Skipping, missing the requirements file.
INFO     Inventory /home/lex/PycharmProjects/netologyProjects/devops-netology/ansible/08-ansible-05-testing/playbook/ansible-clickhouse/molecule/centos_7/../resources/inventory/hosts.yml linked to /home/lex/.cache/molecule/ansible-clickhouse/centos_7/inventory/hosts
INFO     Inventory /home/lex/PycharmProjects/netologyProjects/devops-netology/ansible/08-ansible-05-testing/playbook/ansible-clickhouse/molecule/centos_7/../resources/inventory/group_vars/ linked to /home/lex/.cache/molecule/ansible-clickhouse/centos_7/inventory/group_vars
INFO     Inventory /home/lex/PycharmProjects/netologyProjects/devops-netology/ansible/08-ansible-05-testing/playbook/ansible-clickhouse/molecule/centos_7/../resources/inventory/host_vars/ linked to /home/lex/.cache/molecule/ansible-clickhouse/centos_7/inventory/host_vars
INFO     Running centos_7 > lint
INFO     Inventory /home/lex/PycharmProjects/netologyProjects/devops-netology/ansible/08-ansible-05-testing/playbook/ansible-clickhouse/molecule/centos_7/../resources/inventory/hosts.yml linked to /home/lex/.cache/molecule/ansible-clickhouse/centos_7/inventory/hosts
INFO     Inventory /home/lex/PycharmProjects/netologyProjects/devops-netology/ansible/08-ansible-05-testing/playbook/ansible-clickhouse/molecule/centos_7/../resources/inventory/group_vars/ linked to /home/lex/.cache/molecule/ansible-clickhouse/centos_7/inventory/group_vars
INFO     Inventory /home/lex/PycharmProjects/netologyProjects/devops-netology/ansible/08-ansible-05-testing/playbook/ansible-clickhouse/molecule/centos_7/../resources/inventory/host_vars/ linked to /home/lex/.cache/molecule/ansible-clickhouse/centos_7/inventory/host_vars
INFO     Running centos_7 > cleanup
WARNING  Skipping, cleanup playbook not configured.
INFO     Inventory /home/lex/PycharmProjects/netologyProjects/devops-netology/ansible/08-ansible-05-testing/playbook/ansible-clickhouse/molecule/centos_7/../resources/inventory/hosts.yml linked to /home/lex/.cache/molecule/ansible-clickhouse/centos_7/inventory/hosts
INFO     Inventory /home/lex/PycharmProjects/netologyProjects/devops-netology/ansible/08-ansible-05-testing/playbook/ansible-clickhouse/molecule/centos_7/../resources/inventory/group_vars/ linked to /home/lex/.cache/molecule/ansible-clickhouse/centos_7/inventory/group_vars
INFO     Inventory /home/lex/PycharmProjects/netologyProjects/devops-netology/ansible/08-ansible-05-testing/playbook/ansible-clickhouse/molecule/centos_7/../resources/inventory/host_vars/ linked to /home/lex/.cache/molecule/ansible-clickhouse/centos_7/inventory/host_vars
INFO     Running centos_7 > destroy
INFO     Sanity checks: 'docker'
INFO     Inventory /home/lex/PycharmProjects/netologyProjects/devops-netology/ansible/08-ansible-05-testing/playbook/ansible-clickhouse/molecule/centos_7/../resources/inventory/hosts.yml linked to /home/lex/.cache/molecule/ansible-clickhouse/centos_7/inventory/hosts
INFO     Inventory /home/lex/PycharmProjects/netologyProjects/devops-netology/ansible/08-ansible-05-testing/playbook/ansible-clickhouse/molecule/centos_7/../resources/inventory/group_vars/ linked to /home/lex/.cache/molecule/ansible-clickhouse/centos_7/inventory/group_vars
INFO     Inventory /home/lex/PycharmProjects/netologyProjects/devops-netology/ansible/08-ansible-05-testing/playbook/ansible-clickhouse/molecule/centos_7/../resources/inventory/host_vars/ linked to /home/lex/.cache/molecule/ansible-clickhouse/centos_7/inventory/host_vars
INFO     Running centos_7 > syntax
INFO     Inventory /home/lex/PycharmProjects/netologyProjects/devops-netology/ansible/08-ansible-05-testing/playbook/ansible-clickhouse/molecule/centos_7/../resources/inventory/hosts.yml linked to /home/lex/.cache/molecule/ansible-clickhouse/centos_7/inventory/hosts
INFO     Inventory /home/lex/PycharmProjects/netologyProjects/devops-netology/ansible/08-ansible-05-testing/playbook/ansible-clickhouse/molecule/centos_7/../resources/inventory/group_vars/ linked to /home/lex/.cache/molecule/ansible-clickhouse/centos_7/inventory/group_vars
INFO     Inventory /home/lex/PycharmProjects/netologyProjects/devops-netology/ansible/08-ansible-05-testing/playbook/ansible-clickhouse/molecule/centos_7/../resources/inventory/host_vars/ linked to /home/lex/.cache/molecule/ansible-clickhouse/centos_7/inventory/host_vars
INFO     Running centos_7 > create
INFO     Inventory /home/lex/PycharmProjects/netologyProjects/devops-netology/ansible/08-ansible-05-testing/playbook/ansible-clickhouse/molecule/centos_7/../resources/inventory/hosts.yml linked to /home/lex/.cache/molecule/ansible-clickhouse/centos_7/inventory/hosts
INFO     Inventory /home/lex/PycharmProjects/netologyProjects/devops-netology/ansible/08-ansible-05-testing/playbook/ansible-clickhouse/molecule/centos_7/../resources/inventory/group_vars/ linked to /home/lex/.cache/molecule/ansible-clickhouse/centos_7/inventory/group_vars
INFO     Inventory /home/lex/PycharmProjects/netologyProjects/devops-netology/ansible/08-ansible-05-testing/playbook/ansible-clickhouse/molecule/centos_7/../resources/inventory/host_vars/ linked to /home/lex/.cache/molecule/ansible-clickhouse/centos_7/inventory/host_vars
INFO     Running centos_7 > prepare
WARNING  Skipping, prepare playbook not configured.
INFO     Inventory /home/lex/PycharmProjects/netologyProjects/devops-netology/ansible/08-ansible-05-testing/playbook/ansible-clickhouse/molecule/centos_7/../resources/inventory/hosts.yml linked to /home/lex/.cache/molecule/ansible-clickhouse/centos_7/inventory/hosts
INFO     Inventory /home/lex/PycharmProjects/netologyProjects/devops-netology/ansible/08-ansible-05-testing/playbook/ansible-clickhouse/molecule/centos_7/../resources/inventory/group_vars/ linked to /home/lex/.cache/molecule/ansible-clickhouse/centos_7/inventory/group_vars
INFO     Inventory /home/lex/PycharmProjects/netologyProjects/devops-netology/ansible/08-ansible-05-testing/playbook/ansible-clickhouse/molecule/centos_7/../resources/inventory/host_vars/ linked to /home/lex/.cache/molecule/ansible-clickhouse/centos_7/inventory/host_vars
INFO     Running centos_7 > converge
CRITICAL Ansible return code was 2, command was: ['ansible-playbook', '-D', '--inventory', '/home/lex/.cache/molecule/ansible-clickhouse/centos_7/inventory', '--skip-tags', 'molecule-notest,notest', '/home/lex/PycharmProjects/netologyProjects/devops-netology/ansible/08-ansible-05-testing/playbook/ansible-clickhouse/molecule/resources/playbooks/converge.yml']
WARNING  An error occurred during the test sequence action: 'converge'. Cleaning up.
INFO     Inventory /home/lex/PycharmProjects/netologyProjects/devops-netology/ansible/08-ansible-05-testing/playbook/ansible-clickhouse/molecule/centos_7/../resources/inventory/hosts.yml linked to /home/lex/.cache/molecule/ansible-clickhouse/centos_7/inventory/hosts
INFO     Inventory /home/lex/PycharmProjects/netologyProjects/devops-netology/ansible/08-ansible-05-testing/playbook/ansible-clickhouse/molecule/centos_7/../resources/inventory/group_vars/ linked to /home/lex/.cache/molecule/ansible-clickhouse/centos_7/inventory/group_vars
INFO     Inventory /home/lex/PycharmProjects/netologyProjects/devops-netology/ansible/08-ansible-05-testing/playbook/ansible-clickhouse/molecule/centos_7/../resources/inventory/host_vars/ linked to /home/lex/.cache/molecule/ansible-clickhouse/centos_7/inventory/host_vars
INFO     Running centos_7 > cleanup
WARNING  Skipping, cleanup playbook not configured.
INFO     Inventory /home/lex/PycharmProjects/netologyProjects/devops-netology/ansible/08-ansible-05-testing/playbook/ansible-clickhouse/molecule/centos_7/../resources/inventory/hosts.yml linked to /home/lex/.cache/molecule/ansible-clickhouse/centos_7/inventory/hosts
INFO     Inventory /home/lex/PycharmProjects/netologyProjects/devops-netology/ansible/08-ansible-05-testing/playbook/ansible-clickhouse/molecule/centos_7/../resources/inventory/group_vars/ linked to /home/lex/.cache/molecule/ansible-clickhouse/centos_7/inventory/group_vars
INFO     Inventory /home/lex/PycharmProjects/netologyProjects/devops-netology/ansible/08-ansible-05-testing/playbook/ansible-clickhouse/molecule/centos_7/../resources/inventory/host_vars/ linked to /home/lex/.cache/molecule/ansible-clickhouse/centos_7/inventory/host_vars
INFO     Running centos_7 > destroy
INFO     Pruning extra files from scenario ephemeral directory
lex@lexhost ~/PycharmProjects/netologyProjects/2.1. Системы контроля версий/devops-netology/ansible/08-ansible-05-testing/playbook/ansible-clickhouse $ 

```
Детали вызова
```

PLAY [Destroy] *****************************************************************

TASK [Destroy molecule instance(s)] ********************************************
[33mchanged: [localhost] => (item=centos_7)[0m

TASK [Wait for instance(s) deletion to complete] *******************************
[1;30mFAILED - RETRYING: Wait for instance(s) deletion to complete (300 retries left).[0m
[32mok: [localhost] => (item=centos_7)[0m

TASK [Delete docker networks(s)] ***********************************************

PLAY RECAP *********************************************************************
[33mlocalhost[0m                  : [32mok=2   [0m [33mchanged=1   [0m unreachable=0    failed=0    [36mskipped=1   [0m rescued=0    ignored=0

[35m[DEPRECATION WARNING]: Ansible will require Python 3.8 or newer on the [0m
[35mcontroller starting with Ansible 2.12. Current version: 3.7.6 (default, Jan  8 [0m
[35m2020, 19:59:22) [GCC 7.3.0]. This feature will be removed from ansible-core in [0m
[35mversion 2.12. Deprecation warnings can be disabled by setting [0m
[35mdeprecation_warnings=False in ansible.cfg.[0m

playbook: /home/lex/PycharmProjects/netologyProjects/devops-netology/ansible/08-ansible-05-testing/playbook/ansible-clickhouse/molecule/resources/playbooks/converge.yml
[35m[DEPRECATION WARNING]: Ansible will require Python 3.8 or newer on the [0m
[35mcontroller starting with Ansible 2.12. Current version: 3.7.6 (default, Jan  8 [0m
[35m2020, 19:59:22) [GCC 7.3.0]. This feature will be removed from ansible-core in [0m
[35mversion 2.12. Deprecation warnings can be disabled by setting [0m
[35mdeprecation_warnings=False in ansible.cfg.[0m

PLAY [Create] ******************************************************************

TASK [Log into a Docker registry] **********************************************
[36mskipping: [localhost] => (item=None) [0m
[36mskipping: [localhost][0m

TASK [Check presence of custom Dockerfiles] ************************************
[32mok: [localhost] => (item={'capabilities': ['SYS_ADMIN'], 'command': '/usr/sbin/init', 'dockerfile': '../resources/Dockerfile.j2', 'env': {'ANSIBLE_USER': 'ansible', 'DEPLOY_GROUP': 'deployer', 'SUDO_GROUP': 'wheel', 'container': 'docker'}, 'image': 'centos:7', 'name': 'centos_7', 'privileged': True, 'tmpfs': ['/run', '/tmp'], 'volumes': ['/sys/fs/cgroup:/sys/fs/cgroup']})[0m

TASK [Create Dockerfiles from image names] *************************************
[33mchanged: [localhost] => (item={'capabilities': ['SYS_ADMIN'], 'command': '/usr/sbin/init', 'dockerfile': '../resources/Dockerfile.j2', 'env': {'ANSIBLE_USER': 'ansible', 'DEPLOY_GROUP': 'deployer', 'SUDO_GROUP': 'wheel', 'container': 'docker'}, 'image': 'centos:7', 'name': 'centos_7', 'privileged': True, 'tmpfs': ['/run', '/tmp'], 'volumes': ['/sys/fs/cgroup:/sys/fs/cgroup']})[0m

TASK [Discover local Docker images] ********************************************
[32mok: [localhost] => (item={'diff': [], 'dest': '/home/lex/.cache/molecule/ansible-clickhouse/centos_7/Dockerfile_centos_7', 'src': '/home/lex/.ansible/tmp/ansible-tmp-1673922669.6813817-2225-263590458447962/source', 'md5sum': 'e90d08cd34f49a5f8a41a07de1348618', 'checksum': '4b70768619482424811f2977aa277a5acf2b13a1', 'changed': True, 'uid': 1000, 'gid': 1000, 'owner': 'lex', 'group': 'lex', 'mode': '0600', 'state': 'file', 'size': 2199, 'invocation': {'module_args': {'src': '/home/lex/.ansible/tmp/ansible-tmp-1673922669.6813817-2225-263590458447962/source', 'dest': '/home/lex/.cache/molecule/ansible-clickhouse/centos_7/Dockerfile_centos_7', 'mode': '0600', 'follow': False, '_original_basename': 'Dockerfile.j2', 'checksum': '4b70768619482424811f2977aa277a5acf2b13a1', 'backup': False, 'force': True, 'unsafe_writes': False, 'content': None, 'validate': None, 'directory_mode': None, 'remote_src': None, 'local_follow': None, 'owner': None, 'group': None, 'seuser': None, 'serole': None, 'selevel': None, 'setype': None, 'attributes': None}}, 'failed': False, 'item': {'capabilities': ['SYS_ADMIN'], 'command': '/usr/sbin/init', 'dockerfile': '../resources/Dockerfile.j2', 'env': {'ANSIBLE_USER': 'ansible', 'DEPLOY_GROUP': 'deployer', 'SUDO_GROUP': 'wheel', 'container': 'docker'}, 'image': 'centos:7', 'name': 'centos_7', 'privileged': True, 'tmpfs': ['/run', '/tmp'], 'volumes': ['/sys/fs/cgroup:/sys/fs/cgroup']}, 'ansible_loop_var': 'item', 'i': 0, 'ansible_index_var': 'i'})[0m

TASK [Build an Ansible compatible image (new)] *********************************
[32mok: [localhost] => (item=molecule_local/centos:7)[0m

TASK [Create docker network(s)] ************************************************

TASK [Determine the CMD directives] ********************************************
[32mok: [localhost] => (item={'capabilities': ['SYS_ADMIN'], 'command': '/usr/sbin/init', 'dockerfile': '../resources/Dockerfile.j2', 'env': {'ANSIBLE_USER': 'ansible', 'DEPLOY_GROUP': 'deployer', 'SUDO_GROUP': 'wheel', 'container': 'docker'}, 'image': 'centos:7', 'name': 'centos_7', 'privileged': True, 'tmpfs': ['/run', '/tmp'], 'volumes': ['/sys/fs/cgroup:/sys/fs/cgroup']})[0m

TASK [Create molecule instance(s)] *********************************************
[33mchanged: [localhost] => (item=centos_7)[0m

TASK [Wait for instance(s) creation to complete] *******************************
[1;30mFAILED - RETRYING: Wait for instance(s) creation to complete (300 retries left).[0m
[33mchanged: [localhost] => (item={'started': 1, 'finished': 0, 'ansible_job_id': '831663529555.2372', 'results_file': '/home/lex/.ansible_async/831663529555.2372', 'changed': True, 'failed': False, 'item': {'capabilities': ['SYS_ADMIN'], 'command': '/usr/sbin/init', 'dockerfile': '../resources/Dockerfile.j2', 'env': {'ANSIBLE_USER': 'ansible', 'DEPLOY_GROUP': 'deployer', 'SUDO_GROUP': 'wheel', 'container': 'docker'}, 'image': 'centos:7', 'name': 'centos_7', 'privileged': True, 'tmpfs': ['/run', '/tmp'], 'volumes': ['/sys/fs/cgroup:/sys/fs/cgroup']}, 'ansible_loop_var': 'item'})[0m

PLAY RECAP *********************************************************************
[33mlocalhost[0m                  : [32mok=7   [0m [33mchanged=3   [0m unreachable=0    failed=0    [36mskipped=2   [0m rescued=0    ignored=0

[35m[DEPRECATION WARNING]: Ansible will require Python 3.8 or newer on the [0m
[35mcontroller starting with Ansible 2.12. Current version: 3.7.6 (default, Jan  8 [0m
[35m2020, 19:59:22) [GCC 7.3.0]. This feature will be removed from ansible-core in [0m
[35mversion 2.12. Deprecation warnings can be disabled by setting [0m
[35mdeprecation_warnings=False in ansible.cfg.[0m

PLAY [Converge] ****************************************************************

TASK [Gathering Facts] *********************************************************
[32mok: [centos_7][0m

TASK [Apply Clickhouse Role] ***************************************************

TASK [ansible-clickhouse : Include OS Family Specific Variables] ***************
[32mok: [centos_7][0m

TASK [ansible-clickhouse : include_tasks] **************************************
[36mincluded: /home/lex/PycharmProjects/netologyProjects/devops-netology/ansible/08-ansible-05-testing/playbook/ansible-clickhouse/tasks/precheck.yml for centos_7[0m

TASK [ansible-clickhouse : Requirements check | Checking sse4_2 support] *******
[32mok: [centos_7][0m

TASK [ansible-clickhouse : Requirements check | Not supported distribution && release] ***
[36mskipping: [centos_7][0m

TASK [ansible-clickhouse : include_tasks] **************************************
[36mincluded: /home/lex/PycharmProjects/netologyProjects/devops-netology/ansible/08-ansible-05-testing/playbook/ansible-clickhouse/tasks/params.yml for centos_7[0m

TASK [ansible-clickhouse : Set clickhouse_service_enable] **********************
[32mok: [centos_7][0m

TASK [ansible-clickhouse : Set clickhouse_service_ensure] **********************
[32mok: [centos_7][0m

TASK [ansible-clickhouse : include_tasks] **************************************
[36mincluded: /home/lex/PycharmProjects/netologyProjects/devops-netology/ansible/08-ansible-05-testing/playbook/ansible-clickhouse/tasks/install/yum.yml for centos_7[0m

TASK [ansible-clickhouse : Install by YUM | Ensure clickhouse repo installed] ***
[31m--- before: /etc/yum.repos.d/clickhouse.repo[0m
[32m+++ after: /etc/yum.repos.d/clickhouse.repo[0m
[36m@@ -0,0 +1,7 @@[0m
[32m+[clickhouse][0m
[32m+async = 1[0m
[32m+baseurl = https://packages.clickhouse.com/rpm/stable/[0m
[32m+enabled = 1[0m
[32m+gpgcheck = 0[0m
[32m+name = Clickhouse repo[0m
[32m+[0m

[33mchanged: [centos_7][0m

TASK [ansible-clickhouse : Install by YUM | Ensure clickhouse package installed (latest)] ***
[33mchanged: [centos_7][0m

TASK [ansible-clickhouse : Install by YUM | Ensure clickhouse package installed (version latest)] ***
[36mskipping: [centos_7][0m

TASK [ansible-clickhouse : include_tasks] **************************************
[36mincluded: /home/lex/PycharmProjects/netologyProjects/devops-netology/ansible/08-ansible-05-testing/playbook/ansible-clickhouse/tasks/configure/sys.yml for centos_7[0m

TASK [ansible-clickhouse : Check clickhouse config, data and logs] *************
[32mok: [centos_7] => (item=/var/log/clickhouse-server)[0m
[31m--- before[0m
[32m+++ after[0m
[36m@@ -1,4 +1,4 @@[0m
 {
[31m-    "mode": "0700",[0m
[32m+    "mode": "0770",[0m
     "path": "/etc/clickhouse-server"
 }

[33mchanged: [centos_7] => (item=/etc/clickhouse-server)[0m
[31m--- before[0m
[32m+++ after[0m
[36m@@ -1,7 +1,7 @@[0m
 {
[31m-    "group": 0,[0m
[31m-    "mode": "0755",[0m
[31m-    "owner": 0,[0m
[32m+    "group": 996,[0m
[32m+    "mode": "0770",[0m
[32m+    "owner": 999,[0m
     "path": "/var/lib/clickhouse/tmp/",
[31m-    "state": "absent"[0m
[32m+    "state": "directory"[0m
 }

[33mchanged: [centos_7] => (item=/var/lib/clickhouse/tmp/)[0m
[31m--- before[0m
[32m+++ after[0m
[36m@@ -1,4 +1,4 @@[0m
 {
[31m-    "mode": "0700",[0m
[32m+    "mode": "0770",[0m
     "path": "/var/lib/clickhouse/"
 }

[33mchanged: [centos_7] => (item=/var/lib/clickhouse/)[0m

TASK [ansible-clickhouse : Config | Create config.d folder] ********************
[31m--- before[0m
[32m+++ after[0m
[36m@@ -1,4 +1,4 @@[0m
 {
[31m-    "mode": "0500",[0m
[32m+    "mode": "0770",[0m
     "path": "/etc/clickhouse-server/config.d"
 }

[33mchanged: [centos_7][0m

TASK [ansible-clickhouse : Config | Create users.d folder] *********************
[31m--- before[0m
[32m+++ after[0m
[36m@@ -1,4 +1,4 @@[0m
 {
[31m-    "mode": "0500",[0m
[32m+    "mode": "0770",[0m
     "path": "/etc/clickhouse-server/users.d"
 }

[33mchanged: [centos_7][0m

TASK [ansible-clickhouse : Config | Generate system config] ********************
[31m--- before[0m
[32m+++ after: /home/lex/.ansible/tmp/ansible-local-25753fkdindm/tmp6r5f5dj8/config.j2[0m
[36m@@ -0,0 +1,382 @@[0m
[32m+<?xml version="1.0"?>[0m
[32m+<!--[0m
[32m+ -[0m
[32m+ - Ansible managed: Do NOT edit this file manually![0m
[32m+ -[0m
[32m+--> [0m
[32m+<clickhouse>[0m
[32m+    <logger>[0m
[32m+        <!-- Possible levels: https://github.com/pocoproject/poco/blob/develop/Foundation/include/Poco/Logger.h#L105 -->[0m
[32m+        <level>trace</level>[0m
[32m+        <log>/var/log/clickhouse-server/clickhouse-server.log</log>[0m
[32m+        <errorlog>/var/log/clickhouse-server/clickhouse-server.err.log</errorlog>[0m
[32m+        <size>1000M</size>[0m
[32m+        <count>10</count>[0m
[32m+    </logger>[0m
[32m+[0m
[32m+    <http_port>8123</http_port>[0m
[32m+[0m
[32m+    <tcp_port>9000</tcp_port>[0m
[32m+[0m
[32m+    <!-- Used with https_port and tcp_port_secure. Full ssl options list: https://github.com/ClickHouse-Extras/poco/blob/master/NetSSL_OpenSSL/include/Poco/Net/SSLManager.h#L71 -->[0m
[32m+    <openSSL>[0m
[32m+        <server> <!-- Used for https server AND secure tcp port -->[0m
[32m+            <!-- openssl req -subj "/CN=localhost" -new -newkey rsa:2048 -days 365 -nodes -x509 -keyout /etc/clickhouse-server/server.key -out /etc/clickhouse-server/server.crt -->[0m
[32m+            <certificateFile>/etc/clickhouse-server/server.crt</certificateFile>[0m
[32m+            <privateKeyFile>/etc/clickhouse-server/server.key</privateKeyFile>[0m
[32m+            <!-- openssl dhparam -out /etc/clickhouse-server/dhparam.pem 4096 -->[0m
[32m+            <dhParamsFile>/etc/clickhouse-server/dhparam.pem</dhParamsFile>[0m
[32m+            <verificationMode>none</verificationMode>[0m
[32m+            <loadDefaultCAFile>true</loadDefaultCAFile>[0m
[32m+            <cacheSessions>true</cacheSessions>[0m
[32m+            <disableProtocols>sslv2,sslv3</disableProtocols>[0m
[32m+            <preferServerCiphers>true</preferServerCiphers>[0m
[32m+        </server>[0m
[32m+[0m
[32m+        <client> <!-- Used for connecting to https dictionary source -->[0m
[32m+            <loadDefaultCAFile>true</loadDefaultCAFile>[0m
[32m+            <cacheSessions>true</cacheSessions>[0m
[32m+            <disableProtocols>sslv2,sslv3</disableProtocols>[0m
[32m+            <preferServerCiphers>true</preferServerCiphers>[0m
[32m+            <!-- Use for self-signed: <verificationMode>none</verificationMode> -->[0m
[32m+            <invalidCertificateHandler>[0m
[32m+                <!-- Use for self-signed: <name>AcceptCertificateHandler</name> -->[0m
[32m+                <name>RejectCertificateHandler</name>[0m
[32m+            </invalidCertificateHandler>[0m
[32m+        </client>[0m
[32m+    </openSSL>[0m
[32m+[0m
[32m+    <!-- Default root page on http[s] server. For example load UI from https://tabix.io/ when opening http://localhost:8123 -->[0m
[32m+    <!--[0m
[32m+    <http_server_default_response><![CDATA[<html ng-app="SMI2"><head><base href="http://ui.tabix.io/"></head><body><div ui-view="" class="content-ui"></div><script src="http://loader.tabix.io/master.js"></script></body></html>]]></http_server_default_response>[0m
[32m+    -->[0m
[32m+[0m
[32m+    <!-- Port for communication between replicas. Used for data exchange. -->[0m
[32m+    <interserver_http_port>9009</interserver_http_port>[0m
[32m+[0m
[32m+[0m
[32m+[0m
[32m+    <!-- Hostname that is used by other replicas to request this server.[0m
[32m+         If not specified, than it is determined analoguous to 'hostname -f' command.[0m
[32m+         This setting could be used to switch replication to another network interface.[0m
[32m+      -->[0m
[32m+    <!--[0m
[32m+    <interserver_http_host>example.clickhouse.com</interserver_http_host>[0m
[32m+    -->[0m
[32m+[0m
[32m+    <!-- Listen specified host. use :: (wildcard IPv6 address), if you want to accept connections both with IPv4 and IPv6 from everywhere. -->[0m
[32m+    <!-- <listen_host>::</listen_host> -->[0m
[32m+    <!-- Same for hosts with disabled ipv6: -->[0m
[32m+    <!-- <listen_host>0.0.0.0</listen_host> -->[0m
[32m+    <listen_host>127.0.0.1</listen_host>[0m
[32m+[0m
[32m+    <max_connections>2048</max_connections>[0m
[32m+    <keep_alive_timeout>3</keep_alive_timeout>[0m
[32m+[0m
[32m+    <!-- Maximum number of concurrent queries. -->[0m
[32m+    <max_concurrent_queries>100</max_concurrent_queries>[0m
[32m+[0m
[32m+    <!-- Set limit on number of open files (default: maximum). This setting makes sense on Mac OS X because getrlimit() fails to retrieve[0m
[32m+         correct maximum value. -->[0m
[32m+    <!-- <max_open_files>262144</max_open_files> -->[0m
[32m+[0m
[32m+    <!-- Size of cache of uncompressed blocks of data, used in tables of MergeTree family.[0m
[32m+         In bytes. Cache is single for server. Memory is allocated only on demand.[0m
[32m+         Cache is used when 'use_uncompressed_cache' user setting turned on (off by default).[0m
[32m+         Uncompressed cache is advantageous only for very short queries and in rare cases.[0m
[32m+      -->[0m
[32m+    <uncompressed_cache_size>8589934592</uncompressed_cache_size>[0m
[32m+[0m
[32m+    <!-- Approximate size of mark cache, used in tables of MergeTree family.[0m
[32m+         In bytes. Cache is single for server. Memory is allocated only on demand.[0m
[32m+         You should not lower this value.[0m
[32m+      -->[0m
[32m+    <mark_cache_size>5368709120</mark_cache_size>[0m
[32m+[0m
[32m+[0m
[32m+    <!-- Path to data directory, with trailing slash. -->[0m
[32m+    <path>/var/lib/clickhouse/</path>[0m
[32m+[0m
[32m+    <!-- Path to temporary data for processing hard queries. -->[0m
[32m+    <tmp_path>/var/lib/clickhouse/tmp/</tmp_path>[0m
[32m+[0m
[32m+    <!-- Directory with user provided files that are accessible by 'file' table function. -->[0m
[32m+    <user_files_path>/var/lib/clickhouse/user_files/</user_files_path>[0m
[32m+[0m
[32m+    <!-- Path to configuration file with users, access rights, profiles of settings, quotas. -->[0m
[32m+    <users_config>users.xml</users_config>[0m
[32m+[0m
[32m+    <!-- Default profile of settings. -->[0m
[32m+    <default_profile>default</default_profile>[0m
[32m+[0m
[32m+    <!-- System profile of settings. This settings are used by internal processes (Buffer storage, Distibuted DDL worker and so on). -->[0m
[32m+    <!-- <system_profile>default</system_profile> -->[0m
[32m+[0m
[32m+    <!-- Default database. -->[0m
[32m+    <default_database>default</default_database>[0m
[32m+[0m
[32m+    <!-- Server time zone could be set here.[0m
[32m+[0m
[32m+         Time zone is used when converting between String and DateTime types,[0m
[32m+          when printing DateTime in text formats and parsing DateTime from text,[0m
[32m+          it is used in date and time related functions, if specific time zone was not passed as an argument.[0m
[32m+[0m
[32m+         Time zone is specified as identifier from IANA time zone database, like UTC or Africa/Abidjan.[0m
[32m+         If not specified, system time zone at server startup is used.[0m
[32m+[0m
[32m+         Please note, that server could display time zone alias instead of specified name.[0m
[32m+         Example: W-SU is an alias for Europe/Moscow and Zulu is an alias for UTC.[0m
[32m+    -->[0m
[32m+    <!-- <timezone>Europe/Moscow</timezone> -->[0m
[32m+[0m
[32m+    <!-- You can specify umask here (see "man umask"). Server will apply it on startup.[0m
[32m+         Number is always parsed as octal. Default umask is 027 (other users cannot read logs, data files, etc; group can only read).[0m
[32m+    -->[0m
[32m+    <!-- <umask>022</umask> -->[0m
[32m+[0m
[32m+    <!-- Perform mlockall after startup to lower first queries latency[0m
[32m+          and to prevent clickhouse executable from being paged out under high IO load.[0m
[32m+         Enabling this option is recommended but will lead to increased startup time for up to a few seconds.[0m
[32m+    -->[0m
[32m+    <mlock_executable>False</mlock_executable>[0m
[32m+[0m
[32m+    <!-- Configuration of clusters that could be used in Distributed tables.[0m
[32m+         https://clickhouse.com/docs/en/engines/table-engines/special/distributed/[0m
[32m+      -->[0m
[32m+    <remote_servers incl="clickhouse_remote_servers" />[0m
[32m+[0m
[32m+[0m
[32m+    <!-- If element has 'incl' attribute, then for it's value will be used corresponding substitution from another file.[0m
[32m+         By default, path to file with substitutions is /etc/metrika.xml. It could be changed in config in 'include_from' element.[0m
[32m+         Values for substitutions are specified in /clickhouse/name_of_substitution elements in that file.[0m
[32m+      -->[0m
[32m+[0m
[32m+    <!-- ZooKeeper is used to store metadata about replicas, when using Replicated tables.[0m
[32m+         Optional. If you don't use replicated tables, you could omit that.[0m
[32m+[0m
[32m+         See https://clickhouse.com/docs/en/engines/table-engines/mergetree-family/replication/[0m
[32m+      -->[0m
[32m+    <zookeeper incl="zookeeper-servers" optional="true" />[0m
[32m+[0m
[32m+    <!-- Substitutions for parameters of replicated tables.[0m
[32m+          Optional. If you don't use replicated tables, you could omit that.[0m
[32m+         See https://clickhouse.com/docs/en/engines/table-engines/mergetree-family/replication/#creating-replicated-tables[0m
[32m+      -->[0m
[32m+    <macros incl="macros" optional="true" />[0m
[32m+[0m
[32m+[0m
[32m+    <!-- Reloading interval for embedded dictionaries, in seconds. Default: 3600. -->[0m
[32m+    <builtin_dictionaries_reload_interval>3600</builtin_dictionaries_reload_interval>[0m
[32m+[0m
[32m+    <!-- If true, dictionaries are created lazily on first use. Otherwise they are initialised on server startup. Default: true -->[0m
[32m+    <!-- See also: https://clickhouse.com/docs/en/operations/server-configuration-parameters/settings/#server_configuration_parameters-dictionaries_lazy_load -->[0m
[32m+    <dictionaries_lazy_load>True</dictionaries_lazy_load>[0m
[32m+[0m
[32m+    <!-- Maximum session timeout, in seconds. Default: 3600. -->[0m
[32m+    <max_session_timeout>3600</max_session_timeout>[0m
[32m+[0m
[32m+    <!-- Default session timeout, in seconds. Default: 60. -->[0m
[32m+    <default_session_timeout>60</default_session_timeout>[0m
[32m+[0m
[32m+    <!-- Sending data to Graphite for monitoring. Several sections can be defined. -->[0m
[32m+    <!--[0m
[32m+        interval - send every X second[0m
[32m+        root_path - prefix for keys[0m
[32m+        hostname_in_path - append hostname to root_path (default = true)[0m
[32m+        metrics - send data from table system.metrics[0m
[32m+        events - send data from table system.events[0m
[32m+        asynchronous_metrics - send data from table system.asynchronous_metrics[0m
[32m+    -->[0m
[32m+    <!--[0m
[32m+    <graphite>[0m
[32m+        <host>localhost</host>[0m
[32m+        <port>42000</port>[0m
[32m+        <timeout>0.1</timeout>[0m
[32m+        <interval>60</interval>[0m
[32m+        <root_path>one_min</root_path>[0m
[32m+        <hostname_in_path>true</hostname_in_path>[0m
[32m+[0m
[32m+        <metrics>true</metrics>[0m
[32m+        <events>true</events>[0m
[32m+        <asynchronous_metrics>true</asynchronous_metrics>[0m
[32m+    </graphite>[0m
[32m+    <graphite>[0m
[32m+        <host>localhost</host>[0m
[32m+        <port>42000</port>[0m
[32m+        <timeout>0.1</timeout>[0m
[32m+        <interval>1</interval>[0m
[32m+        <root_path>one_sec</root_path>[0m
[32m+[0m
[32m+        <metrics>true</metrics>[0m
[32m+        <events>true</events>[0m
[32m+        <asynchronous_metrics>false</asynchronous_metrics>[0m
[32m+    </graphite>[0m
[32m+    -->[0m
[32m+[0m
[32m+[0m
[32m+    <!-- Query log. Used only for queries with setting log_queries = 1. -->[0m
[32m+    <query_log>[0m
[32m+        <!-- What table to insert data. If table is not exist, it will be created.[0m
[32m+             When query log structure is changed after system update,[0m
[32m+              then old table will be renamed and new table will be created automatically.[0m
[32m+        -->[0m
[32m+        <database>system</database>[0m
[32m+        <table>query_log</table>[0m
[32m+        <!--[0m
[32m+            PARTITION BY expr https://clickhouse.com/docs/en/table_engines/mergetree-family/custom_partitioning_key/[0m
[32m+            Example:[0m
[32m+                event_date[0m
[32m+                toMonday(event_date)[0m
[32m+                toYYYYMM(event_date)[0m
[32m+                toStartOfHour(event_time)[0m
[32m+        -->[0m
[32m+        <partition_by>toYYYYMM(event_date)</partition_by>[0m
[32m+        <!-- Interval of flushing data. -->[0m
[32m+        <flush_interval_milliseconds>7500</flush_interval_milliseconds>[0m
[32m+    </query_log>[0m
[32m+[0m
[32m+    <!-- Query thread log. Has information about all threads participated in query execution.[0m
[32m+         Used only for queries with setting log_query_threads = 1. -->[0m
[32m+    <query_thread_log>[0m
[32m+        <database>system</database>[0m
[32m+        <table>query_thread_log</table>[0m
[32m+        <partition_by>toYYYYMM(event_date)</partition_by>[0m
[32m+        [0m
[32m+        <flush_interval_milliseconds>7500</flush_interval_milliseconds>[0m
[32m+    </query_thread_log>[0m
[32m+[0m
[32m+    <!-- Uncomment if use part log.[0m
[32m+         Part log contains information about all actions with parts in MergeTree tables (creation, deletion, merges, downloads).[0m
[32m+    <part_log>[0m
[32m+        <database>system</database>[0m
[32m+        <table>part_log</table>[0m
[32m+        <flush_interval_milliseconds>7500</flush_interval_milliseconds>[0m
[32m+    </part_log>[0m
[32m+    -->[0m
[32m+[0m
[32m+[0m
[32m+    <!-- Parameters for embedded dictionaries, used in Yandex.Metrica.[0m
[32m+         See https://clickhouse.com/docs/en/dicts/internal_dicts/[0m
[32m+    -->[0m
[32m+[0m
[32m+    <!-- Path to file with region hierarchy. -->[0m
[32m+    <!-- <path_to_regions_hierarchy_file>/opt/geo/regions_hierarchy.txt</path_to_regions_hierarchy_file> -->[0m
[32m+[0m
[32m+    <!-- Path to directory with files containing names of regions -->[0m
[32m+    <!-- <path_to_regions_names_files>/opt/geo/</path_to_regions_names_files> -->[0m
[32m+[0m
[32m+[0m
[32m+    <!-- Configuration of external dictionaries. See:[0m
[32m+         https://clickhouse.com/docs/en/sql-reference/dictionaries/external-dictionaries/external-dicts[0m
[32m+    -->[0m
[32m+    <dictionaries_config>*_dictionary.xml</dictionaries_config>[0m
[32m+[0m
[32m+    <!-- Uncomment if you want data to be compressed 30-100% better.[0m
[32m+         Don't do that if you just started using ClickHouse.[0m
[32m+      -->[0m
[32m+    <compression incl="clickhouse_compression">[0m
[32m+    <!--[0m
[32m+        <!- - Set of variants. Checked in order. Last matching case wins. If nothing matches, lz4 will be used. - ->[0m
[32m+        <case>[0m
[32m+[0m
[32m+            <!- - Conditions. All must be satisfied. Some conditions may be omitted. - ->[0m
[32m+            <min_part_size>10000000000</min_part_size>        <!- - Min part size in bytes. - ->[0m
[32m+            <min_part_size_ratio>0.01</min_part_size_ratio>   <!- - Min size of part relative to whole table size. - ->[0m
[32m+[0m
[32m+            <!- - What compression method to use. - ->[0m
[32m+            <method>zstd</method>[0m
[32m+        </case>[0m
[32m+    -->[0m
[32m+    </compression>[0m
[32m+[0m
[32m+    <!-- Allow to execute distributed DDL queries (CREATE, DROP, ALTER, RENAME) on cluster.[0m
[32m+         Works only if ZooKeeper is enabled. Comment it if such functionality isn't required. -->[0m
[32m+    <distributed_ddl>[0m
[32m+        <!-- Path in ZooKeeper to queue with DDL queries -->[0m
[32m+        <path>/clickhouse/task_queue/ddl</path>[0m
[32m+[0m
[32m+        <!-- Settings from this profile will be used to execute DDL queries -->[0m
[32m+        <!-- <profile>default</profile> -->[0m
[32m+    </distributed_ddl>[0m
[32m+[0m
[32m+    <!-- Settings to fine tune MergeTree tables. See documentation in source code, in MergeTreeSettings.h -->[0m
[32m+        <merge_tree>[0m
[32m+        </merge_tree>[0m
[32m+[0m
[32m+    <!-- Protection from accidental DROP.[0m
[32m+         If size of a MergeTree table is greater than max_table_size_to_drop (in bytes) than table could not be dropped with any DROP query.[0m
[32m+         If you want do delete one table and don't want to restart clickhouse-server, you could create special file <clickhouse-path>/flags/force_drop_table and make DROP once.[0m
[32m+         By default max_table_size_to_drop is 50GB; max_table_size_to_drop=0 allows to DROP any tables.[0m
[32m+         The same for max_partition_size_to_drop.[0m
[32m+         Uncomment to disable protection.[0m
[32m+    -->[0m
[32m+    <!-- <max_table_size_to_drop>0</max_table_size_to_drop> -->[0m
[32m+    <!-- <max_partition_size_to_drop>0</max_partition_size_to_drop> -->[0m
[32m+[0m
[32m+    <!-- Example of parameters for GraphiteMergeTree table engine -->[0m
[32m+    <graphite_rollup_example>[0m
[32m+        <pattern>[0m
[32m+            <regexp>click_cost</regexp>[0m
[32m+            <function>any</function>[0m
[32m+            <retention>[0m
[32m+                <age>0</age>[0m
[32m+                <precision>3600</precision>[0m
[32m+            </retention>[0m
[32m+            <retention>[0m
[32m+                <age>86400</age>[0m
[32m+                <precision>60</precision>[0m
[32m+            </retention>[0m
[32m+        </pattern>[0m
[32m+        <default>[0m
[32m+            <function>max</function>[0m
[32m+            <retention>[0m
[32m+                <age>0</age>[0m
[32m+                <precision>60</precision>[0m
[32m+            </retention>[0m
[32m+            <retention>[0m
[32m+                <age>3600</age>[0m
[32m+                <precision>300</precision>[0m
[32m+            </retention>[0m
[32m+            <retention>[0m
[32m+                <age>86400</age>[0m
[32m+                <precision>3600</precision>[0m
[32m+            </retention>[0m
[32m+        </default>[0m
[32m+    </graphite_rollup_example>[0m
[32m+[0m
[32m+[0m
[32m+    <!-- Exposing metrics data for scraping from Prometheus. -->[0m
[32m+    <!--[0m
[32m+        endpoint – HTTP endpoint for scraping metrics by prometheus server. Start from ‘/’.[0m
[32m+        port – Port for endpoint.[0m
[32m+        metrics – Flag that sets to expose metrics from the system.metrics table.[0m
[32m+        events – Flag that sets to expose metrics from the system.events table.[0m
[32m+        asynchronous_metrics – Flag that sets to expose current metrics values from the system.asynchronous_metrics table.[0m
[32m+    -->[0m
[32m+    <!--[0m
[32m+    <prometheus>[0m
[32m+        <endpoint>/metrics</endpoint>[0m
[32m+        <port>8001</port>[0m
[32m+        <metrics>true</metrics>[0m
[32m+        <events>true</events>[0m
[32m+        <asynchronous_metrics>true</asynchronous_metrics>[0m
[32m+    </prometheus>[0m
[32m+    -->[0m
[32m+[0m
[32m+[0m
[32m+    <!-- Directory in <clickhouse-path> containing schema files for various input formats.[0m
[32m+         The directory will be created if it doesn't exist.[0m
[32m+      -->[0m
[32m+    <format_schema_path>/var/lib/clickhouse//format_schemas/</format_schema_path>[0m
[32m+[0m
[32m+    <!-- Uncomment to disable ClickHouse internal DNS caching. -->[0m
[32m+    <!-- <disable_internal_dns_cache>1</disable_internal_dns_cache> -->[0m
[32m+[0m
[32m+    <kafka>[0m
[32m+    </kafka>[0m
[32m+[0m
[32m+[0m
[32m+[0m
[32m+[0m
[32m+[0m
[32m+</clickhouse>[0m

[33mchanged: [centos_7][0m

TASK [ansible-clickhouse : Config | Generate users config] *********************
[33mchanged: [centos_7][0m

TASK [ansible-clickhouse : Config | Generate remote_servers config] ************
[36mskipping: [centos_7][0m

TASK [ansible-clickhouse : Config | Generate macros config] ********************
[36mskipping: [centos_7][0m

TASK [ansible-clickhouse : Config | Generate zookeeper servers config] *********
[36mskipping: [centos_7][0m

TASK [ansible-clickhouse : Config | Fix interserver_http_port and intersever_https_port collision] ***
[36mskipping: [centos_7][0m

TASK [ansible-clickhouse : Notify Handlers Now] ********************************

RUNNING HANDLER [ansible-clickhouse : Restart Clickhouse Service] **************
[32mok: [centos_7][0m

TASK [ansible-clickhouse : include_tasks] **************************************
[36mincluded: /home/lex/PycharmProjects/netologyProjects/devops-netology/ansible/08-ansible-05-testing/playbook/ansible-clickhouse/tasks/service.yml for centos_7[0m

TASK [ansible-clickhouse : Ensure clickhouse-server.service is enabled: True and state: restarted] ***
[31mfatal: [centos_7]: FAILED! => {"changed": false, "msg": "Unable to start service clickhouse-server.service: Job for clickhouse-server.service failed because the control process exited with error code. See \"systemctl status clickhouse-server.service\" and \"journalctl -xe\" for details.\n"}[0m

PLAY RECAP *********************************************************************
[31mcentos_7[0m                   : [32mok=18  [0m [33mchanged=7   [0m unreachable=0    [31mfailed=1   [0m [36mskipped=6   [0m rescued=0    ignored=0

[35m[DEPRECATION WARNING]: Ansible will require Python 3.8 or newer on the [0m
[35mcontroller starting with Ansible 2.12. Current version: 3.7.6 (default, Jan  8 [0m
[35m2020, 19:59:22) [GCC 7.3.0]. This feature will be removed from ansible-core in [0m
[35mversion 2.12. Deprecation warnings can be disabled by setting [0m
[35mdeprecation_warnings=False in ansible.cfg.[0m

PLAY [Destroy] *****************************************************************

TASK [Destroy molecule instance(s)] ********************************************
[33mchanged: [localhost] => (item=centos_7)[0m

TASK [Wait for instance(s) deletion to complete] *******************************
[1;30mFAILED - RETRYING: Wait for instance(s) deletion to complete (300 retries left).[0m
[33mchanged: [localhost] => (item=centos_7)[0m

TASK [Delete docker networks(s)] ***********************************************

PLAY RECAP *********************************************************************
[33mlocalhost[0m                  : [32mok=2   [0m [33mchanged=2   [0m unreachable=0    failed=0    [36mskipped=1   [0m rescued=0    ignored=0

```
2,3,4,5
* Добавлено 2 сценария ubuntu, centos
* Добавлен acert на наличие установленного пакета
* Роль отправлена в репо версией 1.0.1

```
(base) lex@lexhost ~/PycharmProjects/netologyProjects/sandbox/ansible/roles/vector $ molecule list
INFO     Running centos > list
INFO     Running default > list
INFO     Running ubuntu > list
                ╷             ╷                  ╷               ╷         ╷            
  Instance Name │ Driver Name │ Provisioner Name │ Scenario Name │ Created │ Converged  
╶───────────────┼─────────────┼──────────────────┼───────────────┼─────────┼───────────╴
  centos7       │ docker      │ ansible          │ centos        │ false   │ false      
  instance      │ delegated   │ ansible          │ default       │ false   │ false      
  ubuntu        │ docker      │ ansible          │ ubuntu        │ false   │ false      
                ╵             ╵                  ╵               ╵         ╵            
(base) lex@lexhost ~/PycharmProjects/netologyProjects/sandbox/ansible/roles/vector $ 



(base) lex@lexhost ~/PycharmProjects/netologyProjects/sandbox/ansible/roles/vector $ molecule test -s centos
INFO     centos scenario test matrix: dependency, lint, cleanup, destroy, syntax, create, prepare, converge, idempotence, side_effect, verify, cleanup, destroy
INFO     Performing prerun...
INFO     Set ANSIBLE_LIBRARY=/home/lex/.cache/ansible-compat/bf9340/modules:/home/lex/.ansible/plugins/modules:/usr/share/ansible/plugins/modules
INFO     Set ANSIBLE_COLLECTIONS_PATH=/home/lex/.cache/ansible-compat/bf9340/collections:/home/lex/.ansible/collections:/usr/share/ansible/collections
INFO     Set ANSIBLE_ROLES_PATH=/home/lex/.cache/ansible-compat/bf9340/roles:/home/lex/.ansible/roles:/usr/share/ansible/roles:/etc/ansible/roles
INFO     Running centos > dependency
WARNING  Skipping, missing the requirements file.
WARNING  Skipping, missing the requirements file.
INFO     Running centos > lint
INFO     Lint is disabled.
INFO     Running centos > cleanup
WARNING  Skipping, cleanup playbook not configured.
INFO     Running centos > destroy
INFO     Sanity checks: 'docker'
[DEPRECATION WARNING]: Ansible will require Python 3.8 or newer on the 
controller starting with Ansible 2.12. Current version: 3.7.6 (default, Jan  8 
2020, 19:59:22) [GCC 7.3.0]. This feature will be removed from ansible-core in 
version 2.12. Deprecation warnings can be disabled by setting 
deprecation_warnings=False in ansible.cfg.

PLAY [Destroy] *****************************************************************

TASK [Destroy molecule instance(s)] ********************************************
changed: [localhost] => (item=centos7)

TASK [Wait for instance(s) deletion to complete] *******************************
FAILED - RETRYING: Wait for instance(s) deletion to complete (300 retries left).
ok: [localhost] => (item=centos7)

TASK [Delete docker networks(s)] ***********************************************

PLAY RECAP *********************************************************************
localhost                  : ok=2    changed=1    unreachable=0    failed=0    skipped=1    rescued=0    ignored=0

INFO     Running centos > syntax
[DEPRECATION WARNING]: Ansible will require Python 3.8 or newer on the 
controller starting with Ansible 2.12. Current version: 3.7.6 (default, Jan  8 
2020, 19:59:22) [GCC 7.3.0]. This feature will be removed from ansible-core in 
version 2.12. Deprecation warnings can be disabled by setting 
deprecation_warnings=False in ansible.cfg.

playbook: /home/lex/PycharmProjects/netologyProjects/sandbox/ansible/roles/vector/molecule/centos/converge.yml
INFO     Running centos > create
[DEPRECATION WARNING]: Ansible will require Python 3.8 or newer on the 
controller starting with Ansible 2.12. Current version: 3.7.6 (default, Jan  8 
2020, 19:59:22) [GCC 7.3.0]. This feature will be removed from ansible-core in 
version 2.12. Deprecation warnings can be disabled by setting 
deprecation_warnings=False in ansible.cfg.

PLAY [Create] ******************************************************************

TASK [Log into a Docker registry] **********************************************
skipping: [localhost] => (item=None) 
skipping: [localhost]

TASK [Check presence of custom Dockerfiles] ************************************
ok: [localhost] => (item={'image': 'docker.io/pycontribs/centos:7', 'name': 'centos7', 'pre_build_image': True})

TASK [Create Dockerfiles from image names] *************************************
skipping: [localhost] => (item={'image': 'docker.io/pycontribs/centos:7', 'name': 'centos7', 'pre_build_image': True}) 

TASK [Discover local Docker images] ********************************************
ok: [localhost] => (item={'changed': False, 'skipped': True, 'skip_reason': 'Conditional result was False', 'item': {'image': 'docker.io/pycontribs/centos:7', 'name': 'centos7', 'pre_build_image': True}, 'ansible_loop_var': 'item', 'i': 0, 'ansible_index_var': 'i'})

TASK [Build an Ansible compatible image (new)] *********************************
skipping: [localhost] => (item=molecule_local/docker.io/pycontribs/centos:7) 

TASK [Create docker network(s)] ************************************************

TASK [Determine the CMD directives] ********************************************
ok: [localhost] => (item={'image': 'docker.io/pycontribs/centos:7', 'name': 'centos7', 'pre_build_image': True})

TASK [Create molecule instance(s)] *********************************************
changed: [localhost] => (item=centos7)

TASK [Wait for instance(s) creation to complete] *******************************
FAILED - RETRYING: Wait for instance(s) creation to complete (300 retries left).
changed: [localhost] => (item={'started': 1, 'finished': 0, 'ansible_job_id': '744627729494.20008', 'results_file': '/home/lex/.ansible_async/744627729494.20008', 'changed': True, 'failed': False, 'item': {'image': 'docker.io/pycontribs/centos:7', 'name': 'centos7', 'pre_build_image': True}, 'ansible_loop_var': 'item'})

PLAY RECAP *********************************************************************
localhost                  : ok=5    changed=2    unreachable=0    failed=0    skipped=4    rescued=0    ignored=0

INFO     Running centos > prepare
WARNING  Skipping, prepare playbook not configured.
INFO     Running centos > converge
[DEPRECATION WARNING]: Ansible will require Python 3.8 or newer on the 
controller starting with Ansible 2.12. Current version: 3.7.6 (default, Jan  8 
2020, 19:59:22) [GCC 7.3.0]. This feature will be removed from ansible-core in 
version 2.12. Deprecation warnings can be disabled by setting 
deprecation_warnings=False in ansible.cfg.

PLAY [Converge] ****************************************************************

TASK [Gathering Facts] *********************************************************
ok: [centos7]

TASK [Include vector] **********************************************************

TASK [vector : Download Vector vector-0.21.0-1.x86_64.rpm] *********************
changed: [centos7]

TASK [vector : Install Vector with Apt package - vector-0.21.0-1.x86_64.rpm] ***
skipping: [centos7]

TASK [vector : Install Vector with Yum package - vector-0.21.0-1.x86_64.rpm] ***
changed: [centos7]

PLAY RECAP *********************************************************************
centos7                    : ok=3    changed=2    unreachable=0    failed=0    skipped=1    rescued=0    ignored=0

INFO     Running centos > idempotence
[DEPRECATION WARNING]: Ansible will require Python 3.8 or newer on the 
controller starting with Ansible 2.12. Current version: 3.7.6 (default, Jan  8 
2020, 19:59:22) [GCC 7.3.0]. This feature will be removed from ansible-core in 
version 2.12. Deprecation warnings can be disabled by setting 
deprecation_warnings=False in ansible.cfg.

PLAY [Converge] ****************************************************************

TASK [Gathering Facts] *********************************************************
ok: [centos7]

TASK [Include vector] **********************************************************

TASK [vector : Download Vector vector-0.21.0-1.x86_64.rpm] *********************
ok: [centos7]

TASK [vector : Install Vector with Apt package - vector-0.21.0-1.x86_64.rpm] ***
skipping: [centos7]

TASK [vector : Install Vector with Yum package - vector-0.21.0-1.x86_64.rpm] ***
ok: [centos7]

PLAY RECAP *********************************************************************
centos7                    : ok=3    changed=0    unreachable=0    failed=0    skipped=1    rescued=0    ignored=0

INFO     Idempotence completed successfully.
INFO     Running centos > side_effect
WARNING  Skipping, side effect playbook not configured.
INFO     Running centos > verify
INFO     Running Ansible Verifier
[DEPRECATION WARNING]: Ansible will require Python 3.8 or newer on the 
controller starting with Ansible 2.12. Current version: 3.7.6 (default, Jan  8 
2020, 19:59:22) [GCC 7.3.0]. This feature will be removed from ansible-core in 
version 2.12. Deprecation warnings can be disabled by setting 
deprecation_warnings=False in ansible.cfg.

PLAY [Verify] ******************************************************************

TASK [Gathering Facts] *********************************************************
ok: [centos7]

TASK [Gather the package facts] ************************************************
ok: [centos7]

TASK [Assert Vector Packages] **************************************************
ok: [centos7] => {
    "changed": false,
    "msg": "All assertions passed"
}

PLAY RECAP *********************************************************************
centos7                    : ok=3    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0

INFO     Verifier completed successfully.
INFO     Running centos > cleanup
WARNING  Skipping, cleanup playbook not configured.
INFO     Running centos > destroy
[DEPRECATION WARNING]: Ansible will require Python 3.8 or newer on the 
controller starting with Ansible 2.12. Current version: 3.7.6 (default, Jan  8 
2020, 19:59:22) [GCC 7.3.0]. This feature will be removed from ansible-core in 
version 2.12. Deprecation warnings can be disabled by setting 
deprecation_warnings=False in ansible.cfg.

PLAY [Destroy] *****************************************************************

TASK [Destroy molecule instance(s)] ********************************************
changed: [localhost] => (item=centos7)

TASK [Wait for instance(s) deletion to complete] *******************************
FAILED - RETRYING: Wait for instance(s) deletion to complete (300 retries left).
changed: [localhost] => (item=centos7)

TASK [Delete docker networks(s)] ***********************************************

PLAY RECAP *********************************************************************
localhost                  : ok=2    changed=2    unreachable=0    failed=0    skipped=1    rescued=0    ignored=0

INFO     Pruning extra files from scenario ephemeral directory
(base) lex@lexhost ~/PycharmProjects/netologyProjects/sandbox/ansible/roles/vector $ 
```


### Tox

1. Добавьте в директорию с vector-role файлы из [директории](./example)
2. Запустите `docker run --privileged=True -v <path_to_repo>:/opt/vector-role -w /opt/vector-role -it aragast/netology:latest /bin/bash`, где path_to_repo - путь до корня репозитория с vector-role на вашей файловой системе.
3. Внутри контейнера выполните команду `tox`, посмотрите на вывод.
5. Создайте облегчённый сценарий для `molecule` с драйвером `molecule_podman`. Проверьте его на исполнимость.
6. Пропишите правильную команду в `tox.ini` для того чтобы запускался облегчённый сценарий.
8. Запустите команду `tox`. Убедитесь, что всё отработало успешно.
9. Добавьте новый тег на коммит с рабочим сценарием в соответствии с семантическим версионированием.

После выполнения у вас должно получится два сценария molecule и один tox.ini файл в репозитории. Ссылка на репозиторий являются ответами на домашнее задание. Не забудьте указать в ответе теги решений Tox и Molecule заданий.

## Ответ
```
в папке vector_role создан сценарий compatibility, в molecule.yml изменил матрицу
к примеру вот так:

scenario:
  test_sequence:
    - dependency
    - cleanup
    - create
    - destroy

файл tox.ini вызывается как и был описан :
 posargs:molecule test -s compatibility --destroy always
```

Пример вызова из образа aragast/netology
Выдавало много каких-то внутренних ошибок, но для примера виден запуск и результаты работы

```
INFO     Pruning extra files from scenario ephemeral directory
ERROR: InvocationError for command /opt/vector-role/.tox/py37-ansible210/bin/molecule test -s compatibility --destroy always (exited with code 1)
py37-ansible30 installed: ansible==3.0.0,ansible-base==2.10.17,ansible-compat==1.0.0,ansible-lint==5.1.3,arrow==1.2.3,bcrypt==4.0.1,binaryornot==0.4.4,bracex==2.3.post1,cached-property==1.5.2,Cerberus==1.3.2,certifi==2022.12.7,cffi==1.15.1,chardet==5.1.0,charset-normalizer==3.0.1,click==8.1.3,click-help-colors==0.9.1,commonmark==0.9.1,cookiecutter==2.1.1,cryptography==39.0.0,distro==1.8.0,enrich==1.2.7,idna==3.4,importlib-metadata==6.0.0,Jinja2==3.1.2,jinja2-time==0.2.0,jmespath==1.0.1,lxml==4.9.2,MarkupSafe==2.1.1,molecule==3.4.0,molecule-podman==1.0.1,packaging==23.0,paramiko==2.12.0,pathspec==0.10.3,pluggy==0.13.1,pycparser==2.21,Pygments==2.14.0,PyNaCl==1.5.0,python-dateutil==2.8.2,python-slugify==7.0.0,PyYAML==5.4.1,requests==2.28.2,rich==13.1.0,ruamel.yaml==0.17.21,ruamel.yaml.clib==0.2.7,selinux==0.2.1,six==1.16.0,subprocess-tee==0.3.5,tenacity==8.1.0,text-unidecode==1.3,typing_extensions==4.4.0,urllib3==1.26.14,wcmatch==8.4.1,yamllint==1.26.3,zipp==3.11.0
py37-ansible30 run-test-pre: PYTHONHASHSEED='1946935823'
py37-ansible30 run-test: commands[0] | molecule test -s compatibility --destroy always
INFO     compatibility scenario test matrix: dependency, cleanup, create, destroy
INFO     Performing prerun...
WARNING  Failed to locate command: [Errno 2] No such file or directory: 'git': 'git'
INFO     Guessed /opt/vector-role as project root directory
WARNING  Computed fully qualified role name of vector-role does not follow current galaxy requirements.
Please edit meta/main.yml and assure we can correctly determine full role name:

galaxy_info:
role_name: my_name  # if absent directory name hosting role is used instead
namespace: my_galaxy_namespace  # if absent, author is used instead

Namespace: https://galaxy.ansible.com/docs/contributing/namespaces.html#galaxy-namespace-limitations
Role: https://galaxy.ansible.com/docs/contributing/creating_role.html#role-names

As an alternative, you can add 'role-name' to either skip_list or warn_list.

INFO     Using /root/.cache/ansible-lint/b984a4/roles/vector-role symlink to current repository in order to enable Ansible to find the role using its expected full name.
INFO     Added ANSIBLE_ROLES_PATH=~/.ansible/roles:/usr/share/ansible/roles:/etc/ansible/roles:/root/.cache/ansible-lint/b984a4/roles
INFO     Running compatibility > dependency
WARNING  Skipping, missing the requirements file.
WARNING  Skipping, missing the requirements file.
INFO     Running compatibility > cleanup
WARNING  Skipping, cleanup playbook not configured.
INFO     Running compatibility > create
INFO     Sanity checks: 'podman'

PLAY [Create] ******************************************************************

TASK [get podman executable path] **********************************************
ok: [localhost]

TASK [save path to executable as fact] *****************************************
ok: [localhost]

TASK [Log into a container registry] *******************************************
skipping: [localhost] => (item="c7 registry username: None specified") 
skipping: [localhost] => (item="ub registry username: None specified") 

TASK [Check presence of custom Dockerfiles] ************************************
ok: [localhost] => (item=Dockerfile: None specified)
ok: [localhost] => (item=Dockerfile: None specified)

TASK [Create Dockerfiles from image names] *************************************
skipping: [localhost] => (item="Dockerfile: None specified; Image: docker.io/pycontribs/centos:7") 
skipping: [localhost] => (item="Dockerfile: None specified; Image: docker.io/pycontribs/ubuntu:latest") 

TASK [Discover local Podman images] ********************************************
ok: [localhost] => (item=c7)
ok: [localhost] => (item=ub)

TASK [Build an Ansible compatible image] ***************************************
skipping: [localhost] => (item=docker.io/pycontribs/centos:7) 
skipping: [localhost] => (item=docker.io/pycontribs/ubuntu:latest) 

TASK [Determine the CMD directives] ********************************************
ok: [localhost] => (item="c7 command: None specified")
ok: [localhost] => (item="ub command: None specified")

TASK [Remove possible pre-existing containers] *********************************
changed: [localhost]

TASK [Discover local podman networks] ******************************************
skipping: [localhost] => (item=c7: None specified) 
skipping: [localhost] => (item=ub: None specified) 

TASK [Create podman network dedicated to this scenario] ************************
skipping: [localhost]

TASK [Create molecule instance(s)] *********************************************
changed: [localhost] => (item=c7)
changed: [localhost] => (item=ub)

TASK [Wait for instance(s) creation to complete] *******************************
failed: [localhost] (item=c7) => {"ansible_job_id": "461746861167.4782", "ansible_loop_var": "item", "attempts": 1, "changed": true, "cmd": ["/usr/bin/podman", "run", "-d", "--name", "c7", "--hostname=c7", "docker.io/pycontribs/centos:7", "bash", "-c", "while true; do sleep 10000; done"], "delta": "0:00:00.301410", "end": "2023-01-17 16:41:35.021993", "finished": 1, "item": {"ansible_job_id": "461746861167.4782", "ansible_loop_var": "item", "changed": true, "failed": false, "finished": 0, "item": {"image": "docker.io/pycontribs/centos:7", "name": "c7", "pre_build_image": true}, "results_file": "/root/.ansible_async/461746861167.4782", "started": 1}, "msg": "non-zero return code", "rc": 126, "start": "2023-01-17 16:41:34.720583", "stderr": "time=\"2023-01-17T16:41:35Z\" level=error msg=\"Unmounting /var/lib/containers/storage/overlay/aa888537d51c551c1720088391d971fab49919322c356f935329c418dcff1c75/merged: invalid argument\"\nError: error mounting storage for container 9d2331adf9e1dfe1f3dd5a13adb487f3e08bad35e2f7defe9ffed2391b95bf2b: creating overlay mount to /var/lib/containers/storage/overlay/aa888537d51c551c1720088391d971fab49919322c356f935329c418dcff1c75/merged, mount_data=\"nodev,fsync=0,lowerdir=/var/lib/containers/storage/overlay/l/UDRNSL6CE6X3EZMOHHSYNMCER6:/var/lib/containers/storage/overlay/l/AB4CCXYDMJQ27GPLY2GOGVFFHV:/var/lib/containers/storage/overlay/l/LS6ZCVESEDSQL6CVDRH6DGXRFL,upperdir=/var/lib/containers/storage/overlay/aa888537d51c551c1720088391d971fab49919322c356f935329c418dcff1c75/diff,workdir=/var/lib/containers/storage/overlay/aa888537d51c551c1720088391d971fab49919322c356f935329c418dcff1c75/work\": using mount program /usr/bin/fuse-overlayfs: fuse-overlayfs: cannot read upper dir: Cannot allocate memory\n: exit status 1", "stderr_lines": ["time=\"2023-01-17T16:41:35Z\" level=error msg=\"Unmounting /var/lib/containers/storage/overlay/aa888537d51c551c1720088391d971fab49919322c356f935329c418dcff1c75/merged: invalid argument\"", "Error: error mounting storage for container 9d2331adf9e1dfe1f3dd5a13adb487f3e08bad35e2f7defe9ffed2391b95bf2b: creating overlay mount to /var/lib/containers/storage/overlay/aa888537d51c551c1720088391d971fab49919322c356f935329c418dcff1c75/merged, mount_data=\"nodev,fsync=0,lowerdir=/var/lib/containers/storage/overlay/l/UDRNSL6CE6X3EZMOHHSYNMCER6:/var/lib/containers/storage/overlay/l/AB4CCXYDMJQ27GPLY2GOGVFFHV:/var/lib/containers/storage/overlay/l/LS6ZCVESEDSQL6CVDRH6DGXRFL,upperdir=/var/lib/containers/storage/overlay/aa888537d51c551c1720088391d971fab49919322c356f935329c418dcff1c75/diff,workdir=/var/lib/containers/storage/overlay/aa888537d51c551c1720088391d971fab49919322c356f935329c418dcff1c75/work\": using mount program /usr/bin/fuse-overlayfs: fuse-overlayfs: cannot read upper dir: Cannot allocate memory", ": exit status 1"], "stdout": "", "stdout_lines": []}
failed: [localhost] (item=ub) => {"ansible_job_id": "401509308001.4804", "ansible_loop_var": "item", "attempts": 1, "changed": true, "cmd": ["/usr/bin/podman", "run", "-d", "--name", "ub", "--hostname=ub", "docker.io/pycontribs/ubuntu:latest", "bash", "-c", "while true; do sleep 10000; done"], "delta": "0:00:00.317556", "end": "2023-01-17 16:41:35.332290", "finished": 1, "item": {"ansible_job_id": "401509308001.4804", "ansible_loop_var": "item", "changed": true, "failed": false, "finished": 0, "item": {"image": "docker.io/pycontribs/ubuntu:latest", "name": "ub", "pre_build_image": true}, "results_file": "/root/.ansible_async/401509308001.4804", "started": 1}, "msg": "non-zero return code", "rc": 126, "start": "2023-01-17 16:41:35.014734", "stderr": "time=\"2023-01-17T16:41:35Z\" level=error msg=\"Unmounting /var/lib/containers/storage/overlay/db368f080ef304efe77a4f0d3e3a0a55a0594c84277118ec7fadec54ae859a92/merged: invalid argument\"\nError: error mounting storage for container 8b338328b5196798e1e3b9c599dccd5375264c663951e3ea6e168563ed8dcf3e: creating overlay mount to /var/lib/containers/storage/overlay/db368f080ef304efe77a4f0d3e3a0a55a0594c84277118ec7fadec54ae859a92/merged, mount_data=\"nodev,fsync=0,lowerdir=/var/lib/containers/storage/overlay/l/RYEGX44QM5XE6QO2JFU224AM4H:/var/lib/containers/storage/overlay/l/54FPCZZRT3T2TOG4SBZBNDIW3V:/var/lib/containers/storage/overlay/l/OVO6S7RXAL6HXVRQFJ2GXUTXSH:/var/lib/containers/storage/overlay/l/G5SMCFCDGFUX4DMODAEHSOZG2U:/var/lib/containers/storage/overlay/l/TIV634VKOOVUEAWFJQSNUZXWCP,upperdir=/var/lib/containers/storage/overlay/db368f080ef304efe77a4f0d3e3a0a55a0594c84277118ec7fadec54ae859a92/diff,workdir=/var/lib/containers/storage/overlay/db368f080ef304efe77a4f0d3e3a0a55a0594c84277118ec7fadec54ae859a92/work\": using mount program /usr/bin/fuse-overlayfs: fuse-overlayfs: cannot read upper dir: Cannot allocate memory\n: exit status 1", "stderr_lines": ["time=\"2023-01-17T16:41:35Z\" level=error msg=\"Unmounting /var/lib/containers/storage/overlay/db368f080ef304efe77a4f0d3e3a0a55a0594c84277118ec7fadec54ae859a92/merged: invalid argument\"", "Error: error mounting storage for container 8b338328b5196798e1e3b9c599dccd5375264c663951e3ea6e168563ed8dcf3e: creating overlay mount to /var/lib/containers/storage/overlay/db368f080ef304efe77a4f0d3e3a0a55a0594c84277118ec7fadec54ae859a92/merged, mount_data=\"nodev,fsync=0,lowerdir=/var/lib/containers/storage/overlay/l/RYEGX44QM5XE6QO2JFU224AM4H:/var/lib/containers/storage/overlay/l/54FPCZZRT3T2TOG4SBZBNDIW3V:/var/lib/containers/storage/overlay/l/OVO6S7RXAL6HXVRQFJ2GXUTXSH:/var/lib/containers/storage/overlay/l/G5SMCFCDGFUX4DMODAEHSOZG2U:/var/lib/containers/storage/overlay/l/TIV634VKOOVUEAWFJQSNUZXWCP,upperdir=/var/lib/containers/storage/overlay/db368f080ef304efe77a4f0d3e3a0a55a0594c84277118ec7fadec54ae859a92/diff,workdir=/var/lib/containers/storage/overlay/db368f080ef304efe77a4f0d3e3a0a55a0594c84277118ec7fadec54ae859a92/work\": using mount program /usr/bin/fuse-overlayfs: fuse-overlayfs: cannot read upper dir: Cannot allocate memory", ": exit status 1"], "stdout": "", "stdout_lines": []}

PLAY RECAP *********************************************************************
localhost                  : ok=7    changed=2    unreachable=0    failed=1    skipped=5    rescued=0    ignored=0

CRITICAL Ansible return code was 2, command was: ['ansible-playbook', '--inventory', '/root/.cache/molecule/vector-role/compatibility/inventory', '--skip-tags', 'molecule-notest,notest', '/opt/vector-role/.tox/py37-ansible30/lib/python3.7/site-packages/molecule_podman/playbooks/create.yml']
WARNING  An error occurred during the test sequence action: 'create'. Cleaning up.
INFO     Running compatibility > cleanup
WARNING  Skipping, cleanup playbook not configured.
INFO     Running compatibility > destroy

PLAY [Destroy] *****************************************************************

TASK [Destroy molecule instance(s)] ********************************************
changed: [localhost] => (item={'image': 'docker.io/pycontribs/centos:7', 'name': 'c7', 'pre_build_image': True})
changed: [localhost] => (item={'image': 'docker.io/pycontribs/ubuntu:latest', 'name': 'ub', 'pre_build_image': True})

TASK [Wait for instance(s) deletion to complete] *******************************
FAILED - RETRYING: Wait for instance(s) deletion to complete (300 retries left).
changed: [localhost] => (item={'started': 1, 'finished': 0, 'ansible_job_id': '431708139387.4896', 'results_file': '/root/.ansible_async/431708139387.4896', 'changed': True, 'failed': False, 'item': {'image': 'docker.io/pycontribs/centos:7', 'name': 'c7', 'pre_build_image': True}, 'ansible_loop_var': 'item'})
changed: [localhost] => (item={'started': 1, 'finished': 0, 'ansible_job_id': '44174574053.4916', 'results_file': '/root/.ansible_async/44174574053.4916', 'changed': True, 'failed': False, 'item': {'image': 'docker.io/pycontribs/ubuntu:latest', 'name': 'ub', 'pre_build_image': True}, 'ansible_loop_var': 'item'})

PLAY RECAP *********************************************************************
localhost                  : ok=2    changed=2    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0

INFO     Pruning extra files from scenario ephemeral directory
ERROR: InvocationError for command /opt/vector-role/.tox/py37-ansible30/bin/molecule test -s compatibility --destroy always (exited with code 1)
py39-ansible210 installed: ansible==2.10.7,ansible-base==2.10.17,ansible-compat==2.2.7,ansible-lint==5.1.3,arrow==1.2.3,attrs==22.2.0,bcrypt==4.0.1,binaryornot==0.4.4,bracex==2.3.post1,Cerberus==1.3.2,certifi==2022.12.7,cffi==1.15.1,chardet==5.1.0,charset-normalizer==3.0.1,click==8.1.3,click-help-colors==0.9.1,commonmark==0.9.1,cookiecutter==2.1.1,cryptography==39.0.0,distro==1.8.0,enrich==1.2.7,idna==3.4,Jinja2==3.1.2,jinja2-time==0.2.0,jmespath==1.0.1,jsonschema==4.17.3,lxml==4.9.2,MarkupSafe==2.1.1,molecule==3.4.0,molecule-podman==1.0.1,packaging==23.0,paramiko==2.12.0,pathspec==0.10.3,pluggy==0.13.1,pycparser==2.21,Pygments==2.14.0,PyNaCl==1.5.0,pyrsistent==0.19.3,python-dateutil==2.8.2,python-slugify==7.0.0,PyYAML==5.4.1,requests==2.28.2,rich==13.1.0,ruamel.yaml==0.17.21,ruamel.yaml.clib==0.2.7,selinux==0.3.0,six==1.16.0,subprocess-tee==0.4.1,tenacity==8.1.0,text-unidecode==1.3,urllib3==1.26.14,wcmatch==8.4.1,yamllint==1.26.3
py39-ansible210 run-test-pre: PYTHONHASHSEED='1946935823'
py39-ansible210 run-test: commands[0] | molecule test -s compatibility --destroy always
INFO     compatibility scenario test matrix: dependency, cleanup, create, destroy
INFO     Performing prerun...
WARNING  Failed to locate command: [Errno 2] No such file or directory: 'git'
INFO     Guessed /opt/vector-role as project root directory
WARNING  Computed fully qualified role name of vector-role does not follow current galaxy requirements.
Please edit meta/main.yml and assure we can correctly determine full role name:

galaxy_info:
role_name: my_name  # if absent directory name hosting role is used instead
namespace: my_galaxy_namespace  # if absent, author is used instead

Namespace: https://galaxy.ansible.com/docs/contributing/namespaces.html#galaxy-namespace-limitations
Role: https://galaxy.ansible.com/docs/contributing/creating_role.html#role-names

As an alternative, you can add 'role-name' to either skip_list or warn_list.

INFO     Using /root/.cache/ansible-lint/b984a4/roles/vector-role symlink to current repository in order to enable Ansible to find the role using its expected full name.
INFO     Added ANSIBLE_ROLES_PATH=~/.ansible/roles:/usr/share/ansible/roles:/etc/ansible/roles:/root/.cache/ansible-lint/b984a4/roles
INFO     Running compatibility > dependency
WARNING  Skipping, missing the requirements file.
WARNING  Skipping, missing the requirements file.
INFO     Running compatibility > cleanup
WARNING  Skipping, cleanup playbook not configured.
INFO     Running compatibility > create
INFO     Sanity checks: 'podman'

PLAY [Create] ******************************************************************

TASK [get podman executable path] **********************************************
ok: [localhost]

TASK [save path to executable as fact] *****************************************
ok: [localhost]

TASK [Log into a container registry] *******************************************
skipping: [localhost] => (item="c7 registry username: None specified") 
skipping: [localhost] => (item="ub registry username: None specified") 

TASK [Check presence of custom Dockerfiles] ************************************
ok: [localhost] => (item=Dockerfile: None specified)
ok: [localhost] => (item=Dockerfile: None specified)

TASK [Create Dockerfiles from image names] *************************************
skipping: [localhost] => (item="Dockerfile: None specified; Image: docker.io/pycontribs/centos:7") 
skipping: [localhost] => (item="Dockerfile: None specified; Image: docker.io/pycontribs/ubuntu:latest") 

TASK [Discover local Podman images] ********************************************
ok: [localhost] => (item=c7)
ok: [localhost] => (item=ub)

TASK [Build an Ansible compatible image] ***************************************
skipping: [localhost] => (item=docker.io/pycontribs/centos:7) 
skipping: [localhost] => (item=docker.io/pycontribs/ubuntu:latest) 

TASK [Determine the CMD directives] ********************************************
ok: [localhost] => (item="c7 command: None specified")
ok: [localhost] => (item="ub command: None specified")

TASK [Remove possible pre-existing containers] *********************************
changed: [localhost]

TASK [Discover local podman networks] ******************************************
skipping: [localhost] => (item=c7: None specified) 
skipping: [localhost] => (item=ub: None specified) 

TASK [Create podman network dedicated to this scenario] ************************
skipping: [localhost]

TASK [Create molecule instance(s)] *********************************************
changed: [localhost] => (item=c7)
changed: [localhost] => (item=ub)

TASK [Wait for instance(s) creation to complete] *******************************
failed: [localhost] (item=c7) => {"ansible_job_id": "581302877171.5213", "ansible_loop_var": "item", "attempts": 1, "changed": true, "cmd": ["/usr/bin/podman", "run", "-d", "--name", "c7", "--hostname=c7", "docker.io/pycontribs/centos:7", "bash", "-c", "while true; do sleep 10000; done"], "delta": "0:00:00.291058", "end": "2023-01-17 16:41:53.584534", "finished": 1, "item": {"ansible_job_id": "581302877171.5213", "ansible_loop_var": "item", "changed": true, "failed": false, "finished": 0, "item": {"image": "docker.io/pycontribs/centos:7", "name": "c7", "pre_build_image": true}, "results_file": "/root/.ansible_async/581302877171.5213", "started": 1}, "msg": "non-zero return code", "rc": 126, "start": "2023-01-17 16:41:53.293476", "stderr": "time=\"2023-01-17T16:41:53Z\" level=error msg=\"Unmounting /var/lib/containers/storage/overlay/068dcbb97159ddfdea10705045985745301606c0eca67c6a655a161fcb1cd7a9/merged: invalid argument\"\nError: error mounting storage for container 77dda5f18cda1ff3ba67d45ac6d7ba4cfce86e6696d83e46fef24eeb6d1c3038: creating overlay mount to /var/lib/containers/storage/overlay/068dcbb97159ddfdea10705045985745301606c0eca67c6a655a161fcb1cd7a9/merged, mount_data=\"nodev,fsync=0,lowerdir=/var/lib/containers/storage/overlay/l/UDRNSL6CE6X3EZMOHHSYNMCER6:/var/lib/containers/storage/overlay/l/AB4CCXYDMJQ27GPLY2GOGVFFHV:/var/lib/containers/storage/overlay/l/LS6ZCVESEDSQL6CVDRH6DGXRFL,upperdir=/var/lib/containers/storage/overlay/068dcbb97159ddfdea10705045985745301606c0eca67c6a655a161fcb1cd7a9/diff,workdir=/var/lib/containers/storage/overlay/068dcbb97159ddfdea10705045985745301606c0eca67c6a655a161fcb1cd7a9/work\": using mount program /usr/bin/fuse-overlayfs: fuse-overlayfs: cannot read upper dir: Cannot allocate memory\n: exit status 1", "stderr_lines": ["time=\"2023-01-17T16:41:53Z\" level=error msg=\"Unmounting /var/lib/containers/storage/overlay/068dcbb97159ddfdea10705045985745301606c0eca67c6a655a161fcb1cd7a9/merged: invalid argument\"", "Error: error mounting storage for container 77dda5f18cda1ff3ba67d45ac6d7ba4cfce86e6696d83e46fef24eeb6d1c3038: creating overlay mount to /var/lib/containers/storage/overlay/068dcbb97159ddfdea10705045985745301606c0eca67c6a655a161fcb1cd7a9/merged, mount_data=\"nodev,fsync=0,lowerdir=/var/lib/containers/storage/overlay/l/UDRNSL6CE6X3EZMOHHSYNMCER6:/var/lib/containers/storage/overlay/l/AB4CCXYDMJQ27GPLY2GOGVFFHV:/var/lib/containers/storage/overlay/l/LS6ZCVESEDSQL6CVDRH6DGXRFL,upperdir=/var/lib/containers/storage/overlay/068dcbb97159ddfdea10705045985745301606c0eca67c6a655a161fcb1cd7a9/diff,workdir=/var/lib/containers/storage/overlay/068dcbb97159ddfdea10705045985745301606c0eca67c6a655a161fcb1cd7a9/work\": using mount program /usr/bin/fuse-overlayfs: fuse-overlayfs: cannot read upper dir: Cannot allocate memory", ": exit status 1"], "stdout": "", "stdout_lines": []}
failed: [localhost] (item=ub) => {"ansible_job_id": "73921104425.5231", "ansible_loop_var": "item", "attempts": 1, "changed": true, "cmd": ["/usr/bin/podman", "run", "-d", "--name", "ub", "--hostname=ub", "docker.io/pycontribs/ubuntu:latest", "bash", "-c", "while true; do sleep 10000; done"], "delta": "0:00:00.302996", "end": "2023-01-17 16:41:53.861283", "finished": 1, "item": {"ansible_job_id": "73921104425.5231", "ansible_loop_var": "item", "changed": true, "failed": false, "finished": 0, "item": {"image": "docker.io/pycontribs/ubuntu:latest", "name": "ub", "pre_build_image": true}, "results_file": "/root/.ansible_async/73921104425.5231", "started": 1}, "msg": "non-zero return code", "rc": 126, "start": "2023-01-17 16:41:53.558287", "stderr": "time=\"2023-01-17T16:41:53Z\" level=error msg=\"Unmounting /var/lib/containers/storage/overlay/6690a1c42fd3f97395000377496ff08e7dbb28612cc0ab370371c7cd079b6f61/merged: invalid argument\"\nError: error mounting storage for container a9e7e9bf7acdcc01bc901a6f225c14dd71f9276ae0333f1aa81b18fefad72f0f: creating overlay mount to /var/lib/containers/storage/overlay/6690a1c42fd3f97395000377496ff08e7dbb28612cc0ab370371c7cd079b6f61/merged, mount_data=\"nodev,fsync=0,lowerdir=/var/lib/containers/storage/overlay/l/RYEGX44QM5XE6QO2JFU224AM4H:/var/lib/containers/storage/overlay/l/54FPCZZRT3T2TOG4SBZBNDIW3V:/var/lib/containers/storage/overlay/l/OVO6S7RXAL6HXVRQFJ2GXUTXSH:/var/lib/containers/storage/overlay/l/G5SMCFCDGFUX4DMODAEHSOZG2U:/var/lib/containers/storage/overlay/l/TIV634VKOOVUEAWFJQSNUZXWCP,upperdir=/var/lib/containers/storage/overlay/6690a1c42fd3f97395000377496ff08e7dbb28612cc0ab370371c7cd079b6f61/diff,workdir=/var/lib/containers/storage/overlay/6690a1c42fd3f97395000377496ff08e7dbb28612cc0ab370371c7cd079b6f61/work\": using mount program /usr/bin/fuse-overlayfs: fuse-overlayfs: cannot read upper dir: Cannot allocate memory\n: exit status 1", "stderr_lines": ["time=\"2023-01-17T16:41:53Z\" level=error msg=\"Unmounting /var/lib/containers/storage/overlay/6690a1c42fd3f97395000377496ff08e7dbb28612cc0ab370371c7cd079b6f61/merged: invalid argument\"", "Error: error mounting storage for container a9e7e9bf7acdcc01bc901a6f225c14dd71f9276ae0333f1aa81b18fefad72f0f: creating overlay mount to /var/lib/containers/storage/overlay/6690a1c42fd3f97395000377496ff08e7dbb28612cc0ab370371c7cd079b6f61/merged, mount_data=\"nodev,fsync=0,lowerdir=/var/lib/containers/storage/overlay/l/RYEGX44QM5XE6QO2JFU224AM4H:/var/lib/containers/storage/overlay/l/54FPCZZRT3T2TOG4SBZBNDIW3V:/var/lib/containers/storage/overlay/l/OVO6S7RXAL6HXVRQFJ2GXUTXSH:/var/lib/containers/storage/overlay/l/G5SMCFCDGFUX4DMODAEHSOZG2U:/var/lib/containers/storage/overlay/l/TIV634VKOOVUEAWFJQSNUZXWCP,upperdir=/var/lib/containers/storage/overlay/6690a1c42fd3f97395000377496ff08e7dbb28612cc0ab370371c7cd079b6f61/diff,workdir=/var/lib/containers/storage/overlay/6690a1c42fd3f97395000377496ff08e7dbb28612cc0ab370371c7cd079b6f61/work\": using mount program /usr/bin/fuse-overlayfs: fuse-overlayfs: cannot read upper dir: Cannot allocate memory", ": exit status 1"], "stdout": "", "stdout_lines": []}

PLAY RECAP *********************************************************************
localhost                  : ok=7    changed=2    unreachable=0    failed=1    skipped=5    rescued=0    ignored=0

CRITICAL Ansible return code was 2, command was: ['ansible-playbook', '--inventory', '/root/.cache/molecule/vector-role/compatibility/inventory', '--skip-tags', 'molecule-notest,notest', '/opt/vector-role/.tox/py39-ansible210/lib/python3.9/site-packages/molecule_podman/playbooks/create.yml']
WARNING  An error occurred during the test sequence action: 'create'. Cleaning up.
INFO     Running compatibility > cleanup
WARNING  Skipping, cleanup playbook not configured.
INFO     Running compatibility > destroy

PLAY [Destroy] *****************************************************************

TASK [Destroy molecule instance(s)] ********************************************
changed: [localhost] => (item={'image': 'docker.io/pycontribs/centos:7', 'name': 'c7', 'pre_build_image': True})
changed: [localhost] => (item={'image': 'docker.io/pycontribs/ubuntu:latest', 'name': 'ub', 'pre_build_image': True})

TASK [Wait for instance(s) deletion to complete] *******************************
FAILED - RETRYING: Wait for instance(s) deletion to complete (300 retries left).
changed: [localhost] => (item={'started': 1, 'finished': 0, 'ansible_job_id': '224860935727.5314', 'results_file': '/root/.ansible_async/224860935727.5314', 'changed': True, 'failed': False, 'item': {'image': 'docker.io/pycontribs/centos:7', 'name': 'c7', 'pre_build_image': True}, 'ansible_loop_var': 'item'})
changed: [localhost] => (item={'started': 1, 'finished': 0, 'ansible_job_id': '153091646926.5332', 'results_file': '/root/.ansible_async/153091646926.5332', 'changed': True, 'failed': False, 'item': {'image': 'docker.io/pycontribs/ubuntu:latest', 'name': 'ub', 'pre_build_image': True}, 'ansible_loop_var': 'item'})

PLAY RECAP *********************************************************************
localhost                  : ok=2    changed=2    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0

INFO     Pruning extra files from scenario ephemeral directory
ERROR: InvocationError for command /opt/vector-role/.tox/py39-ansible210/bin/molecule test -s compatibility --destroy always (exited with code 1)
py39-ansible30 installed: ansible==3.0.0,ansible-base==2.10.17,ansible-compat==2.2.7,ansible-lint==5.1.3,arrow==1.2.3,attrs==22.2.0,bcrypt==4.0.1,binaryornot==0.4.4,bracex==2.3.post1,Cerberus==1.3.2,certifi==2022.12.7,cffi==1.15.1,chardet==5.1.0,charset-normalizer==3.0.1,click==8.1.3,click-help-colors==0.9.1,commonmark==0.9.1,cookiecutter==2.1.1,cryptography==39.0.0,distro==1.8.0,enrich==1.2.7,idna==3.4,Jinja2==3.1.2,jinja2-time==0.2.0,jmespath==1.0.1,jsonschema==4.17.3,lxml==4.9.2,MarkupSafe==2.1.1,molecule==3.4.0,molecule-podman==1.0.1,packaging==23.0,paramiko==2.12.0,pathspec==0.10.3,pluggy==0.13.1,pycparser==2.21,Pygments==2.14.0,PyNaCl==1.5.0,pyrsistent==0.19.3,python-dateutil==2.8.2,python-slugify==7.0.0,PyYAML==5.4.1,requests==2.28.2,rich==13.1.0,ruamel.yaml==0.17.21,ruamel.yaml.clib==0.2.7,selinux==0.3.0,six==1.16.0,subprocess-tee==0.4.1,tenacity==8.1.0,text-unidecode==1.3,urllib3==1.26.14,wcmatch==8.4.1,yamllint==1.26.3
py39-ansible30 run-test-pre: PYTHONHASHSEED='1946935823'
py39-ansible30 run-test: commands[0] | molecule test -s compatibility --destroy always
INFO     compatibility scenario test matrix: dependency, cleanup, create, destroy
INFO     Performing prerun...
WARNING  Failed to locate command: [Errno 2] No such file or directory: 'git'
INFO     Guessed /opt/vector-role as project root directory
WARNING  Computed fully qualified role name of vector-role does not follow current galaxy requirements.
Please edit meta/main.yml and assure we can correctly determine full role name:

galaxy_info:
role_name: my_name  # if absent directory name hosting role is used instead
namespace: my_galaxy_namespace  # if absent, author is used instead

Namespace: https://galaxy.ansible.com/docs/contributing/namespaces.html#galaxy-namespace-limitations
Role: https://galaxy.ansible.com/docs/contributing/creating_role.html#role-names

As an alternative, you can add 'role-name' to either skip_list or warn_list.

INFO     Using /root/.cache/ansible-lint/b984a4/roles/vector-role symlink to current repository in order to enable Ansible to find the role using its expected full name.
INFO     Added ANSIBLE_ROLES_PATH=~/.ansible/roles:/usr/share/ansible/roles:/etc/ansible/roles:/root/.cache/ansible-lint/b984a4/roles
INFO     Running compatibility > dependency
WARNING  Skipping, missing the requirements file.
WARNING  Skipping, missing the requirements file.
INFO     Running compatibility > cleanup
WARNING  Skipping, cleanup playbook not configured.
INFO     Running compatibility > create
INFO     Sanity checks: 'podman'

PLAY [Create] ******************************************************************

TASK [get podman executable path] **********************************************
ok: [localhost]

TASK [save path to executable as fact] *****************************************
ok: [localhost]

TASK [Log into a container registry] *******************************************
skipping: [localhost] => (item="c7 registry username: None specified") 
skipping: [localhost] => (item="ub registry username: None specified") 

TASK [Check presence of custom Dockerfiles] ************************************
ok: [localhost] => (item=Dockerfile: None specified)
ok: [localhost] => (item=Dockerfile: None specified)

TASK [Create Dockerfiles from image names] *************************************
skipping: [localhost] => (item="Dockerfile: None specified; Image: docker.io/pycontribs/centos:7") 
skipping: [localhost] => (item="Dockerfile: None specified; Image: docker.io/pycontribs/ubuntu:latest") 

TASK [Discover local Podman images] ********************************************
ok: [localhost] => (item=c7)
ok: [localhost] => (item=ub)

TASK [Build an Ansible compatible image] ***************************************
skipping: [localhost] => (item=docker.io/pycontribs/centos:7) 
skipping: [localhost] => (item=docker.io/pycontribs/ubuntu:latest) 

TASK [Determine the CMD directives] ********************************************
ok: [localhost] => (item="c7 command: None specified")
ok: [localhost] => (item="ub command: None specified")

TASK [Remove possible pre-existing containers] *********************************
changed: [localhost]

TASK [Discover local podman networks] ******************************************
skipping: [localhost] => (item=c7: None specified) 
skipping: [localhost] => (item=ub: None specified) 

TASK [Create podman network dedicated to this scenario] ************************
skipping: [localhost]

TASK [Create molecule instance(s)] *********************************************
changed: [localhost] => (item=c7)
changed: [localhost] => (item=ub)

TASK [Wait for instance(s) creation to complete] *******************************
failed: [localhost] (item=c7) => {"ansible_job_id": "82800258168.5614", "ansible_loop_var": "item", "attempts": 1, "changed": true, "cmd": ["/usr/bin/podman", "run", "-d", "--name", "c7", "--hostname=c7", "docker.io/pycontribs/centos:7", "bash", "-c", "while true; do sleep 10000; done"], "delta": "0:00:00.332998", "end": "2023-01-17 16:42:12.138001", "finished": 1, "item": {"ansible_job_id": "82800258168.5614", "ansible_loop_var": "item", "changed": true, "failed": false, "finished": 0, "item": {"image": "docker.io/pycontribs/centos:7", "name": "c7", "pre_build_image": true}, "results_file": "/root/.ansible_async/82800258168.5614", "started": 1}, "msg": "non-zero return code", "rc": 126, "start": "2023-01-17 16:42:11.805003", "stderr": "time=\"2023-01-17T16:42:12Z\" level=error msg=\"Unmounting /var/lib/containers/storage/overlay/26c30c1db14dc379acd5d790978e010832cf0e5459cdbf6f27a6dc6db1762889/merged: invalid argument\"\nError: error mounting storage for container 6c39eba1285cab6dcfce997051659208287ce255e2f853f5e2a2b439207cd0fc: creating overlay mount to /var/lib/containers/storage/overlay/26c30c1db14dc379acd5d790978e010832cf0e5459cdbf6f27a6dc6db1762889/merged, mount_data=\"nodev,fsync=0,lowerdir=/var/lib/containers/storage/overlay/l/UDRNSL6CE6X3EZMOHHSYNMCER6:/var/lib/containers/storage/overlay/l/AB4CCXYDMJQ27GPLY2GOGVFFHV:/var/lib/containers/storage/overlay/l/LS6ZCVESEDSQL6CVDRH6DGXRFL,upperdir=/var/lib/containers/storage/overlay/26c30c1db14dc379acd5d790978e010832cf0e5459cdbf6f27a6dc6db1762889/diff,workdir=/var/lib/containers/storage/overlay/26c30c1db14dc379acd5d790978e010832cf0e5459cdbf6f27a6dc6db1762889/work\": using mount program /usr/bin/fuse-overlayfs: fuse-overlayfs: cannot read upper dir: Cannot allocate memory\n: exit status 1", "stderr_lines": ["time=\"2023-01-17T16:42:12Z\" level=error msg=\"Unmounting /var/lib/containers/storage/overlay/26c30c1db14dc379acd5d790978e010832cf0e5459cdbf6f27a6dc6db1762889/merged: invalid argument\"", "Error: error mounting storage for container 6c39eba1285cab6dcfce997051659208287ce255e2f853f5e2a2b439207cd0fc: creating overlay mount to /var/lib/containers/storage/overlay/26c30c1db14dc379acd5d790978e010832cf0e5459cdbf6f27a6dc6db1762889/merged, mount_data=\"nodev,fsync=0,lowerdir=/var/lib/containers/storage/overlay/l/UDRNSL6CE6X3EZMOHHSYNMCER6:/var/lib/containers/storage/overlay/l/AB4CCXYDMJQ27GPLY2GOGVFFHV:/var/lib/containers/storage/overlay/l/LS6ZCVESEDSQL6CVDRH6DGXRFL,upperdir=/var/lib/containers/storage/overlay/26c30c1db14dc379acd5d790978e010832cf0e5459cdbf6f27a6dc6db1762889/diff,workdir=/var/lib/containers/storage/overlay/26c30c1db14dc379acd5d790978e010832cf0e5459cdbf6f27a6dc6db1762889/work\": using mount program /usr/bin/fuse-overlayfs: fuse-overlayfs: cannot read upper dir: Cannot allocate memory", ": exit status 1"], "stdout": "", "stdout_lines": []}
failed: [localhost] (item=ub) => {"ansible_job_id": "886741370253.5632", "ansible_loop_var": "item", "attempts": 1, "changed": true, "cmd": ["/usr/bin/podman", "run", "-d", "--name", "ub", "--hostname=ub", "docker.io/pycontribs/ubuntu:latest", "bash", "-c", "while true; do sleep 10000; done"], "delta": "0:00:00.354548", "end": "2023-01-17 16:42:12.416192", "finished": 1, "item": {"ansible_job_id": "886741370253.5632", "ansible_loop_var": "item", "changed": true, "failed": false, "finished": 0, "item": {"image": "docker.io/pycontribs/ubuntu:latest", "name": "ub", "pre_build_image": true}, "results_file": "/root/.ansible_async/886741370253.5632", "started": 1}, "msg": "non-zero return code", "rc": 126, "start": "2023-01-17 16:42:12.061644", "stderr": "time=\"2023-01-17T16:42:12Z\" level=error msg=\"Unmounting /var/lib/containers/storage/overlay/714e054bd0cc6aa829377f07bcae7a2e7137daad6822046a9b11a7297e0f19e7/merged: invalid argument\"\nError: error mounting storage for container a9769721116c1e10cbf03d4314105b558bcc2e805277468cc2c54e18f9587a12: creating overlay mount to /var/lib/containers/storage/overlay/714e054bd0cc6aa829377f07bcae7a2e7137daad6822046a9b11a7297e0f19e7/merged, mount_data=\"nodev,fsync=0,lowerdir=/var/lib/containers/storage/overlay/l/RYEGX44QM5XE6QO2JFU224AM4H:/var/lib/containers/storage/overlay/l/54FPCZZRT3T2TOG4SBZBNDIW3V:/var/lib/containers/storage/overlay/l/OVO6S7RXAL6HXVRQFJ2GXUTXSH:/var/lib/containers/storage/overlay/l/G5SMCFCDGFUX4DMODAEHSOZG2U:/var/lib/containers/storage/overlay/l/TIV634VKOOVUEAWFJQSNUZXWCP,upperdir=/var/lib/containers/storage/overlay/714e054bd0cc6aa829377f07bcae7a2e7137daad6822046a9b11a7297e0f19e7/diff,workdir=/var/lib/containers/storage/overlay/714e054bd0cc6aa829377f07bcae7a2e7137daad6822046a9b11a7297e0f19e7/work\": using mount program /usr/bin/fuse-overlayfs: fuse-overlayfs: cannot read upper dir: Cannot allocate memory\n: exit status 1", "stderr_lines": ["time=\"2023-01-17T16:42:12Z\" level=error msg=\"Unmounting /var/lib/containers/storage/overlay/714e054bd0cc6aa829377f07bcae7a2e7137daad6822046a9b11a7297e0f19e7/merged: invalid argument\"", "Error: error mounting storage for container a9769721116c1e10cbf03d4314105b558bcc2e805277468cc2c54e18f9587a12: creating overlay mount to /var/lib/containers/storage/overlay/714e054bd0cc6aa829377f07bcae7a2e7137daad6822046a9b11a7297e0f19e7/merged, mount_data=\"nodev,fsync=0,lowerdir=/var/lib/containers/storage/overlay/l/RYEGX44QM5XE6QO2JFU224AM4H:/var/lib/containers/storage/overlay/l/54FPCZZRT3T2TOG4SBZBNDIW3V:/var/lib/containers/storage/overlay/l/OVO6S7RXAL6HXVRQFJ2GXUTXSH:/var/lib/containers/storage/overlay/l/G5SMCFCDGFUX4DMODAEHSOZG2U:/var/lib/containers/storage/overlay/l/TIV634VKOOVUEAWFJQSNUZXWCP,upperdir=/var/lib/containers/storage/overlay/714e054bd0cc6aa829377f07bcae7a2e7137daad6822046a9b11a7297e0f19e7/diff,workdir=/var/lib/containers/storage/overlay/714e054bd0cc6aa829377f07bcae7a2e7137daad6822046a9b11a7297e0f19e7/work\": using mount program /usr/bin/fuse-overlayfs: fuse-overlayfs: cannot read upper dir: Cannot allocate memory", ": exit status 1"], "stdout": "", "stdout_lines": []}

PLAY RECAP *********************************************************************
localhost                  : ok=7    changed=2    unreachable=0    failed=1    skipped=5    rescued=0    ignored=0

CRITICAL Ansible return code was 2, command was: ['ansible-playbook', '--inventory', '/root/.cache/molecule/vector-role/compatibility/inventory', '--skip-tags', 'molecule-notest,notest', '/opt/vector-role/.tox/py39-ansible30/lib/python3.9/site-packages/molecule_podman/playbooks/create.yml']
WARNING  An error occurred during the test sequence action: 'create'. Cleaning up.
INFO     Running compatibility > cleanup
WARNING  Skipping, cleanup playbook not configured.
INFO     Running compatibility > destroy

PLAY [Destroy] *****************************************************************

TASK [Destroy molecule instance(s)] ********************************************
changed: [localhost] => (item={'image': 'docker.io/pycontribs/centos:7', 'name': 'c7', 'pre_build_image': True})
changed: [localhost] => (item={'image': 'docker.io/pycontribs/ubuntu:latest', 'name': 'ub', 'pre_build_image': True})

TASK [Wait for instance(s) deletion to complete] *******************************
FAILED - RETRYING: Wait for instance(s) deletion to complete (300 retries left).
changed: [localhost] => (item={'started': 1, 'finished': 0, 'ansible_job_id': '226897173421.5712', 'results_file': '/root/.ansible_async/226897173421.5712', 'changed': True, 'failed': False, 'item': {'image': 'docker.io/pycontribs/centos:7', 'name': 'c7', 'pre_build_image': True}, 'ansible_loop_var': 'item'})
changed: [localhost] => (item={'started': 1, 'finished': 0, 'ansible_job_id': '455082086987.5730', 'results_file': '/root/.ansible_async/455082086987.5730', 'changed': True, 'failed': False, 'item': {'image': 'docker.io/pycontribs/ubuntu:latest', 'name': 'ub', 'pre_build_image': True}, 'ansible_loop_var': 'item'})

PLAY RECAP *********************************************************************
localhost                  : ok=2    changed=2    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0

INFO     Pruning extra files from scenario ephemeral directory
ERROR: InvocationError for command /opt/vector-role/.tox/py39-ansible30/bin/molecule test -s compatibility --destroy always (exited with code 1)
_______________________________________________________________________________ summary ________________________________________________________________________________
ERROR:   py37-ansible210: commands failed
ERROR:   py37-ansible30: commands failed
ERROR:   py39-ansible210: commands failed
ERROR:   py39-ansible30: commands failed
[root@lexhost vector-role]# 

```

## Необязательная часть

1. Проделайте схожие манипуляции для создания роли lighthouse.
2. Создайте сценарий внутри любой из своих ролей, который умеет поднимать весь стек при помощи всех ролей.
3. Убедитесь в работоспособности своего стека. Создайте отдельный verify.yml, который будет проверять работоспособность интеграции всех инструментов между ними.
4. Выложите свои roles в репозитории. В ответ приведите ссылки.

---

### Как оформить ДЗ?

Выполненное домашнее задание пришлите ссылкой на .md-файл в вашем репозитории.

---
