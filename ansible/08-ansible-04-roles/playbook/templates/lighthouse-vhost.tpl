<VirtualHost *:80>
ServerAdmin webmaster@localhost
DocumentRoot {{ doc_root }}/lighthouse
<Directory {{ doc_root }}/lighthouse >
 AllowOverride All
 Require all granted
</Directory>
</VirtualHost>