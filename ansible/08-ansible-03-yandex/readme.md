# Домашнее задание к занятию "08.03 Использование Yandex Cloud"

## Подготовка к выполнению

1. (Необязательно) Познакомтесь с [lighthouse](https://youtu.be/ymlrNlaHzIY?t=929)
2. Подготовьте в Yandex Cloud три хоста: для `clickhouse`, для `vector` и для `lighthouse`.

Ссылка на репозиторий LightHouse: https://github.com/VKCOM/lighthouse

## Основная часть

1. Допишите playbook: нужно сделать ещё один play, который устанавливает и настраивает lighthouse.
2. При создании tasks рекомендую использовать модули: `get_url`, `template`, `yum`, `apt`.
3. Tasks должны: скачать статику lighthouse, установить nginx или любой другой webserver, настроить его конфиг для открытия lighthouse, запустить webserver.
4. Приготовьте свой собственный inventory файл `prod.yml`.
5. Запустите `ansible-lint site.yml` и исправьте ошибки, если они есть.
6. Попробуйте запустить playbook на этом окружении с флагом `--check`.
7. Запустите playbook на `prod.yml` окружении с флагом `--diff`. Убедитесь, что изменения на системе произведены.
8. Повторно запустите playbook с флагом `--diff` и убедитесь, что playbook идемпотентен.
9. Подготовьте README.md файл по своему playbook. В нём должно быть описано: что делает playbook, какие у него есть параметры и теги.
10. Готовый playbook выложите в свой репозиторий, поставьте тег `08-ansible-03-yandex` на фиксирующий коммит, в ответ предоставьте ссылку на него.

---

### Как оформить ДЗ?

Выполненное домашнее задание пришлите ссылкой на .md-файл в вашем репозитории.

## Ответ 

1. Добавлен новый task для установки lightHouse и apache
2. добавлен templates для конфигурации vhost для apache
3. в group_vars добавил настройки для своих сертификатов для доступа к виртуалкам
4. переработан инсталлятор для clickhouse
5. доработан prod.yml в части lighthouse


    lex@lexhost ~/PycharmProjects/netologyProjects/devops-netology/ansible/08-ansible-03-yandex/playbook $ ansible-playbook -i inventory/prod.yml site.yml 
    [DEPRECATION WARNING]: Ansible will require Python 3.8 or newer on the controller starting with Ansible 2.12. Current version: 3.7.6 (default, Jan  8 2020, 19:59:22)
     [GCC 7.3.0]. This feature will be removed from ansible-core in version 2.12. Deprecation warnings can be disabled by setting deprecation_warnings=False in 
    ansible.cfg.
    
    PLAY [Install Clickhouse] ********************************************************************************************************************************************
    
    TASK [Gathering Facts] ***********************************************************************************************************************************************
    ok: [clickhouse-host]
    
    TASK [Get clickhouse distrib noarch Get clickhouse distrib noarch  from https://packages.clickhouse.com/rpm/stable/] *************************************************
    ok: [clickhouse-host] => (item=clickhouse-client)
    ok: [clickhouse-host] => (item=clickhouse-server)
    
    TASK [Get clickhouse distrib from https://packages.clickhouse.com/rpm/stable/] ***************************************************************************************
    ok: [clickhouse-host] => (item=clickhouse-common-static)
    
    TASK [Install clickhouse packages] ***********************************************************************************************************************************
    ok: [clickhouse-host]
    
    TASK [Start clickhouse service] **************************************************************************************************************************************
    changed: [clickhouse-host]
    
    TASK [Create database] ***********************************************************************************************************************************************
    ok: [clickhouse-host]
    
    PLAY [Install Vector] ************************************************************************************************************************************************
    
    TASK [Gathering Facts] ***********************************************************************************************************************************************
    The authenticity of host '51.250.77.154 (51.250.77.154)' can't be established.
    ECDSA key fingerprint is 29:f6:2e:f2:0a:72:1e:da:4f:84:31:bd:98:01:97:7f.
    Are you sure you want to continue connecting (yes/no)? yes
    ok: [vector-host]
    
    TASK [Download Vector] ***********************************************************************************************************************************************
    changed: [vector-host]
    
    TASK [Install Vector] ************************************************************************************************************************************************
    changed: [vector-host]
    
    PLAY [Lighthouse] ****************************************************************************************************************************************************
    
    TASK [Gathering Facts] ***********************************************************************************************************************************************
    ok: [lighthouse-host]
    
    TASK [Creates install directory for Lighthouse] **********************************************************************************************************************
    ok: [lighthouse-host] => (item=/tmp/lighthouse)
    
    TASK [install required packages] *************************************************************************************************************************************
    ok: [lighthouse-host]
    
    TASK [LightHouse checkout] *******************************************************************************************************************************************
    ok: [lighthouse-host]
    
    TASK [Configure apache. Create /var/www] *****************************************************************************************************************************
    ok: [lighthouse-host]
    
    TASK [Configure apache. Copy lightHouse] *****************************************************************************************************************************
    changed: [lighthouse-host]
    
    TASK [Configure apache. Setup vhost] *********************************************************************************************************************************
    ok: [lighthouse-host]
    
    PLAY RECAP ***********************************************************************************************************************************************************
    clickhouse-host            : ok=6    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
    lighthouse-host            : ok=7    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
    vector-host                : ok=3    changed=2    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
    
    lex@lexhost ~/PycharmProjects/netologyProjects/devops-netology/ansible/08-ansible-03-yandex/playbook $ 

---
